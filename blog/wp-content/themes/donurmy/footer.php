        </section><!-- .content__main -->
        <footer class="footer">
            <div class="footer__row footer__links bg--middlegrey">
                <div class="wrapper">
                    <div class="half left">
                        <?php if( have_rows('footer_aside', 'option') ): ?>
                            <?php while( have_rows('footer_aside', 'option') ): the_row(); ?>
                                <aside>
                                    <?php $blockTitle = get_sub_field('aside_title', 'option'); ?>
                                    <h4><?php echo $blockTitle; ?></h4>
                                    <input type="checkbox" name="<?php echo substr($blockTitle, 0, 4); ?>Toggle" id="<?php echo substr($blockTitle, 0, 4); ?>Toggle" class="checkbox__toggle" />
                                    <label for="<?php echo substr($blockTitle, 0, 4); ?>Toggle" class="checkbox__toggle--label"><?php the_sub_field('aside_title', 'option'); ?></label>
                                    <?php if( have_rows('aside_links_group', 'option') ): ?>
                                        <ul class="footer__list checkbox__toggle--list">
                                            <?php while( have_rows('aside_links_group', 'option') ): the_row(); ?>
                                                <li>
                                                    <a rel="nofollow" href="<?php the_sub_field('aside_link', 'option'); ?>"><?php the_sub_field('aside_link_text', 'option'); ?></a>
                                                </li>
                                            <?php endwhile; ?>
                                        </ul>
                                    <?php endif; ?>
                                </aside>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div><!-- .half -->
                    <div class="half left">
                        <div class="sn-container">
                            <?php if( have_rows('sn_links', 'option') ): ?>
                                <h4><?php _e('Síguenos'); ?></h4>
                                <ul class="social-networks">
                                <?php while ( have_rows('sn_links', 'option') ) : the_row(); ?>
                                    <li>
                                        <a href="<?php the_sub_field('sn_link', 'option'); ?>" target="_blank" title="<?php the_sub_field('sn_link_class', 'option'); ?>">
                                            <span class="icon <?php the_sub_field('sn_link_class', 'option'); ?>"></span>
                                        </a>
                                    </li>
                                <?php endwhile; ?>
                                </ul><!-- .social-networks -->
                            <?php endif; ?>
                        </div>
                    </div><!-- .half -->
                </div>
            </div><!-- .footer__row bg- -middlegrey -->
            <div class="footer__row bg--cleargrey footer--payment-methods">
                <div class="wrapper">
                    <div class="left">
                        <p><?php _e('Compra Segura'); ?></p>
                        <span class="icon rapidssl">RapidSSL</span>
                        <a href="http://www.enisa.es/" target="_blank" title="Enisa" rel="nofollow"><span class="icon enisa">Enisa</span></a>
                        <?php if(get_locale()!='it_IT'){ ?>
                            <a href="https://www.confianzaonline.es/empresas/latorreretailgroup.htm" rel="nofollow" target="_blank" title="Confianza Online"><span class="icon confianza">Confianza Online</span></a>
                        <?php } ?>
                    </div><!-- .left -->
                    <div class="right">
                        <span class="icon mastercard">Mastercard</span>
                        <span class="icon visa">Visa</span>
                        <span class="icon paypal">Paypal</span>
                        <span>
                <svg version="1.1"
                     xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                     xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"
                     x="0px" y="0px" width="98px" height="31px" viewBox="-0.982 -0.869 98 31"
                     enable-background="new -0.982 -0.869 98 31"
                     xml:space="preserve" style="width: 59px;height: 16px;">
<defs>
</defs>
                    <path fill-rule="evenodd" clip-rule="evenodd" fill="#1A559B" d="M85.164,19.122h-8.243l4.091-13.675L85.164,19.122 M83.867,0.006
	c2.217,0,2.787,0.554,3.63,3.057l9.096,26.915H88.49l-1.976-6.47H75.607l-1.966,6.47h-4.676l9.173-29.974L83.867,0.006z"/>
                    <path fill-rule="evenodd" clip-rule="evenodd" fill="#1A559B" d="M36.456,13.194l2.502-3.731c0.633-0.937,0.765-1.524,0.765-2.181
	c0-1.831-1.608-3.369-3.466-3.369h-3.845v22.051h2.131c3.565,0,6.27-1.925,6.27-6.434C40.813,15.813,39.069,13.827,36.456,13.194
	 M37.953,29.99H25.362V0h13.05c6.12,0,8.251,4.987,5.154,9.586L42.1,11.76C52.367,17.089,49.027,29.99,37.953,29.99z"/>
                    <path fill-rule="evenodd" clip-rule="evenodd" fill="#1A559B" d="M11.098,13.194l2.5-3.731c0.632-0.937,0.765-1.524,0.765-2.181
	c0-1.831-1.611-3.369-3.463-3.369H7.052v22.051h2.13c3.565,0,6.27-1.925,6.27-6.434C15.452,15.813,13.706,13.827,11.098,13.194
	 M12.593,29.99H0V0h13.054c6.118,0,8.251,4.987,5.152,9.586L16.74,11.76C27.005,17.089,23.667,29.99,12.593,29.99z"/>
                    <path fill="#1A559B" d="M67.74,0h4.734l-9.137,29.975l-7.195-0.004L46.339,0.004L51.638,0c2.043,0,2.582,0.527,3.652,3.923
	l5.673,18.846L67.74,0"/>
                </svg></span>                    </div><!-- .right -->
                </div>
            </div><!-- .footer__row -->
            <div class="colophon">
                <div class="wrapper">
                    <span>2007 - <?php the_date('Y'); ?> <?php _e('Latorre Retail Group, S.L.U. Todos los derechos reservados.'); ?></span>
                    <?php 
                        if( have_rows('colophon_menu', 'option') ):
                        while ( have_rows('colophon_menu', 'option') ) : the_row();
                    ?>
                        <a href="<?php the_sub_field('colophon_link', 'option'); ?>"><?php the_sub_field('colophon_link_text', 'option'); ?></a>

                    <?php 
                        endwhile;
                        endif;
                    ?>
                </div>
            </div><!-- .colophon -->
        </footer><!-- .footer -->
        <?php wp_footer(); ?>
        
    </body>
</html>