<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>">
        <title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>
        
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1">
            
        <link rel="alternate" type="text/xml" title="<?php bloginfo('name'); ?> RSS 0.92 Feed" href="<?php bloginfo('rss_url'); ?>">
        <link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>">
        <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS 2.0 Feed" href="<?php bloginfo('rss2_url'); ?>">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

        <?php wp_head(); ?>
        <?php
            if(get_locale()=='es_ES')
            {
                ?>
                <!-- Google Tag Manager -->
                <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                    })(window,document,'script','dataLayer','GTM-MB3V2T');</script>
                <!-- End Google Tag Manager -->
                <?php
            }
            else{
                ?>
                <!-- Google Tag Manager -->
                <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                    })(window,document,'script','dataLayer','GTM-NXFQPB8');</script>
                <!-- End Google Tag Manager -->
                <?php
            }
        ?>
    </head>
    <body <?php body_class(); ?>>
        <?php
            if(get_locale()=='es_ES')
            {
                ?>
                <!-- Google Tag Manager (noscript) -->
                <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MB3V2T"
                                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
                <!-- End Google Tag Manager (noscript) -->
                <?php
            }
            else{
                ?>
                <!-- Google Tag Manager (noscript) -->
                <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NXFQPB8"
                                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
                <!-- End Google Tag Manager (noscript) -->
                <?php
            }
        ?>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        <?php 
            $currentHost = getCurrentHost();
            $templateDirectory = get_template_directory();
            $svg = file_get_contents($templateDirectory.'/assets/images/sprites-svg.svg'); 
            echo $svg;
        ?>
        <header class="header">
        <div class="header--top">
            <nav class="header--top__tabs">
                <span class="donurmy-tab">
                    <a href="https://<?php echo getCurrentHost(); ?>/">
                        <svg class="logo-donurmy-tab">
                            <use xlink:href="#logo-donurmy-clean" />
                        </svg>
                    </a>
                </span>
                <a href="https://www.dresoop.es/" class="dresoop-tab" title="Dresoop.es">
                    <span>Disfraces</span>
                    <svg class="logo-dresoop-tab">
                        <use xlink:href="#logo-dresoop-w-claim" />
                    </svg>                    
                </a>
            </nav><!-- .header- -top__tabs -->          
        </div><!-- .header- -top -->
        <div class="header--bottom wrapper" itemscope itemtype="http://schema.org/Organization">
            <a href="https://<?php echo getCurrentHost(); ?>" class="logo" itemprop="url">
                <?php if (is_home()) : ?>
                    <h1><?php bloginfo('name'); ?></h1>    
                <?php else : ?>
                    <h2><?php bloginfo('name'); ?></h2>
                <?php endif; ?>
                <!--[if gte IE 9]><!-->
                <svg class="logo-donurmy">
                    <use xlink:href="#logo-donurmy" />
                </svg>
                <span class="claim">create your habitat</span>
                <!--<![endif]-->
                <img itemprop="logo" style="display:none;" src="<?php bloginfo('template_directory'); ?>/assets/images/logo-dresoop.png" alt="Donurmy" />
            </a>
            <div class="header__content">
                <div class="info">
                    <a href="tel:<?php the_field('phone_number_i18n', 'option'); ?>">
                        <p><span><i class="fa fa-phone"></i></span> <?php the_field('phone_number_visual', 'option'); ?></p>
                        <span>
                            <?php the_field('timetable_text', 'option'); ?>
                        </span>
                    </a>
                </div><!-- .info -->
            </div><!-- .header__content -->
        </div><!-- .header- -bottom --> 
    </header><!-- .header -->
    <nav class="nav clearfix">
        <div class="page-navigation wrapper">
            <?php 
                    $menuParameters = array(
                        'container'       => 'false',
                        'echo'            => false,
                        'items_wrap'      => '%3$s',
                        'depth'           => 0,
                    );
                    echo strip_tags(wp_nav_menu( $menuParameters ), '<a>' );
                ?>
        </div>
    </nav><!-- .nav -->
    <section class="content__main clearfix wrapper">
        <nav class="nav--breadcrumb">
            <?php breadcrumbs(); ?>
        </nav><!-- .nav- -breadcrumbs -->
    