<?php get_header(); ?>

    <section class="width__70 left">
        <header>
            <h1 class="alpha"><?php _e('Página No Encontrada'); ?></h1>               
        </header><!-- .head-title -->
        
        <article class="post">
            <p><?php _e('Lo sentimos pero la página que estaba buscando no se encuentra o no está disponible en estos momentos.'); ?></p>
            <?php get_search_form(); ?>
        </article>
        
    </section>
    <?php get_sidebar(); ?>

<?php get_footer(); ?>