<?php get_header(); ?>

        <section class="width__70 left">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <article id="post-<?php the_ID(); ?>" class="post">
                    <header>
                        <h1 class="alpha"><?php the_title(); ?></h1>
                        <time datetime="<?php echo date(DATE_W3C); ?>" pubdate class="updated"><i class="fa fa-calendar-o"></i> <?php the_time('j \d\e\ F \d\e\ Y') ?></time>
                    </header>
                    <section class="post__content">
                        
                        <?php the_content(); ?>

                    </section><!-- .post-content -->
                    <footer class="post__footer">
                        <div class="meta">
                            <p>
                                <i class="fa fa-tag"></i> <?php _e('Publicado en'); ?> <?php the_category(', '); ?>
                                <span class="comments__link">
                                    <i class="fa fa-comments"></i> <?php comments_popup_link( __('Comenta la entrada &raquo;'), __('1 Comentario &raquo;'), __('% Comentarios &raquo;')); ?>
                                </span>
                            </p>
                        </div><!-- meta -->
                    </footer>
                </article><!-- .post -->
                
                <?php comments_template(); ?>

            <?php endwhile; else: ?>
                    
                <article class="post">
                    <section class="post__content">
                        <p>Lo siento no hay posts que coincidan con su búsqueda.</p>
                    </section><!-- .post-content -->
                </article><!-- .post -->
                
            <?php endif; ?>
        </section>
        <?php get_sidebar(); ?>
                
<?php get_footer(); ?>