					<?php if (is_search()):?>
						<form method="get" id="searchform" class="search-form" action="<?php bloginfo('url'); ?>/">
							<input type="text" id="s" name="s"placeholder="Buscar" value="<?php the_search_query(); ?>">
							<input type="submit" value="Buscar">							
						</form>
						<div class="clearfix"></div>
					<?php else : ?>
						<form method="get" id="searchform" class="search-form" action="<?php bloginfo('url'); ?>/">
							<input type="text" id="s" name="s"placeholder="Buscar" value="<?php the_search_query(); ?>">
							<button class="btn btn--ico"><i class="fa fa-search"></i></button>
						</form>
					<?php endif; ?>