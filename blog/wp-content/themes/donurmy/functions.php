<?php
    
    // Favicon function
    function donurmy_favicon() { ?>
        <link rel="shortcut icon" href="<?php echo bloginfo('stylesheet_directory') ?>/assets/images/favicon.png" />
        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo bloginfo('stylesheet_directory') ?>/assets/images/favicons/donurmy/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo bloginfo('stylesheet_directory') ?>/assets/images/favicons/donurmy/apple-touch-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo bloginfo('stylesheet_directory') ?>/assets/images/favicons/donurmy/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo bloginfo('stylesheet_directory') ?>/assets/images/favicons/donurmy/apple-touch-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo bloginfo('stylesheet_directory') ?>/assets/images/favicons/donurmy/apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo bloginfo('stylesheet_directory') ?>/assets/images/favicons/donurmy/apple-touch-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo bloginfo('stylesheet_directory') ?>/assets/images/favicons/donurmy/apple-touch-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo bloginfo('stylesheet_directory') ?>/assets/images/favicons/donurmy/apple-touch-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo bloginfo('stylesheet_directory') ?>/assets/images/favicons/donurmy/apple-touch-icon-180x180.png">
        <link rel="icon" type="image/png" href="<?php echo bloginfo('stylesheet_directory') ?>/assets/images/favicons/donurmy/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="<?php echo bloginfo('stylesheet_directory') ?>/assets/images/favicons/donurmy/android-chrome-192x192.png" sizes="192x192">
        <link rel="icon" type="image/png" href="<?php echo bloginfo('stylesheet_directory') ?>/assets/images/favicons/donurmy/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="<?php echo bloginfo('stylesheet_directory') ?>/assets/images/favicons/donurmy/favicon-16x16.png" sizes="16x16">
        <link rel="manifest" href="<?php echo bloginfo('stylesheet_directory') ?>/assets/images/favicons/donurmy/manifest.json">
        <meta name="msapplication-TileColor" content="#2b5797">
        <meta name="msapplication-TileImage" content="<?php echo bloginfo('stylesheet_directory') ?>/assets/images/favicons/donurmy/mstile-144x144.png">
        <meta name="theme-color" content="#ffffff">
    <?php }
    add_action('wp_head', 'donurmy_favicon');

    function donurmy_scripts(){
        // Set false if you want to load on the <head>.
        if (!is_admin()) {
            wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/assets/js/modernizr-2.6.2.min.js', false );
            wp_enqueue_style( 'donurmy-style', get_stylesheet_uri() );
            wp_enqueue_script( 'jquery', get_template_directory_uri() . '/assets/js/jquery-1.10.1.min.js', array( 'jquery' ), true );
            wp_enqueue_script( 'main-script', get_template_directory_uri() . '/assets/js/main.js', array(),'', true );
            if ( is_singular() ) wp_enqueue_script( 'comment-reply' );
        }
    }
    add_action( 'wp_enqueue_scripts', 'donurmy_scripts' );

    add_action('wp_footer', 'google_tag_manager');
    function google_tag_manager() { ?>
    
        <?php if (get_field('gtm_code', 'option')) {
            the_field('gtm_code', 'option');
        } ?>
     
    <?php
    }


    // Enqueue Fonts.
    function load_fonts() {
        wp_register_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');
        wp_register_style('roboto', '//fonts.googleapis.com/css?family=Roboto:400,400italic,500,500italic,700,700italic');
        wp_enqueue_style( 'font-awesome');
        wp_enqueue_style( 'roboto');
    }
    
    add_action('wp_print_styles', 'load_fonts');
    
    if (function_exists('register_sidebar')) {
        register_sidebar(array(
            'name' => __('Sidebar Widgets','mminimal' ),
            'id'   => 'sidebar-widgets',
            'description'   => __( 'These are widgets for the sidebar.','mminimal' ),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h2>',
            'after_title'   => '</h2>'
        ));
    }

    // Removing the style on html tag for the user bar.
    add_action('get_header', 'my_filter_head');

    function my_filter_head() {
        remove_action('wp_head', '_admin_bar_bump_cb');
    }
    //add_theme_support( 'post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'audio', 'chat', 'video')); // Add 3.1 post format theme support.

    // Function for activate thumbnails and size.       
    add_theme_support( 'post-thumbnails' );
    add_image_size( 'gallery-thumbnail', 150, 150, true );

    // Customising the classes on the body_class function.
    add_filter('body_class','my_class_names');  
    function my_class_names($classes) {  
        global $wp_query;  
      
        $arr = array();  
        
        if(is_front_page()) {
            $arr[] = 'page__home';
        }

        if(is_page()) {  
            global $post;
            $post_slug=$post->post_name;
            $arr[] = 'page__' . $post_slug;  
        }  
      
        if(is_single()) {  
            $post_id = $wp_query->get_queried_object_id();  
            $arr[] = 'post single post-id-' . $post_id;  
        }
        
        if(is_404()) {
            $arr[] = 'page__404';   
        }

        if (is_user_logged_in()) {
            $arr[] = 'user__logon';
        }

        if (is_search()) {
            $arr[] = 'page__search';
        }

        return $arr;  
    }

    function register_my_menus() {
        register_nav_menus(
            array(
              'header-menu' => __( 'Header Menu' ),
              'extra-menu' => __( 'Extra Menu' )
            )
        );
    }
    add_action( 'init', 'register_my_menus' );
    
    // ACF Options Page.
    if(function_exists('acf_add_options_page')) {
        acf_add_options_page(array(
            'page_title'    => 'Header',
            'menu_title'    => 'Header',
            'menu_slug'     => 'custom_header',
            'capability'    => 'edit_posts',
            'redirect'      => false, 
        ));
        acf_add_options_page(array(
            'page_title'    => 'Footer',
            'menu_title'    => 'Footer',
            'menu_slug'     => 'custom_footer',
            'capability'    => 'edit_posts',
            'redirect'      => false, 
        ));
        acf_add_options_page(array(
            'page_title'    => 'Google Tag Manager',
            'menu_title'    => 'GTM',
            'menu_slug'     => 'gtm_script',
            'capability'    => 'edit_posts',
            'redirect'      => false, 
        ));
    }
    
    //add_filter('acf/settings/show_admin', '__return_false');

    // Disable auto-generated p on ACF WYSIWIG editor
    function ptobr($string){
        return preg_replace("/<\/p>[^<]*<p>/", "<br /><br />", $string);}

    function stripp($string){
        return preg_replace('/(<p>|<\/p>)/i','',$string) ;}

    // Determine the text of the 'Read more'
    function my_more_link($more_link, $more_link_text) {
    return str_replace($more_link_text, 'Leer más &raquo;', $more_link);
    }
    add_filter('the_content_more_link', 'my_more_link', 10, 2);

    // Breadcrumbs
    function breadcrumbs() {
        if (!is_home()) {
            echo '<a href="';
            echo get_option('home');
            echo '">';
            bloginfo('name');
            echo '</a>  ';
            if (is_category() || is_single()) {
                the_category('<i></i>');
                if (is_single()) {
                    echo '  <span>'.get_the_title().'</span>';
                }
            } elseif (is_page()) {
                echo '<span>'.get_the_title().'</span>';
            }
        } else {
            echo '<span>Home</span>';
        }
    }

    add_filter('next_posts_link_attributes', 'posts_link_attributes_next');
    add_filter('previous_posts_link_attributes', 'posts_link_attributes_prev');

    function posts_link_attributes_prev() {
        return 'class="btn btn__prev-page"';
    }

    function posts_link_attributes_next() {
        return 'class="btn btn__next-page"';
    }

    
    function mytheme_comment($comment, $args, $depth) {
       $GLOBALS['comment'] = $comment; ?>
       <div <?php comment_class('clearfix'); ?> id="div-comment-<?php comment_ID() ?>">
            <div id="comment-<?php comment_ID(); ?>" class="comment__bubble">
                <div class="comment-author vcard">
                    <div class="avatar">
                        <?php
                            $avatar_size = 62;
                            if ( '0' != $comment->comment_parent ) {
                                $avatar_size = 31;
                            }
                            echo get_avatar( $comment, $avatar_size ); 
                        ?>
                    </div>
                    <?php printf( __( '<span class="fn">%s</span>' ), get_comment_author_link() ); ?>
                    <div class="comment-meta commentmetadata">
                        <a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>">
                            <i class="fa fa-calendar"></i>
                            <?php
                                /* translators: 1: date, 2: time */
                                printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time() ); ?></a><?php edit_comment_link( __( '(Edit)' ), '  ', '' );
                            ?>
                        </a>
                    </div>
                </div><!-- .comment-author -->
                <div class="comment__content">
                    <?php if ( $comment->comment_approved == '0' ) : ?>
                        <em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.' ); ?></em>
                        <br />
                    <?php endif; ?>

                    <?php comment_text(); ?>
                </div><!-- .comment__content -->
                <div class="reply">
                    <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
                </div><!-- .reply -->
            </div><!-- .comment__bubble -->
    <?php
    }

?>