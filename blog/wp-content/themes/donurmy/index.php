<?php get_header(); ?>
    
        <section class="width__70 left post__list">
            <?php while (have_posts()) : the_post(); ?>

                <article id="post-<?php the_ID(); ?>" class="post">
                    <header>
                        <h2 class="alpha"><a href="<?php the_permalink() ?>" rel="bookmark" title="Enlace permanente a <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                        <time datetime="<?php echo date(DATE_W3C); ?>" pubdate class="updated"><i class="fa fa-calendar-o"></i> <?php the_time('j \d\e\ F \d\e\ Y') ?></time>
                    </header>
                    <section class="post__content">                 
                        <div class="post__thumbnail">
                            <a href="<?php the_permalink(); ?>">
                                <?php if (get_the_post_thumbnail()) : ?>
                                    <?php the_post_thumbnail('thumbnail'); ?>
                                <?php else : ?>
                                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/default-thumbnail.jpg" width="150" height="150" alt="Miniatura" />
                                <?php endif; ?>
                            </a>
                        </div><!-- .post__thumbnail -->
                        <?php the_excerpt(); ?>
                    </section><!-- .post-content -->
                    <footer class="post__footer">
                        <div class="meta">
                            <p>
                                <i class="fa fa-tag"></i> <?php _e('Publicado en'); ?> <?php the_category(', '); ?>
                                <span class="comments__link">
                                    <i class="fa fa-comments"></i> <?php comments_popup_link( __('Comenta la entrada &raquo;'), __('1 Comentario &raquo;'), __('% Comentarios &raquo;')); ?>
                                </span>
                            </p>
                        </div><!-- meta -->
                    </footer>
                </article><!-- .post -->

            <?php endwhile; ?>

            <nav class="navigation">
                <div class="next-posts"><?php next_posts_link( __('Página Siguiente &raquo;')); ?></div>
                <div class="prev-posts"><?php previous_posts_link( __('&laquo; Página Anterior')); ?></div>
            </nav><!-- .navigation -->
        </section><!-- .post__list -->
        <?php get_sidebar(); ?> 

<?php get_footer(); ?>