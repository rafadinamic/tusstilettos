<?php // Do not delete these lines
    if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
        die ('Please do not load this page directly. Thanks!');
    if (!empty($post->post_password)) {
        if ($_COOKIE['wp-postpass_' . COOKIEHASH] != $post->post_password) { ?>
            <p class="nocomments">This post is password protected. Enter the password to view comments.</p>
            <?php return;
        }
    }
    $oddcomment = 'class="comment" '; // alternating comments
?>

<?php if ('open' == $post->comment_status) : ?>

        <?php comment_form(
            array(
                'fields' => 
                    array(
                        'author' => 
                            '<fieldset class="fieldset--full">' 
                            .'<label for="author" class="required">' 
                            .__( 'Nombre' ) 
                            .'</label> ' 
                            .'<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" />'
                            .'</fieldset>',
                        'email' => 
                            '<fieldset class="fieldset--full">'
                            .'<label for="email" class="required">' 
                            .__( 'E-mail' ) 
                            .'</label> ' 
                            .'<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" />'
                            .'</fieldset>',
                        'url' => 
                            '<fieldset class="fieldset--full">'
                            .'<label for="url">'
                            .__( 'Web' )
                            .'</label>'
                            .'<input id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '" />'
                            .'</fieldset>',
                        'check' =>
                            '<fieldset class="fieldset--full fieldset--checkbox">'
                            .'<input type="checkbox" id="blog_comments_checkbox" required>'
                            .'<label for="blog_comments_checkbox"><span>He leído y acepto las <a target="_blank" href="https://www.donurmy.es/ayuda/condiciones-de-uso_p25">condiciones de uso y política de privacidad</a></span></label>'
                            .'</fieldset>'
                    ),
                'label_submit'          => __( 'Enviar' ),
                'class_submit'          => 'btn btn__submit btn--ico',
                'submit_button'         => '<button id="%2$s" class="%3$s" /><span class="btn--helper"></span>%4$s<i class="fa fa-chevron-circle-right"></i></button>',
                'title_reply'           => __( '<i class="fa fa-pencil-square-o"></i> Escribe un comentario' ),
                'cancel_reply_link'     => __( 'Cancelar respuesta' ),
                'comment_field'         =>  
                    '<fieldset class="fieldset--full">'
                    .'<label for="comment" class="required">'
                    . _x( 'Comment', 'noun' )
                    .'</label>'
                    .'<textarea id="comment" name="comment" cols="45" rows="8" aria-required="true">'
                    .'</textarea>'
                    .'</fieldset>',
                'comment_notes_before'  => '',
                'comment_notes_after'   => '<small class="small-print">*Tu e-mail no ser&aacute; publicado. Los campos marcados con un asterisco son obligatorios.</small>'
            )
        ); ?>

<?php endif; ?>

<?php if ($comments) : // there are comments ?>

        <section class="comment__list commentlist">
            <h3><i class="fa fa-comments"></i> <?php comments_number('', __('Un comentario'), __('% comentarios') ); ?></h3>

            <?php wp_list_comments(array( 
                'style'             => 'div',
                'type'              => 'comment',
                'avatar_size'       => '62',
                'callback'          => 'mytheme_comment'
            )); ?>
            <?php if ( get_option( 'page_comments' ) ) {
                paginate_comments_links(); 
            } ?>
        </section>

<?php else : // no comments yet ?>

    <?php if ('open' == $post->comment_status) : ?>
        <!-- [comments are open, but there are no comments] -->

     <?php else : ?>
        <!-- [comments are closed, and no comments] -->
        <p>Los comentarios están cerrados.</p>

    <?php endif; ?>
<?php endif; ?>