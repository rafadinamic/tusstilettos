<!DOCTYPE HTML>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="es-es"><![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8 ie7" lang="es-es"><![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9 ie8" lang="es-es"><![endif]-->
<!--[if gt IE 8]>
<html class="no-js ie9" lang="es-es"><![endif]-->
<html lang="es-es">
<head>
    <meta charset="utf-8"/>
    <title>Blog - TusStilettos</title>
    <meta name="description"
          content="Descubre los stilettos y los zapatos de tacón de moda para tus looks y elige los zapatos stilettos online desde la comodidad de tu casa"/>
    <meta name="p:domain_verify" content="db120a3e83844e6f7bf6fb4212a255ad"/>

    <meta name="robots" content="index,follow"/>
    <meta name="viewport" content="width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <link rel="icon" type="image/vnd.microsoft.icon" href="/img/favicon.ico?1497009023"/>
    <link rel="shortcut icon" type="image/x-icon" href="/img/favicon.ico?1497009023"/>
    <link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
    <link rel="stylesheet"
          href="https://tusstilettos.dev/themes/PRS070171/cache/v_143_01d36e371342df15783939ccf43fd13c_all.css"
          type="text/css" media="all"/>
    <!-- ================ Additional Links By Tempaltemela : START  ============= -->
    <link rel="stylesheet" type="text/css" href="https://tusstilettos.dev/themes/PRS070171/css/megnor/custom.css"/>
    <link rel="stylesheet" type="text/css" href="https://tusstilettos.dev/themes/PRS070171/css/megnor/lightbox.css"/>
    <link rel="stylesheet" type="text/css" href="https://tusstilettos.dev/themes/PRS070171/css/megnor/avd.css"/>
    <!-- ================ Additional Links By Tempaltemela : END  ============= -->

    <script type="text/javascript"
            src="https://tusstilettos.dev/themes/PRS070171/cache/v_76_4a92d9b0ca070ed4d18b5cfd0ffa17c9.js"></script>
    <!-- ================ Additional SCRIPT By Tempaltemela : START  ============= -->
    <script type="text/javascript" src="https://tusstilettos.dev/themes/PRS070171/js/megnor/owl.carousel.js"></script>
    <script type="text/javascript" src="https://tusstilettos.dev/themes/PRS070171/js/megnor/custom.js"></script>
    <!-- <script type="text/javascript" src="https://tusstilettos.dev/themes/PRS070171/js/megnor/parallex.js"></script> -->
    <script type="text/javascript"
            src="https://tusstilettos.dev/themes/PRS070171/js/megnor/lightbox-2.6.min.js"></script>
    <script type="text/javascript"
            src="https://tusstilettos.dev/themes/PRS070171/js/megnor/jquery.slimscroll.js"></script>
    <script type="text/javascript" src="https://tusstilettos.dev/themes/PRS070171/js/megnor/doubletaptogo.js"></script>
    <!-- ================ Additional SCRIPT By Tempaltemela : END  ============= -->


    <script type="text/javascript">
        (window.gaDevIds = window.gaDevIds || []).push('d6YPbH');
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-89714249-1', 'auto', {'allowLinker': true});
        ga('require', 'ec');
        ga('require', 'linker');
        ga('linker:autoLink', ['https://www.paypal.com/es/cgi-bin/webscr', 'https://sis-t.redsys.es:25443/sis/realizarPago']);</script>
    <!--Start of Zopim Live Chat Script-->

    <script type="text/javascript">
        window.$zopim || (function (d, s) {
            var z = $zopim = function (c) {
                z._.push(c)
            }, $ = z.s =
                d.createElement(s), e = d.getElementsByTagName(s)[0];
            z.set = function (o) {
                z.set._.push(o)
            };
            z._ = [];
            z.set._ = [];
            $.async = !0;
            $.setAttribute('charset', 'utf-8');
            $.src = '//v2.zopim.com/?4oxF3dBnQbEuimGeTTGlgpGTffdMLKhZ';
            z.t = +new Date;
            $.type = 'text/javascript';
            e.parentNode.insertBefore($, e)
        })(document, 'script');

    </script>
    <!--End of Zopim Live Chat Script-->


    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,300" type="text/css" media="all"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,300,700" type="text/css" media="all"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700" type="text/css" media="all"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:400,700" type="text/css" media="all"/>
    <!--[if IE 8]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->


    <noscript>
        <img height="1" width="1" src="https://www.facebook.com/tr?id=1421117817913094&ev=PageView&noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body id="index" class="index hide-left-column show-right-column lang_es">
<div id="page">
    <div class="header-container">
        <header id="header">
            <div class="banner">
                <div class="container">
                    <div class="row">

                    </div>
                </div>
            </div>
            <div class="headerdiv">
                <div class="container">
                    <div class="row">
                        <div id="header_logo" class="col-lg-4 col-md-4  col-sm-4 col-xs-12">
                            <a href="https://tusstilettos.dev/" title="TusStilettos">
                                <img class="logo img-responsive"
                                     src="https://tusstilettos.dev/img/stilos-logo-1482861042.jpg" alt="TusStilettos"
                                     width="180" height="88"/>
                            </a>
                        </div>

                        <div id="block_top_menu" class="sf-contener clearfix col-lg-12">
                            <!--<a href="#" id="alternar-panel-oculto"><div class="cat-title" >Menú</div></a>-->
                            <a href="#" id="alternar-panel-oculto">
                                <div class="cat-title"></div>
                            </a>
                            <ul class="sf-menu clearfix menu-content" id="vermenu">
                                <li><a href="https://tusstilettos.dev/12-tus-dias" title="Tus días">Tus días</a></li>
                                <li><a href="https://tusstilettos.dev/13-tus-eventos" title="Tus eventos">Tus
                                        eventos</a></li>
                                <li><a href="https://tusstilettos.dev/14-tus-noches" title="Tus noches">Tus noches</a>
                                </li>
                                <li><a href="https://tusstilettos.dev/15-tus-caprichos" title="Tus caprichos">Tus
                                        caprichos</a></li>
                                <li><a href="https://tusstilettos.dev/21-tus-colores" title="Tus Colores">Tus
                                        Colores</a></li>
                                <li><a href="https://tusstilettos.dev/16-tus-oportunidades" title="Tus oportunidades">Tus
                                        oportunidades</a></li>
                                <li><a href="https://tusstilettos.dev/blog" title="Blog">Blog</a></li>
                            </ul>
                        </div>
                        <!--/ Menu -->

                    </div>
                </div>
            </div>
        </header>
    </div>
    <div class="columns-container">
        <div id="slider_row" class="row">
            <div class="container lyl-filter">
                <div class="clear"></div>


            </div>

        </div>

        <div id="columns" class="container">
            <div class="row" id="columns_inner">
                <div id="columns" class="container">
                    <div class="row" id="columns_inner">

                        <div id="center_column" class="center_column col-xs-12" style="width: 100%;">


                            <h1 class="page-heading product-listing"><span class="cat-name">Tu Blog&nbsp;</span></h1>
                            <div class="content_scene_cat">

                                <!-- Category image -->
                                <div class="cat_desc">

                                    <div class="rte"><p><img src="/img/cms/tu-blog.png" alt="" height="91" width="340">
                                        </p>
                                        <p>Descubre nuestras novedades</p>
                                    </div>


                                </div>

                                <div class="align_center">
                                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/tu-blog.jpg"
                                         alt="Tus oportunidades" title="Tus oportunidades" id="categoryImage" width="1200"
                                         height="293">
                                </div>

                            </div>