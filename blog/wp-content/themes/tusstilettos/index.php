<?php get_header(); ?>


    <!-- Products list -->

    <ul class="product_list row list">


<?php while (have_posts()) :
    the_post(); ?>

    <li class="ajax_block_product col-lg-3 last-line last-item-of-mobile-line col-xs-12">
        <div class="product-container">
            <div class="row">
                <!--Imagen-->
                <div class="left-block col-xs-12 col-sm-5 col-md-5 col-lg-4">
                    <div class="product-image-container">
                        <a class="product_img_link"
                           href="<?php the_permalink(); ?>"
                           title="<?php the_permalink(); ?>">
                            <?php if (get_the_post_thumbnail()) : ?>
                                <?php the_post_thumbnail('thumbnail'); ?>
                            <?php else : ?>
                                <img src="<?php bloginfo('template_directory'); ?>/assets/images/default-thumbnail.png" alt="Miniatura"/>
                            <?php endif; ?>
                        </a>
                    </div>

                </div>
                <div class="center-block col-xs-12 col-sm-7 col-md-7 col-lg-8">
                    <div class="product-flags">
                        <span class="discount">¡Precio rebajado!</span>
                    </div>
                    <h5 itemprop="name">
                        <a class="product-name"
                           href="https://tusstilettos.com/80-irina-terciopelo.html"
                           title="Irina Terciopelo" itemprop="url">
                            <a href="<?php the_permalink() ?>" rel="bookmark"
                               title="Enlace permanente a <?php the_title_attribute(); ?>"><?php the_title(); ?></a>
                        </a>
                    </h5>
                    <p class="product-desc">
                        <?php the_excerpt(); ?>
                    </p>
                    <div class="color-list-container"></div>
                    <span class="availability">
																	<span class=" label-success">
										En stock									</span>
															</span></div>
                <div class="right-block col-xs-12 col-sm-7 col-md-7 col-lg-8">
                    <div class="right-block-content row">
                        <div class="button-container col-xs-12 col-md-12">
                            <p class="btn btn-default">
                                                <span> <time datetime="<?php echo date(DATE_W3C); ?>" pubdate
                                                             class="updated"><i
                                                                class="fa fa-calendar-o"></i> <?php the_time('j \d\e\ F \d\e\ Y') ?></time></span>
                            </p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </li>

    </ul>

<?php endwhile; ?>

    <div class="content_sortPagiBar">
        <div class="bottom-pagination-content clearfix">


            <!-- Pagination -->
            <div id="pagination_bottom" class="pagination clearfix">
            </div>
            <div class="product-count">
                Mostrando 1 - 1 de 1 productos
            </div>
            <!-- /Pagination -->

        </div>
    </div>


    </div>
    </div>
    </div>
    </div><!-- #center_column -->
    </div><!-- .row -->
    </div>

<?php get_footer(); ?>