<?php get_header(); ?>
    
        <section class="width__70 left">
            <header>
                <h1 class="alpha"><?php the_title(); ?></h1>               
            </header><!-- .head-title -->
            
            <?php while (have_posts()) : the_post(); ?>
                <article class="post">
                    <?php the_content(); ?>
                </article>
            <?php endwhile; ?>
        </section>
        <?php get_sidebar(); ?> 

<?php get_footer(); ?>