<!-- Footer -->
<div class="footer-container">

    <div class="content">

        <footer id="footer" class="container">
            <div class="row"><!-- Block links module -->
                <section class="footer-block col-xs-12 col-sm-4" id="tm_links_block1_footer">
                    <h4 class="title_block">

                    </h4>
                    <div class="block_content toggle-footer">
                        <ul class="bullet">

                            <li>
                                <a href="http://tusstilettos.com/content/4-sobre-tus-stilettos"
                                   title="Sobre tus stilettos" target="_self">Sobre tus stilettos</a>
                            </li>

                            <li>
                                <a href="/content/8-faq" title="FAQ" target="_self">FAQ</a>
                            </li>

                            <li>
                                <a href="/contactanos" title="Contacto" target="_self">Contacto</a>
                            </li>
                        </ul>
                    </div>
                </section>
                <!-- /Block links module -->
                <section id="social_block" class="footer-block col-xs-12 col-sm-4">
                    <h4 class="title_block">Síganos</h4>
                    <ul class="toggle-footer">
                        <li class="facebook">
                            <a class="_blank" href="https://www.facebook.com/tusstilettos/">
                                <span>Facebook</span>
                            </a>
                        </li>
                        <li class="twitter">
                            <a class="_blank" href="https://twitter.com/TusStilettos">
                                <span>Twitter</span>
                            </a>
                        </li>
                        <li class="pinterest">
                            <a class="_blank" href="https://es.pinterest.com/tusstilettos/">
                                <span>Pinterest</span>
                            </a>
                        </li>
                        <li class="instagram">
                            <a class="_blank" href="https://www.instagram.com/tusstilettos/">
                                <span>Instagram</span>
                            </a>
                        </li>
                    </ul>
                </section>

                <!-- Block links module -->
                <div class="clearfix"></div>
                <section class="footer-block col-xs-12 col-sm-12" id="tm_links_block2_footer">
                    <h4 class="title_block">

                    </h4>
                    <div class="block_content toggle-footer">
                        <ul class="bullet">

                            <li>
                                <a href="/content/7-privacidad" title="Privacidad" target="_self">Privacidad</a>
                            </li>

                            <li>
                                <a href="/content/2-aviso-legal" title="Aviso Legal" target="_self">Aviso Legal</a>
                            </li>

                            <li>
                                <a href="http://tusstilettos.com/content/3-envios-y-devoluciones"
                                   title="Envíos y Devoluciones" target="_self">Envíos y Devoluciones</a>
                            </li>

                            <li>
                                <a href="http://tusstilettos.com/content/1-politica-de-cookies" title="Cookies"
                                   target="_self">Cookies</a>
                            </li>

                            <li>
                                <a href="http://tusstilettos.com/content/10-condiciones-generales-de-contratacion"
                                   title="Condiciones Generales" target="_self">Condiciones Generales</a>
                            </li>

                            <li>
                                <a href="http://tusstilettos.com/content/12-terminos-y-condiciones-de-uso"
                                   title="Términos y Condiciones" target="_self">Términos y Condiciones</a>
                            </li>
                        </ul>
                    </div>
                </section>
                <!-- /Block links module -->
                <div class="center-text"> &copy; TusStilettos 2016</div>
        </footer>
    </div>
</div><!-- #footer -->
</div><!-- #page -->
<a class="top_button" href="#" style="display:none;">&nbsp;</a>
</body></html>