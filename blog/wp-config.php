<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'tusstile_wordpress');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', 'invitado51');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

define( 'WP_MEMORY_LIMIT', '64M' );

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '{D=qV+Smk3p`GI*9vzq>},3:Q>O{IElA~>4#pW_C~m&>~&ZbXV7,o>S{5 D]QUn?');
define('SECURE_AUTH_KEY', '2p JhoBM&k:LV8]@A?Ix@#,=S&}}GJcKbRXHvN~AnJ*~OGhljC4a;M9G7^j=A~VK');
define('LOGGED_IN_KEY', ']Vn7U<0g{v5uUX!?H}Z O:PwU$]fXCAFIEJL%EvM/uAw$SK?H|GQ]t3IV8HX]3d-');
define('NONCE_KEY', 'S0L/ASBXW`+{e=^5hA^k`%$X`)WkSqBtLfLjJkTYAj4Z6o+(h,;PzeeU+fnCA38I');
define('AUTH_SALT', '~RWst8[l5-hd2M>:e@vUe3y0!F$k3gmaB%.~`O,.SoNv&3;GsV^q)l_ds7G>=_-6');
define('SECURE_AUTH_SALT', '}bk^MU+*9RP=LsQNjMma{qvG0O7`4S@lBc:[Ou>yua&>ei0,-#]W:O7%Od=|.bq/');
define('LOGGED_IN_SALT', 'fkNA&R_J[yl}:o]e|jY~NGn5@Sh&|;<tkiavr{y/I3Bues*vC?lIzo].#5`wVPj_');
define('NONCE_SALT', '?t[qKl{6<P7f{v4+KF?3VNUaff0RFcN/XdXI^G^%(q#(RP+T~}<%cwL<2/LA{hT%');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

