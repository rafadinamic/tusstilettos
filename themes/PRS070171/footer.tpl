{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if !isset($content_only) || !$content_only}
					</div><!-- #center_column -->
					{if isset($right_column_size) && !empty($right_column_size)}
						<div id="right_column" class="col-xs-12 column"  style="width:{$right_column_size}%;">{$HOOK_RIGHT_COLUMN}</div>
					{/if}
					</div><!-- .row -->
				</div><!-- #columns -->
			</div><!-- .columns-container -->

    {if $page_name =='index'}

    <div class="footer-container" style="padding: 25px">
            <div class="content">
                <footer id="footer"  class="container">
                    <div class="row">
                        {l s='Text Home'}
                    </div>
                </footer>
            </div>
    </div>
    {/if}


    {if isset($HOOK_FOOTER)}
				<!-- Footer -->
				<div class="footer-container">
					{* if $page_name =='index' }
						<div class="footer_hide">
							<div class="container">
								<div class="footer_btn">
									<span class="footer_top_arrow"></span>
								</div>
							</div>
						</div>
					{ /if *}
					<div class="content">
						{* <div class="tm_newsletterdiv"></div> *}
						<footer id="footer"  class="container">
							<div class="row">{$HOOK_FOOTER}</div>
                            <div class="center-text"> &copy; TusStilettos 2016</div>
						</footer>
					</div>
				</div><!-- #footer -->
			{/if}
		</div><!-- #page -->
		<a class="top_button" href="#" style="display:none;">&nbsp;</a>
{/if}
{include file="$tpl_dir./global.tpl"}
	</body>
</html>