/*! Customized Jquery from Mahesh Vaghani.  mahesh@templatemela.com  : www.templatemela.com
Authors & copyright (c) 2013: TemplateMela - Megnor Computer Private Limited. */
// Megnor Start
$(document).ready(function () {
	
	
	
	
	var $window = $(window), $document = $(document),
		transitionSupported = typeof document.body.style.transitionProperty === "string", // detect CSS transition support
		scrollTime = 1; // scroll time in seconds

	$(document).on("click", "a[href*=#]:not([href=#])", function(e) {
		var target, avail, scroll, deltaScroll;
    
		if (location.pathname.replace(/^\//, "") == this.pathname.replace(/^\//, "") && location.hostname == this.hostname) {
			target = $(this.hash);
			target = target.length ? target : $("[id=" + this.hash.slice(1) + "]");

			if (target.length) {
				avail = $document.height() - $window.height();

				if (avail > 0) {
					scroll = target.offset().top;
          
					if (scroll > avail) {
						scroll = avail;
					}
				} else {
					scroll = 0;
				}

				deltaScroll = $window.scrollTop() - scroll;

				// if we don't have to scroll because we're already at the right scrolling level,
				if (!deltaScroll) {
					return; // do nothing
				}

				e.preventDefault();
				
				if (transitionSupported) {
					$("html").css({
						"margin-top": deltaScroll + "px",
						"transition": scrollTime + "s ease-in-out"
					}).data("transitioning", scroll);
				} else {
					$("html, body").stop(true, true) // stop potential other jQuery animation (assuming we're the only one doing it)
					.animate({
						scrollTop: scroll + "px"
					}, scrollTime * 1000);
					
					return;
				}
			}
}

	if (transitionSupported) {
		$("html").on("transitionend webkitTransitionEnd msTransitionEnd oTransitionEnd", function(e) {
			var $this = $(this),
				scroll = $this.data("transitioning");
			
			if (e.target === e.currentTarget && scroll) {
				$this.removeAttr("style").removeData("transitioning");
				
				$("html, body").scrollTop(scroll);
			}
		});
	}
});
	
	
	
	
	
	
	$('#header .cart_block dl.products').slimScroll({
		height: '100%'
	});
	$('#block_top_menu li:has(ul)').doubleTapToGo();
	
	$('.footer-container #footer #social_block').clone().prependTo( ".footer-container .footer_hide .container" );
	$('.footer-container .footer_hide .container #social_block').find('ul').removeAttr('style');
	//=================== Show or hide Go Top button ========================//
	$(window).scroll(function() {
		if ($(this).scrollTop() > 500) {
			$('.top_button').fadeIn(500);
		} else {
			$('.top_button').fadeOut(500);
		}
	});							
	$('.top_button').click(function(event) {
		event.preventDefault();		
		$('html, body').animate({scrollTop: 0}, 800);
	});	

	/*======  curosol For Manufacture ==== */
	 var tmbrand = $("#manufacturer-carousel");
      tmbrand.owlCarousel({
     	 items : 5, //10 items above 1000px browser width
     	 itemsDesktop : [1199,4], 
     	 itemsDesktopSmall : [991,3], 
     	 itemsTablet: [767,2], 
     	 itemsMobile : [480,1] 
      });
      // Custom Navigation Events
      $(".manufacturer_next").click(function(){
        tmbrand.trigger('owl.next');
      })
      $(".manufacturer_prev").click(function(){
        tmbrand.trigger('owl.prev');
      })
	  
	 /*======  curosol For Feature PRoduct ==== */
	 var tmfeature = $("#feature-carousel");
      tmfeature.owlCarousel({
     	 items : 4, //10 items above 1000px browser width
     	 itemsDesktop : [1199,3], 
     	 itemsDesktopSmall : [991,2], 
     	 itemsTablet: [479,1], 
     	 itemsMobile : [319,1] 
      });
      // Custom Navigation Events
      $(".feature_next").click(function(){
        tmfeature.trigger('owl.next');
      })
      $(".feature_prev").click(function(){
        tmfeature.trigger('owl.prev');
      })
	  
	  /*======  Curosol For New PRoduct ==== */
	 var tmnewProduct = $("#newproduct-carousel");
      tmnewProduct.owlCarousel({
     	 items : 4, //10 items above 1000px browser width
     	 itemsDesktop : [1199,3], 
     	 itemsDesktopSmall : [991,2], 
     	 itemsTablet: [479,1], 
     	 itemsMobile : [319,1] 
      });
      // Custom Navigation Events
      $(".newproduct_next").click(function(){
        tmnewProduct.trigger('owl.next');
      })
      $(".newproduct_prev").click(function(){
        tmnewProduct.trigger('owl.prev');
      })
	  
	  
	   /*======  curosol For Accessories Product ==== */
	 var tmaccessories = $("#accessories-carousel");
      tmaccessories.owlCarousel({
     	 items : 4, //10 items above 1000px browser width
     	 itemsDesktop : [1199,3], 
     	 itemsDesktopSmall : [991,2], 
     	 itemsTablet: [479,1], 
     	 itemsMobile : [319,1] 
      });
      $(".accessories_next").click(function(){
        tmaccessories.trigger('owl.next');
      })
      $(".accessories_prev").click(function(){
        tmaccessories.trigger('owl.prev');
      })
  
	  /*======  curosol For Category Product ==== */
	 var tmproductcategory = $("#productscategory-carousel");
      tmproductcategory.owlCarousel({
     	 items : 4, //10 items above 1000px browser width
     	 itemsDesktop : [1199,3], 
     	 itemsDesktopSmall : [991,2], 
     	 itemsTablet: [479,1], 
     	 itemsMobile : [319,1] 
      });
      // Custom Navigation Events
      $(".productcategory_next").click(function(){
        tmproductcategory.trigger('owl.next');
      })
      $(".productcategory_prev").click(function(){
        tmproductcategory.trigger('owl.prev');
      })
	  
	  /*======  curosol For Crosssel Product ==== */
	 var tmcrossselling = $("#crossselling-carousel");
      tmcrossselling.owlCarousel({
     	 items : 4, //10 items above 1000px browser width
     	 itemsDesktop : [1199,3], 
     	 itemsDesktopSmall : [991,2], 
     	 itemsTablet: [479,1], 
     	 itemsMobile : [319,1] 
      });
      // Custom Navigation Events
      $(".crossselling_next").click(function(){
        tmcrossselling.trigger('owl.next');
      })
      $(".crossselling_prev").click(function(){
        tmcrossselling.trigger('owl.prev');
      })
	  
	  /*======  curosol For topseller Product ==== */

	 var tmbestseller = $("#topsellerproduct-carousel");
      tmbestseller.owlCarousel({
     	 items : 4, //10 items above 1000px browser width
     	 itemsDesktop : [1199,3], 
     	 itemsDesktopSmall : [991,2], 
     	 itemsTablet: [479,1], 
     	 itemsMobile : [319,1] 
      });

      // Custom Navigation Events
      $(".topsellerproduct_next").click(function(){
        tmbestseller.trigger('owl.next');
      })
      $(".topsellerproduct_prev").click(function(){
        tmbestseller.trigger('owl.prev');
      });

	  /*======  curosol For Tesimonial ==== */

	  var tmtestimonial = $("#testimonial_carousel");
     	 tmtestimonial.owlCarousel({
			 autoPlay: false,
			 singleItem:true
      });

		// Custom Navigation Events
      $(".tmtestimonial_next").click(function(){
        tmtestimonial.trigger('owl.next');
      });

      $(".tmtestimonial_prev").click(function(){
        tmtestimonial.trigger('owl.prev');
      });
	  
	  $('#currencies-block-top').css('display','block');
	  $('#header_links').css('display','block');
	  $('#languages-block-top').css('display','block');
	  
	  
	$('.header_user_info .tm_userinfotitle').click(function(event){
		
		$(this).toggleClass('active');
		
		event.stopPropagation();
		
		$(".user_link").slideToggle("fast");
	
	});
	
	$(".user_link").on("click", function (event) {
		
		event.stopPropagation();
	
	});
	
	$('.header_permentlink #header_links li').appendTo('.header_user_info .user_link');
	
	$('#languages-block-top').appendTo('.header_user_info .user_link');
	
	$('#currencies-block-top').appendTo('.header_user_info .user_link');
	
	$( ".header_user_info ul #languages-block-top, .header_user_info ul #currencies-block-top" ).wrapAll( "<li class='lang-curr' />");
		
		
	$('#newsletter_block_left').appendTo('.tm_newsletterdiv');
	
	
		
	// ----------------Search----------------------

	$('.search_button').click(function(event){
		
		$(this).toggleClass('active');
		
		event.stopPropagation();
		
		$(".searchtoggle").slideToggle("fast");
		
		$( "#search_query_top" ).focus();
	
	});
	
	$(".searchtoggle").on("click", function (event) {
		
		event.stopPropagation();
	
	});

});

function responsivecolumn()
{
	if ($(document).width() <= 991)
	{
		$('.container #columns_inner #left_column').appendTo('.container #columns_inner');
	}
	else if($(document).width() >= 992)
	{
		$('.container #columns_inner #left_column').prependTo('.container #columns_inner');
	}
}
$(document).ready(function(){responsivecolumn();});
$(window).resize(function(){responsivecolumn();});
// Megnor End

$(document).ready(function() {
    $('.tabs .tab-links a').on('click', function(e)  {
        var currentAttrValue = $(this).attr('href');
 
        // Show/Hide Tabs
        $('.tabs ' + currentAttrValue).show().siblings().hide();
 
        // Change/remove current tab to active
        $(this).parent('li').addClass('active').siblings().removeClass('active');
 
        e.preventDefault();
    });
});

function mainbannerZoom() {
	if ($(window).width() > 767) {
		$('#fullpage .section').each(function() {									  
			var that = $(this);
			var url = that.find('img').attr('src');
			that.css({'background-image':'url("' + url + '")'});
		});
	}
	if ($(window).width() < 767) {
		$('#fullpage .section').removeAttr('style');
	}
}
$(document).ready(function() { mainbannerZoom();});
$(window).resize(function() {mainbannerZoom();});

function mainbannerEffect(){
	if ($(window).width() > 767) {
		$('#fullpage').addClass('tmfullpage');
		$('#fullpage').fullpage({
			navigation: true,
			navigationPosition: 'right'
		});
	}
	if ($(window).width() < 767) {
		if ($('#fullpage').hasClass('tmfullpage')){
			$.fn.fullpage.destroy('all');
			$('#fullpage').removeClass('tmfullpage');
		}
	}
}
$(document).ready(function() { mainbannerEffect();});
$(window).resize(function() {mainbannerEffect();});


function toggleHomeFooter() {
	$('.footer-container .footer_hide .footer_btn span.footer_top_arrow').click(function(event){
		$(this).toggleClass('active');
		$('.footer-container .footer_hide #social_block').toggleClass("active");
		event.stopPropagation();
		$(".footer-container .hide_content").slideToggle("slow");
	});
	
	$(".footer-container .hide_content").on("click", function (event) {
		event.stopPropagation();
	});
}
$(document).ready(function() { toggleHomeFooter();});
$(window).resize(function() {
	if ($(window).width() < 767) {
		$('.footer-container .hide_content').removeAttr('style');
	}
});
