{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie8"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<!--[if gt IE 8]> <html class="no-js ie9"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<html{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}>
	<head>
		<meta charset="utf-8" />
		<title>{$meta_title|escape:'html':'UTF-8'}</title>
{if isset($meta_description) AND $meta_description}
		<meta name="description" content="{$meta_description|escape:'html':'UTF-8'}" />
{/if}
{if isset($meta_keywords) AND $meta_keywords}
		<meta name="keywords" content="{$meta_keywords|escape:'html':'UTF-8'}" />
{/if}
		<meta name="p:domain_verify" content="db120a3e83844e6f7bf6fb4212a255ad"/>
{*
	Modifications for URL canonical in products
*}
{if isset($product) }
    {if
		in_array(
        $smarty.server.REQUEST_URI,
        array(
		'/33-giselle-glitter.html',
		'/34-giselle-glitter.html',
		'/35-giselle-glitter.html'
		))
	}
	<link rel="canonical" href="https://tusstilettos.com/32-giselle-glitter.html"/>
	{elseif
		in_array(
		$smarty.server.REQUEST_URI,
		array(
		'/85-giselle-charol.html',
		'/86-giselle-charol.html',
		'/87-giselle-charol.html',
        '/38-giselle-marron.html',
		'/20-giselle-azul.html',
		'/17-giselle-bronce.html',
		'/19-giselle-kaki.html',
		'/21-giselle-mostaza.html',
		'/36-giselle-rojo.html',
		'/27-giselle-negro.html',
		'/14-giselle-serpiente.html',
		'/16-giselle-negro.html',
		'/37-giselle-naranja.html',
		'/15-giselle-plata.html',
		'/18-giselle-rosa.html',
		'/84-giselle-charol.html'
    ))
    }
	<link rel="canonical" href="https://tusstilettos.com/84-giselle-charol.html"/>
	{elseif
		in_array(
		$smarty.server.REQUEST_URI,
		array(
		'/89-keira-bicolor-blanco.html'
		))
    }
	<link rel="canonical" href="https://tusstilettos.com/88-keira-rojo.html"/>
	{elseif
		in_array(
		$smarty.server.REQUEST_URI,
		array(
		'/12-elsa-negro.html',
        '/8-elsa-marron.html',
		'/5-elsa-camel.html',
		'/10-elsa-kaki.html',
		'/11-elsa-azul.html',
		'/17-elsa-40',
		'/3-elsa-naranja.html',
		'/4-elsa-mostaza.html',
		'/9-elsa-rosa.html',
		'/13-elsa-negro.html',
		'/2-elsa-rojo.html'
    ))
    }
		<link rel="canonical" href="https://tusstilettos.com/12-elsa-negro.html"/>
    {elseif
		in_array(
		$smarty.server.REQUEST_URI,
		array(
		'/79-miranda-marron.html'
		))
    }
	<link rel="canonical" href="https://tusstilettos.com/78-miranda-marron.html"/>
	{elseif
		in_array(
		$smarty.server.REQUEST_URI,
		array(
		'/76-miranda-negro.html'
		))
    }
	<link rel="canonical" href="https://tusstilettos.com/75-miranda-negro.html"/>
	{elseif
		in_array(
		$smarty.server.REQUEST_URI,
		array(
		'/67-miranda-marino.html'
		))
    }
	<link rel="canonical" href="https://tusstilettos.com/28-miranda-marino.html"/>
	{elseif
		in_array(
		$smarty.server.REQUEST_URI,
		array(
		'/77-miranda-rojo.html'
		))
    }
	<link rel="canonical" href="https://tusstilettos.com/60-miranda-rojo.html"/>
	{elseif
		in_array(
		$smarty.server.REQUEST_URI,
		array(
		'/72-sandra-negro.html',
		'/83-sandra-charol.html',
		'/26-sandra-militar.html',
		'/82-sandra-charol.html',
		'/63-sandra-azul.html',
		'/61-sandra-rosa.html'
		))
    }
		<link rel="canonical" href="https://tusstilettos.com/26-sandra-militar.html"/>
	{elseif
		in_array(
		$smarty.server.REQUEST_URI,
		array(
		'/80-irina-terciopelo.html',
        '/64-irina-rojo.html',
		'/66-irina-negro.html',
		'/65-irina-plata.html',
		'/20-irina-100',
		'/25-irina-flores.html',
		'/71-irina-terciopelo.html',
		'/62-irina-dorado.html'
		))
    }
	<link rel="canonical" href="https://tusstilettos.com/71-irina-terciopelo.html"/>
    {elseif
		in_array(
		$smarty.server.REQUEST_URI,
		array(
		'/110-hanne-azul.html',
		'/105-hanne-fucsia.html',
		'/92-hanne-coral.html',
		'/93-hanne-verde.html'
		))
    }
		<link rel="canonical" href="https://tusstilettos.com/93-hanne-verde.html"/>
    {elseif
		in_array(
		$smarty.server.REQUEST_URI,
		array(
		'/97-kristen-verde.html',
		'/100-kristen-negro.html ',
		'/101-kristen-rosa.html ',
		'/99-kristen-azul.html ',
		'/98-kristen-mostaza.html',
		'/102-kristen-fucsia.html'
		))
    }
		<link rel="canonical" href="https://tusstilettos.com/97-kristen-verde.html"/>
	{/if}
{/if}
{if $page_name == 'prices-drop' OR $page_name == 'search' OR $page_name == 'contact' OR $page_name == 'authentication'}
	<meta name="robots" content="noindex" />
{else}
	<meta name="robots" content="{if isset($nobots)}no{/if}index,{if isset($nofollow) && $nofollow}no{/if}follow" />
{/if}
		<meta name="viewport" content="width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0" />
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<link rel="icon" type="image/vnd.microsoft.icon" href="{$favicon_url}?{$img_update_time}" />
		<link rel="shortcut icon" type="image/x-icon" href="{$favicon_url}?{$img_update_time}" />
        <link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet">
		{if isset($css_files)}
			{foreach from=$css_files key=css_uri item=media}
				{if $css_uri == 'lteIE9'}
					<!--[if lte IE 9]>
					{foreach from=$css_files[$css_uri] key=css_uriie9 item=mediaie9}
					<link rel="stylesheet" href="{$css_uriie9|escape:'html':'UTF-8'}" type="text/css" media="{$mediaie9|escape:'html':'UTF-8'}" />
					{/foreach}
					<![endif]-->
				{else}
					<link rel="stylesheet" href="{$css_uri|escape:'html':'UTF-8'}" type="text/css" media="{$media|escape:'html':'UTF-8'}" />
				{/if}
			{/foreach}
		{/if}
<!-- ================ Additional Links By Tempaltemela : START  ============= -->
<link rel="stylesheet" type="text/css" href="{$css_dir}megnor/custom.css" />
<link rel="stylesheet" type="text/css" href="{$css_dir}megnor/lightbox.css" />
<link rel="stylesheet" type="text/css" href="{$css_dir}megnor/avd.css" />
<!-- ================ Additional Links By Tempaltemela : END  ============= -->

{if isset($js_defer) && !$js_defer && isset($js_files) && isset($js_def)}
	{$js_def}
	{foreach from=$js_files item=js_uri}
	<script type="text/javascript" src="{$js_uri|escape:'html':'UTF-8'}"></script>
	{/foreach}
{/if}
<!-- ================ Additional SCRIPT By Tempaltemela : START  ============= -->
<script type="text/javascript" src="{$js_dir}megnor/owl.carousel.js"></script>
<script type="text/javascript" src="{$js_dir}megnor/custom.js"></script>
<!-- <script type="text/javascript" src="{$js_dir}megnor/parallex.js"></script> -->
<script type="text/javascript" src="{$js_dir}megnor/lightbox-2.6.min.js"></script>
<script type="text/javascript" src="{$js_dir}megnor/jquery.slimscroll.js"></script>
<script type="text/javascript" src="{$js_dir}megnor/doubletaptogo.js"></script>
<!-- ================ Additional SCRIPT By Tempaltemela : END  ============= -->
<script type="text/javascript">

       $(document).ready(function(){

    $('#alternar-panel-oculto').toggle(


        function(e){
            $('#vermenu').slideDown();
                        e.preventDefault();
        },
        function(e){
            $('#vermenu').slideUp();
             e.preventDefault();
        }

    );

});

</script>


		{$HOOK_HEADER}
		<link rel="stylesheet" href="http{if Tools::usingSecureMode()}s{/if}://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,300" type="text/css" media="all" />
		<link rel="stylesheet" href="http{if Tools::usingSecureMode()}s{/if}://fonts.googleapis.com/css?family=Raleway:400,300,700" type="text/css" media="all" />
		<link rel="stylesheet" href="http{if Tools::usingSecureMode()}s{/if}://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700" type="text/css" media="all" />
		<link rel="stylesheet" href="http{if Tools::usingSecureMode()}s{/if}://fonts.googleapis.com/css?family=Lato:400,700" type="text/css" media="all" />
		<!--[if IE 8]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->

<!-- Facebook Pixel Code -->
<script>
{literal}
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '1421117817913094');
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1"
src="https://www.facebook.com/tr?id=1421117817913094&ev=PageView
&noscript=1"/>
{/literal}
</noscript>
<!-- End Facebook Pixel Code -->
	</head>
	<body{if isset($page_name)} id="{$page_name|escape:'html':'UTF-8'}"{/if} class="{if isset($page_name)}{$page_name|escape:'html':'UTF-8'}{/if}{if isset($body_classes) && $body_classes|@count} {implode value=$body_classes separator=' '}{/if}{if $hide_left_column} hide-left-column{else} show-left-column{/if}{if $hide_right_column} hide-right-column{else} show-right-column{/if}{if isset($content_only) && $content_only} content_only{/if} lang_{$lang_iso}">
	{if !isset($content_only) || !$content_only}
		{if isset($restricted_country_mode) && $restricted_country_mode}
			<div id="restricted-country">
				<p>{l s='You cannot place a new order from your country.'}{if isset($geolocation_country) && $geolocation_country} <span class="bold">{$geolocation_country|escape:'html':'UTF-8'}</span>{/if}</p>
			</div>
		{/if}
		<div id="page">
			<div class="header-container">
				<header id="header">
					{capture name='displayBanner'}{hook h='displayBanner'}{/capture}
					{if $smarty.capture.displayBanner}
						<div class="banner">
							<div class="container">
								<div class="row">
									{$smarty.capture.displayBanner}
								</div>
							</div>
						</div>
					{/if}
					{capture name='displayNav'}{hook h='displayNav'}{/capture}
					{if $smarty.capture.displayNav}
						<div class="nav">
							<div class="container">
								<div class="row">
									<nav>
										{$smarty.capture.displayNav}
									</nav>
								</div>
							</div>
						</div>
					{/if}
					<div class="headerdiv">
						<div class="container">
							<div class="row">
								<div id="header_logo" class="col-lg-4 col-md-4  col-sm-4 col-xs-12">
									<a href="{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}" title="{$shop_name|escape:'html':'UTF-8'}">
										<img class="logo img-responsive" src="{$logo_url}" alt="{$shop_name|escape:'html':'UTF-8'}"{if isset($logo_image_width) && $logo_image_width} width="{$logo_image_width}"{/if}{if isset($logo_image_height) && $logo_image_height} height="{$logo_image_height}"{/if}/>
									</a>
								</div>
								{if isset($HOOK_TOP)}{$HOOK_TOP}{/if}
							</div>
						</div>
					</div>
				</header>
			</div>
		<div class="columns-container">
				<div id="slider_row" class="row">
                 {if $page_name == 'index'}
                 <div class="sliderlamz">
                        {hook h='slidermoda'}
                        </div>
                {/if}
                {if $page_name == 'category'}
               		<div class="container lyl-filter">
                            {if isset($alturas)}
                                {$alturas}
                            {/if}
                            {include file="./product-sort.tpl"}
                    </div>
                {/if}






                    {if $page_name == 'index'}
                        <style>
                            .title_seo {
                                font-family: Courier;
                                letter-spacing: 3vw;
                                margin: 40px 0 40px 0;
                                font-size: 15px;
                                text-align: center;
                            }
                            @media (max-width: 825px) {
                                .title_seo{
                                    letter-spacing: 2vw;

                                }
                            }


                        </style>
                        <div>
                            <h1 class="title_seo">Comprar Stilettos Online</h1>
                        </div>
                       {capture name='displayTopColumn'}{hook h='displayTopColumn'}{/capture}
                    {if $smarty.capture.displayTopColumn}
                        <div id="top_column" class="center_column col-xs-12 col-sm-12">{$smarty.capture.displayTopColumn}</div>
                    {/if}



                       <div class="container">
                       		<div class="tacones-medidas">
	<div class="col-md-2">
        <a href="#elsa40">
            <img class="img-1" src="{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}modules/pm_advancedsearch4/search_files/criterions/57a4a2ad03322.png" />

            <p class="title-tacon">4 cm</p>
        </a>
    </div>
    <div class="col-md-2">
    <a href="#miranda65">
    <img class="img-1" src="{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}modules/pm_advancedsearch4/search_files/criterions/57a4a22df24b2.png" />

            <p class="title-tacon">6.5 cm</p> </a>
    </div>
    <div class="col-md-2">
    <a href="#kate80"> <img class="img-1" src="{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}modules/pm_advancedsearch4/search_files/criterions/57a4a2321656a.png" />
     <p class="title-tacon">8 cm</p> </a>
    </div>
    <div class="col-md-2">
    <a href="#irina100"> <img class="img-1" src="{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}modules/pm_advancedsearch4/search_files/criterions/57a4a2353d6a3.png" />

            <p class="title-tacon">10 cm</p> </a>
    </div>
    <div class="col-md-4">
    <div class="container lyl-filter">
             <ul class="hidden-xs">
	<li class="display-title">{l s='View:'}</li>
    <li id="grid"><a rel="nofollow" href="#" title="{l s='Grid'}"></a></li>
    <li id="list"><a rel="nofollow" href="#" title="{l s='List'}"></a></li>
    <li id="ball"><a rel="nofollow" href="#" title="{l s='List'}"></a></li>
    <li id="eyes"><a rel="nofollow" class="iframe" href="{$link->getCMSLink('9')}?content_only=1" title="{l s='Video'}"></a></li>
    <li id="soft"><a rel="nofollow" class="iframe" href="{$link->getCMSLink('6')}?content_only=1" title="{l s='Soft+'}"></a></li>
</ul>
                    </div>
    </div>


</div>
                       </div>







                  {/if}







                </div>
                {* if $page_name !='index' && $page_name !='pagenotfound'}
					{include file="$tpl_dir./breadcrumb.tpl"}
				{/if *}
                <div id="columns" class="container">
					<div class="row" id="columns_inner">
						{if isset($left_column_size) && !empty($left_column_size)}
						<div id="left_column" class="column col-xs-12" style="width:{$left_column_size}%;">{$HOOK_LEFT_COLUMN}</div>
						{/if}
						{if isset($left_column_size) && isset($right_column_size)}{assign var='cols' value=(12 - $left_column_size - $right_column_size)}{else}{assign var='cols' value=12}{/if}
						<div id="center_column" class="center_column col-xs-12" style="width:{100 - $left_column_size - $right_column_size}%;">
	{/if}
