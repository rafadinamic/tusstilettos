<?php

// Put your MailChimp API and List ID hehe
$api_key = 'fcb473f7efba7f2d0bad4dc617c3410b-us16';
$list_id = 'fc56ddf49b';

// Let's start by including the MailChimp API wrapper
include('./inc/MailChimp.php');
// Then call/use the class
use \DrewM\MailChimp\MailChimp;
$MailChimp = new MailChimp($api_key);

// Submit subscriber data to MailChimp
// For parameters doc, refer to: http://developer.mailchimp.com/documentation/mailchimp/reference/lists/members/
// For wrapper's doc, visit: https://github.com/drewm/mailchimp-api
$result = $MailChimp->post("lists/$list_id/members", [
    'email_address' => $_POST["email"],
    'status'        => 'subscribed',
]);

if ($MailChimp->success()) {
    // Success message
    echo "success";

} else {
    echo "error";

}
