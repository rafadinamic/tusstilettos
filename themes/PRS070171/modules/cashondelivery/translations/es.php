<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{cashondelivery}prs070171>cashondelivery_1f9497d3e8bac9b50151416f04119cec'] = 'Contrareembolso';
$_MODULE['<{cashondelivery}prs070171>cashondelivery_7a3ef27eb0b1895ebf263ad7dd949fb6'] = 'Acepta pagos a contrareembolso';
$_MODULE['<{cashondelivery}prs070171>cashondelivery_b7ada96a0da7ee7fb5371cca0b036d5c'] = 'Paga a contrareembolso';
$_MODULE['<{cashondelivery}prs070171>validation_644818852b4dd8cf9da73543e30f045a'] = 'Volver al Checkout';
$_MODULE['<{cashondelivery}prs070171>validation_6ff063fbc860a79759a7369ac32cee22'] = 'Checkout';
$_MODULE['<{cashondelivery}prs070171>validation_d538c5b86e9a71455ba27412f4e9ab51'] = 'Pague a contrareembolso';
$_MODULE['<{cashondelivery}prs070171>validation_f1d3b424cd68795ecaa552883759aceb'] = 'Resumen del pedido';
$_MODULE['<{cashondelivery}prs070171>validation_ad2f6328c24caa7d25dd34bfc3e7e25a'] = 'Ha elegido pagar a contrareembolso.';
$_MODULE['<{cashondelivery}prs070171>validation_e2867a925cba382f1436d1834bb52a1c'] = 'Total';
$_MODULE['<{cashondelivery}prs070171>validation_1f87346a16cf80c372065de3c54c86d9'] = '(I.V.A. incl.)';
$_MODULE['<{cashondelivery}prs070171>validation_52f64bc0164b0e79deaeaaaa7e93f98f'] = 'Por favor, confirma tu pedido haciendo click en \'Confirmar pedido\'.';
$_MODULE['<{cashondelivery}prs070171>validation_569fd05bdafa1712c4f6be5b153b8418'] = 'Otros métodos de pago';
$_MODULE['<{cashondelivery}prs070171>validation_46b9e3665f187c739c55983f757ccda0'] = 'Confirmar pedido';
$_MODULE['<{cashondelivery}prs070171>confirmation_88526efe38fd18179a127024aba8c1d7'] = 'Su pedido %s esta completado.';
$_MODULE['<{cashondelivery}prs070171>confirmation_8861c5d3fa54b330d1f60ba50fcc4aab'] = 'Ha elegido pagar a contrareembolso.';
$_MODULE['<{cashondelivery}prs070171>confirmation_e6dc7945b557a1cd949bea92dd58963e'] = 'Su pedido se enviará en breve.';
$_MODULE['<{cashondelivery}prs070171>confirmation_0db71da7150c27142eef9d22b843b4a9'] = 'Para dudas o información, por favor póngase en contacto con nuestro';
$_MODULE['<{cashondelivery}prs070171>confirmation_64430ad2835be8ad60c59e7d44e4b0b1'] = 'servicio de atención al cliente';
$_MODULE['<{cashondelivery}prs070171>payment_b7ada96a0da7ee7fb5371cca0b036d5c'] = 'Pague a contrareembolso';
$_MODULE['<{cashondelivery}prs070171>payment_536dc7424180872c8c2488ae0286fb53'] = 'Pagar la mercancía al recibirla';
$_MODULE['<{cashondelivery}prs070171>validation_ea9cf7e47ff33b2be14e6dd07cbcefc6'] = 'Envio';
$_MODULE['<{cashondelivery}prs070171>confirmation_2e2117b7c81aa9ea6931641ea2c6499f'] = 'Tu pedido en';
$_MODULE['<{cashondelivery}prs070171>confirmation_75fbf512d744977d62599cc3f0ae2bb4'] = ' esta completado.';
