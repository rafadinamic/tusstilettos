{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if isset($orderProducts) && count($orderProducts)}
    <section id="crossselling" class="page-product-box">
		<h2 class="centertitle_block">
            {if $page_name == 'product'}
                {l s='Customers who bought this product also bought:' mod='crossselling'}
            {else}
                {l s='We recommend' mod='crossselling'}
            {/if}
        </h2>
    	<div id="crossselling_list" class="block">
			<!-- Megnor start -->
			{assign var='sliderFor' value=5} <!-- Define Number of product for SLIDER -->
			{assign var='productCount' value=count($orderProducts)}
			
			<!-- Megnor End -->
			<div class="block_content">
				<ul id="{if $productCount >= $sliderFor}crossselling-carousel{else}crossselling-grid{/if}" class="product_list {if $productCount >= $sliderFor}tm-carousel{else}grid{/if} clearfix">
					{foreach from=$orderProducts item='orderProduct' name=orderProduct}
						<li class="{if $productCount >= $sliderFor}item{else}ajax_block_product col-xs-12 col-sm-6 col-md-4 col-lg-3 {/if} product-box" itemprop="isRelatedTo" itemscope itemtype="http://schema.org/Product">
							<div class="product-container" itemtype="http://schema.org/Product" itemscope="">
								<div class="left-block">
									<div class="product-image-container">
										<a class="lnk_img product-image" href="{$orderProduct.link|escape:'html':'UTF-8'}" title="{$orderProduct.name|htmlspecialchars}" >
											<img itemprop="image" src="{$orderProduct.image}" alt="{$orderProduct.name|htmlspecialchars}" />
											{hook h="displayTmHoverImage" link_rewrite=$orderProduct.link_rewrite id_product=$orderProduct.id_product}
										</a>
									</div>
								</div>
								<div class="right-block">
									<div class="clearfix">
										{if !$PS_CATALOG_MODE && ($orderProduct.allow_oosp || $orderProduct.quantity > 0)}
											<div class="no-print button-container">
												<a class="exclusive button ajax_add_to_cart_button" href="{$link->getPageLink('cart', true, NULL, "qty=1&amp;id_product={$orderProduct.id_product|intval}&amp;token={$static_token}&amp;add")|escape:'html':'UTF-8'}" data-id-product="{$orderProduct.id_product|intval}" title="{l s='Add to cart' mod='crossselling'}"><span>{l s='Add to cart'}</span></a>
											</div>
										{/if}
									</div>
									<h5 itemprop="name">
										<a itemprop="url" href="{$orderProduct.link|escape:'html':'UTF-8'}" title="{$orderProduct.name|htmlspecialchars}" class="product-name">{$orderProduct.name|escape:'html':'UTF-8'}</a>
									</h5>
									{if $crossDisplayPrice AND $orderProduct.show_price == 1 AND !isset($restricted_country_mode) AND !$PS_CATALOG_MODE}
										<p class="price_display content_price">
											<span class="price product-price">{convertPrice price=$orderProduct.displayed_price}</span>
										</p>
									{/if}
								</div>
							</div>	
						</li>
					{/foreach}
				</ul>
			</div>
			{if $productCount >= $sliderFor}
			<div class="customNavigation">
				<a class="btn prev crossselling_prev">prev</a>
				<a class="btn next crossselling_next">next</a>
			</div>
			{/if}
		</div>
    </section>
{/if}
