{if $MENU != ''}
	<!-- Menu -->
	<div id="block_top_menu" class="sf-contener clearfix col-lg-12">
		<!--<a href="#" id="alternar-panel-oculto"><div class="cat-title" >{l s="Menu" mod="blocktopmenu"}</div></a>-->
		<a href="#" id="alternar-panel-oculto"><div class="cat-title" ></div></a>
		<ul class="sf-menu clearfix menu-content" id="vermenu"> 
			{$MENU}
			{if $MENU_SEARCH}
				<li class="sf-search noBack" style="float:right">
					<form id="searchbox" action="{$link->getPageLink('search')|escape:'html':'UTF-8'}" method="get">
						<p>
							<input type="hidden" name="controller" value="search" />
							<input type="hidden" value="position" name="orderby"/>
							<input type="hidden" value="desc" name="orderway"/>
							<input type="text" name="search_query" value="{if isset($smarty.get.search_query)}{$smarty.get.search_query|escape:'html':'UTF-8'}{/if}" />
						</p>
					</form>
				</li>
			{/if}
            <li class="right-lyl title-menu">TACÓN:</li>
			<li class="right-lyl"><a href="{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}17-elsa-40">40 </a>  </li>
            <li class="right-lyl"><a href="{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}18-miranda-65">65 </a>  </li>
            <li class="right-lyl"><a href="{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}19-kate-80">80 </a>  </li>
            <li class="right-lyl"><a href="{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}20-irina-100">100 </a>  </li>
            
		</ul>
	</div>
	<!--/ Menu -->
{/if}