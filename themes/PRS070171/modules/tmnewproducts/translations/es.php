<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{tmnewproducts}prs070171>tmnewproducts_1cd777247f2a6ed79534d4ace72d78ce'] = 'Debe rellenar el campo \"productos mostrados\"';
$_MODULE['<{tmnewproducts}prs070171>tmnewproducts_73293a024e644165e9bf48f270af63a0'] = 'Número incorrecto';
$_MODULE['<{tmnewproducts}prs070171>tmnewproducts_c888438d14855d7d96a2724ee9c306bd'] = 'configuración actualizada';
$_MODULE['<{tmnewproducts}prs070171>tmnewproducts_f4f70727dc34561dfde1a3c529b6205c'] = 'Ajustes';
$_MODULE['<{tmnewproducts}prs070171>tmnewproducts_26986c3388870d4148b1b5375368a83d'] = 'Productos para mostrar';
$_MODULE['<{tmnewproducts}prs070171>tmnewproducts_3ea7689283770958661c27c37275b89c'] = 'Define el número de productos para mostrar en este bloque.';
$_MODULE['<{tmnewproducts}prs070171>tmnewproducts_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Activado';
$_MODULE['<{tmnewproducts}prs070171>tmnewproducts_b9f5c797ebbf55adccdd8539a65a0241'] = 'Desactivado';
$_MODULE['<{tmnewproducts}prs070171>tmnewproducts_c9cc8cce247e49bae79f15173ce97354'] = 'Guardar';
