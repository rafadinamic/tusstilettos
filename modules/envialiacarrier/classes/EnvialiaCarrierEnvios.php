<?php

class EnvialiaCarrierEnvios extends ObjectModel
{
	public $id_envialia_envios;
	public $id_envio_order;
	public $codigo_envio;
	public $url_track;
	public $num_albaran;
	public $codigo_barras;
	public $fecha;

	/**
	 * @see ObjectModel::$definition
	 */
	public static $definition = array(
		'table' => 'envialia_envios',
		'primary' => 'id_envialia_envios',
		'multilang' => false,
		'fields' => array(
			'id_envialia_envios' => array(
				'type' => self::TYPE_INT, 
				'validate' => 'isUnsignedId', 
				'required' => true
			),
			'id_envio_order' => array(
				'type' => self::TYPE_INT, 
				'validate' => 'isUnsignedId', 
				'required' => true
			),
			'codigo_envio' => array('type' => self::TYPE_STRING),
			'url_track' => array('type' => self::TYPE_STRING),
			'num_albaran' => array('type' => self::TYPE_STRING),
			'codigo_barras' => array('type' => self::TYPE_STRING),
			'fecha' => array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
		),
	);

	public static function getEnvios($limit_start, $limit_end = false)
	{
		self::inicializarEnvialiaEnvios();
		
		$limit = (int)$limit_start;
		if ($limit_end)
			$limit = (int)$limit_start.','.(int)$limit_end;

	    // obtenemos todos los pedidos relacionados con ENVIALIA
	    $pedidos = Db::getInstance()->ExecuteS('
	    	SELECT o.id_order,o.module,o.total_paid_real,o.valid,o.date_add,c.name,e.*,u.firstname,u.lastname 
	    	FROM '._DB_PREFIX_.'orders o 
	        JOIN '._DB_PREFIX_.'carrier c ON c.id_carrier = o.id_carrier 
	        JOIN '._DB_PREFIX_.'envialia_envios e ON e.id_envio_order = o.id_order 
	        JOIN '._DB_PREFIX_.'customer u ON u.id_customer = o.id_customer 
	        WHERE c.external_module_name = "envialiacarrier" 
	        ORDER BY o.id_order DESC 
	        LIMIT '.$limit);

		return $pedidos;
	}

	public static function inicializarEnvialiaEnvios()
    {
    	// verificamos si hay pedidos sin registro de envio nuevo
        $envios = Db::getInstance()->ExecuteS('SELECT o.id_order FROM '._DB_PREFIX_.'orders o JOIN '._DB_PREFIX_.'carrier c ON c.id_carrier = o.id_carrier WHERE c.external_module_name = "envialiacarrier"');
        if(!$envios){
            return false;
        }
        foreach ($envios as $envio){
        	if(!Db::getInstance()->ExecuteS('SELECT id_envio_order FROM '._DB_PREFIX_.'envialia_envios where id_envio_order = "'.$envio['id_order'].'"')){
        	   Db::getInstance()->Execute('INSERT INTO '._DB_PREFIX_.'envialia_envios (id_envio_order,codigo_envio,url_track,num_albaran) VALUES ("'.$envio['id_order'].'","","","")');
        	}            
        }
        return true;        
    }    

}
