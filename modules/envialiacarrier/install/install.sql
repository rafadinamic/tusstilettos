CREATE TABLE IF NOT EXISTS `PREFIX_envialia_envios` (
  `id_envialia_envios` int(11) NOT NULL AUTO_INCREMENT,
  `id_envio_order` int(11) NOT NULL,
  `codigo_envio` varchar(50) NOT NULL,
  `url_track` varchar(255) NOT NULL,
  `num_albaran` varchar(100) NOT NULL,
  `codigo_barras` text,
  `fecha` datetime NOT NULL,
  PRIMARY KEY (`id_envialia_envios`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;