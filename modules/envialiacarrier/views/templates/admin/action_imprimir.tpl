<style>
.panel-heading .panel-heading-action a:first-child {
    display: none;
}
</style>

<a href="{$href}" class="edit btn btn-default" title="{$action}">
	<i class="icon-print"></i> {$action}
</a>