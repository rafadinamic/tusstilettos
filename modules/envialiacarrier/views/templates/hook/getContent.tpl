{if isset($confirmation)}
    {if $confirmation eq 'ok'}
<div class="alert alert-success">{l s='Módulo de ENVIALIA configurado correctamente.' mod='envialiacarrier'}</div>
    {else}
<div class="alert alert-danger">{l s='Error: Imposible conectarse a la API de ENVIALIA. Revise que los datos de conexión esten correctamente.' mod='envialiacarrier'}</div>
    {/if}
{/if}

