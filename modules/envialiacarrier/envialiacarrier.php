<?php
require_once(_PS_MODULE_DIR_.'envialiacarrier/lib/envialialog.php');
require_once(_PS_MODULE_DIR_.'envialiacarrier/lib/envialiawebservice.php');
require_once(_PS_MODULE_DIR_.'envialiacarrier/lib/envialiatools.php');
require_once(dirname(__FILE__).'/classes/EnvialiaCarrierEnvios.php');
define('ENVIALIA_VERSION','2.2');

// DEBUG
//ini_set('display_errors',1);
//error_reporting(E_ALL);
define('ENVIALIA_DEBUG',false);

class EnvialiaCarrier extends CarrierModule
{
	public $id_carrier;

	public function __construct()
	{
		$this->name = 'envialiacarrier';
		$this->tab = 'shipping_logistics';
		$this->version = ENVIALIA_VERSION;
		$this->author = 'ENVIALIA';
		$this->bootstrap = true;
		parent::__construct();
		$this->displayName = $this->l('Transportista ENVIALIA');
		$this->description = $this->l('Módulo que integra el sistema de envíos ECOMM de ENVIALIA para Prestashop 1.6.x');
	}

	public function install()
	{
		if (!parent::install())
			return false;

		if (!$this->registerHook('actionCarrierUpdate')			
			|| !$this->registerHook('displayAdminOrder')
			|| !$this->registerHook('actionOrderStatusPostUpdate'))
			return false;

		// Install admin tab
		if(!$this->installTab('AdminParentOrders', 'AdminEnvialiaCarrier', 'ENVIALIA PEDIDOS'))
			return false;

		$sql_file = dirname(__FILE__).'/install/install.sql';
		if (!$this->loadSQLFile($sql_file))
			return false;

		if (!$this->installCarriers())
			return false;

		return true;
	}

	public function uninstall()
	{
		// Call uninstall parent method
		if (!parent::uninstall())
			return false;

		// Execute module install SQL statements
		// $sql_file = dirname(__FILE__).'/install/uninstall.sql';
		// if (!$this->loadSQLFile($sql_file))
		//	return false;

		// Uninstall admin tab
		if (!$this->uninstallTab('AdminEnvialiaCarrier'))
			return false;

		// Uninstall transportistas
		if (!$this->uninstallCarriers())
			return false;

		// Delete configuration values
		//Configuration::deleteByName('MYMOD_GRADES');
		//Configuration::deleteByName('MYMOD_COMMENTS');

		// All went well!
		return true;
	}

	public function loadSQLFile($sql_file)
	{
		// Get install MySQL file content
		$sql_content = file_get_contents($sql_file);

		// Replace prefix and store MySQL command in array
		$sql_content = str_replace('PREFIX_', _DB_PREFIX_, $sql_content);
		$sql_requests = preg_split("/;\s*[\r\n]+/", $sql_content);

		// Execute each MySQL command
		$result = true;
		foreach($sql_requests AS $request)
			if (!empty($request))
				$result &= Db::getInstance()->execute(trim($request));

		return $result;
	}

	public function installTab($parent, $class_name, $name)
	{
		// Create new admin tab
		$tab = new Tab();
		$tab->id_parent = (int)Tab::getIdFromClassName($parent);
		$tab->name = array();
		foreach (Language::getLanguages(true) as $lang)
			$tab->name[$lang['id_lang']] = $name;
		$tab->class_name = $class_name;
		$tab->module = $this->name;
		$tab->active = 1;
		return $tab->add();
	}

	public function uninstallTab($class_name)
	{
		// Retrieve Tab ID
		$id_tab = (int)Tab::getIdFromClassName($class_name);

		// Load tab
		$tab = new Tab((int)$id_tab);

		// Delete it
		return $tab->delete();
	}

	public function uninstallCarriers()
	{
		$carriers_list = array(
			'ENVIALIA_24H' => 'Entrega al día siguiente',
			'ENVIALIA_72H' => 'Entrega máximo tres días',
			'ENVIALIA_EUROPE' => 'Entrega terrestre EUROPE EXPRESS',
			'ENVIALIA_WORLDWIDE' => 'Entrega aéreo WOLRDWIDE',
		);

		foreach ($carriers_list as $carrier_key => $carrier_name) 
		{
			if(ENVIALIA_DEBUG)
			{
				EnvialiaLog::info('uninstallCarriers: transportista '.$carrier_name.'='.Configuration::get($carrier_key).chr(10));
			}

			$carrier_id = Configuration::get($carrier_key);
			$carrier = new Carrier($carrier_id);
			$carrier->delete();

			// Delete configuration values
			Configuration::deleteByName($carrier_key);	
		}

		return true;
	}

	public function installCarriers()
	{
		$id_lang_default = Language::getIsoById(Configuration::get('PS_LANG_DEFAULT'));
		$carriers_list = array(
			'ENVIALIA_24H' => 'Entrega al día siguiente',
			'ENVIALIA_72H' => 'Entrega máximo tres días',
			'ENVIALIA_EUROPE' => 'Entrega terrestre EUROPE EXPRESS',
			'ENVIALIA_WORLDWIDE' => 'Entrega aéreo WOLRDWIDE',
		);
		foreach ($carriers_list as $carrier_key => $carrier_name) 
		{
			if(ENVIALIA_DEBUG)
			{
				EnvialiaLog::info('installCarriers: transportista '.$carrier_name.'='.Configuration::get($carrier_key).chr(10));
			}

			if (Configuration::get($carrier_key) < 1)
			{
				// Create new carrier
				$carrier = new Carrier();
				$carrier->name = $carrier_name;
				$carrier->id_tax_rules_group = 0;
				$carrier->active = 1;
				$carrier->deleted = 0;
				foreach (Language::getLanguages(true) as $language)
				{
					$carrier->delay[(int)$language['id_lang']] = $carrier_name;
				}
				$carrier->shipping_handling = false;
				$carrier->range_behavior = 0;
				$carrier->is_module = true;
				$carrier->shipping_external = true;
				$carrier->external_module_name = $this->name;
				$carrier->need_range = true;
				if (!$carrier->add())
					return false;

				// Associate carrier to all groups
				$groups = Group::getGroups(true);
				foreach ($groups as $group)
				{
					Db::getInstance()->insert('carrier_group', array('id_carrier' => (int)$carrier->id, 'id_group' => (int)$group['id_group']));
				}
				// Create price range
				$rangePrice = new RangePrice();
				$rangePrice->id_carrier = $carrier->id;
				$rangePrice->delimiter1 = '0';
				$rangePrice->delimiter2 = '100000';
				$rangePrice->add();

				// Create weight range
				$rangeWeight = new RangeWeight();
				$rangeWeight->id_carrier = $carrier->id;
				$rangeWeight->delimiter1 = '0';
				$rangeWeight->delimiter2 = '100000';
				$rangeWeight->add();

				// Associate carrier to all zones
				$zones = Zone::getZones(true);
				foreach ($zones as $zone)
				{
					Db::getInstance()->insert('carrier_zone', array('id_carrier' => (int)$carrier->id, 'id_zone' => (int)$zone['id_zone']));
					Db::getInstance()->insert('delivery', array('id_carrier' => (int)$carrier->id, 'id_range_price' => (int)$rangePrice->id, 'id_range_weight' => NULL, 'id_zone' => (int)$zone['id_zone'], 'price' => '0'));
					Db::getInstance()->insert('delivery', array('id_carrier' => (int)$carrier->id, 'id_range_price' => NULL, 'id_range_weight' => (int)$rangeWeight->id, 'id_zone' => (int)$zone['id_zone'], 'price' => '0'));
				}

				// Copy the carrier logo
				copy(dirname(__FILE__).'/views/img/'.$carrier_key.'.jpg', _PS_SHIP_IMG_DIR_.'/'.(int)$carrier->id.'.jpg');

				// Save the carrier ID in the Configuration table
				Configuration::updateValue($carrier_key, $carrier->id);
			}
		}
		return true;
	}

	public function getHookController($hook_name)
	{
		require_once(dirname(__FILE__).'/controllers/hook/'. $hook_name.'.php');
		$controller_name = $this->name.$hook_name.'Controller';
		$controller = new $controller_name($this, __FILE__, $this->_path);
		return $controller;
	}

	public function getContent()
	{
		$controller = $this->getHookController('getContent');
		return $controller->run();
	}

	public function getOrderShippingCost($params, $shipping_cost)
	{
		$controller = $this->getHookController('getOrderShippingCost');
		return $controller->run($params, $shipping_cost);
	}

	public function getOrderShippingCostExternal($params)
	{
		return $this->getOrderShippingCost($params, 0);
	}

	// Metodos para los Hooks o Eventos (patron observer)
	public function HookActionOrderStatusPostUpdate($params)
	{
		$controller = $this->getHookController('actionOrderStatusPostUpdate');
		return $controller->run($params);
	}

	public function hookActionCarrierUpdate($params)
	{
		$controller = $this->getHookController('actionCarrierUpdate');
		return $controller->run($params);
	}

	/*public function hookDisplayCarrierList($params)
	{
		$controller = $this->getHookController('displayCarrierList');
		return $controller->run();
	}

	public function hookDisplayAdminOrder($params)
	{
		$controller = $this->getHookController('displayAdminOrder');
		return $controller->run();
	}*/
}
