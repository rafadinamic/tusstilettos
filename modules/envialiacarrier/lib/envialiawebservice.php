<?php
class EnvialiaWebService
{	
    public static function getDataConnection()
    {
        $data = array(
            'agencia' => Configuration::get('ENVIALIA_CODIGO_AGENCIA'),
            'cliente' => Configuration::get('ENVIALIA_CODIGO_CLIENTE'),
            'pass' => Configuration::get('ENVIALIA_PASSWORD_CLIENTE'),
            'url' => Configuration::get('ENVIALIA_URL'),
        );

        return $data;
    }

	public static function getTipoServicio($id_transportista) 
	{
		if($id_transportista){
			if (Configuration::get('ENVIALIA_24H') == $id_transportista)
				 $tipo_servicio = 'E24';
			if (Configuration::get('ENVIALIA_72H') == $id_transportista)
				 $tipo_servicio = 'E72';
			if (Configuration::get('ENVIALIA_EUROPE') == $id_transportista)
				 $tipo_servicio = 'EEU';
			if (Configuration::get('ENVIALIA_WORLDWIDE') == $id_transportista)
				 $tipo_servicio = 'EWW';
		}
		else{
			// Si no conseguimos el tipo de servicio, es porque usa redsys o similar
			// Realizamos una busqueda mas profunda

			$transportista = Db::getInstance()->ExecuteS('SELECT * FROM '._DB_PREFIX_.'carrier WHERE id_carrier = '.$datos[0]['transportista_ref']);

			if(ENVIALIA_DEBUG){
			   EnvialiaLog::info("Resultado SQL de transportista por REDSYS :\n" . print_r($transportistas,true));
			}

			$lista = array(
				'Entrega al día siguiente',
				'Entrega máximo tres días',
				'Entrega terrestre EUROPE EXPRESS',
				'Entrega aéreo WOLRDWIDE');

			$mod_envialia = trim($transportista[0]['external_module_name']);
			$nombre = trim($transportista[0]['name']);

			if($mod_envialia == 'envialiacarrier'){
				if($nombre == $lista[0])
					$tipo_servicio = 'E24';
				if($nombre == $lista[1])
					$tipo_servicio = 'E72';
				if($nombre == $lista[2])
					$tipo_servicio = 'EEU';
				if($nombre == $lista[3])
					$tipo_servicio = 'EWW';
			}
		}

		return $tipo_servicio;
	}
    
	public static function getDataGrabaEnvio($id_pedido = false)
    {
        $data = array();

        if($id_pedido){
            // Vamos a por todos los datos necesarios para realizar el pedido
            $datos = Db::getInstance()->ExecuteS('
                SELECT o.id_order,o.module,o.total_paid_real,o.id_carrier,u.email,a.firstname,
                a.lastname,a.address1,a.address2,a.postcode,a.other,a.city,a.phone,a.phone_mobile,
                z.iso_code 
                FROM '._DB_PREFIX_.'orders AS o 
                JOIN '._DB_PREFIX_.'customer AS u 
                JOIN '._DB_PREFIX_.'address a 
                JOIN '._DB_PREFIX_.'country AS z 
                WHERE o.id_order = "'.$id_pedido.'" 
                AND u.id_customer = o.id_customer 
                AND a.id_address = o.id_address_delivery 
                AND a.id_country = z.id_country');
            
            // Obtenemos el tipo de servicio
			$data['tipo_servicio'] = self::getTipoServicio($datos[0]['id_carrier']);

            //Obtenemos la referencia , el peso y numero de productos
            $productos = Db::getInstance()->ExecuteS(
                'SELECT product_reference, product_quantity, product_weight FROM '._DB_PREFIX_.'order_detail 
                where id_order = "'.$id_pedido.'"');
            $peso = 0;
            $num_productos = 0;
            $ref_productos = "";
                        
            foreach ($productos as $producto){
                $peso += floatval($producto['product_quantity'] * $producto['product_weight']);
                $num_productos += $producto['product_quantity'];
                $ref_productos .= $producto['product_reference']."|";
            }
            if($peso < 1){
                $peso=1;
            }
            $data['peso'] = $peso;
            $data['ref_productos'] = $ref_productos;

            // Calculamos el numero de paquetes para envialia segun el num de articulos
            $envialia_numero_paquetes = 1;
            $bultos = Configuration::get('ENVIALIA_BULTOS');
            //bultos fijos
            if($bultos == 0){
                $num_articulos = Configuration::get('ENVIALIA_FIJO_BULTOS');
                if($num_articulos == '' || $num_articulos == 0){
                    $num_articulos = 1;
                }
                $envialia_numero_paquetes = intval($num_articulos);
            }
            
            //bultos variables
            if($bultos == 1){
                $num_articulos = Configuration::get('ENVIALIA_NUM_BULTOS');
                if($num_articulos == '' || $num_articulos == 0){
                    $num_articulos = 1;
                }
                $envialia_numero_paquetes = ceil($num_productos / $num_articulos);
            }
            $data['numero_paquetes'] = $envialia_numero_paquetes;

            // Obtenemos el num de pedido
            $data['referencia'] = sprintf('%06d', $datos[0]['id_order']);
            
            //Datos del comprador
            $data['nombre_destinatario']       = $datos[0]['firstname'].' '.$datos[0]['lastname'];
            $data['nombre_via_destinatario']   = $datos[0]['address1'];//.'- '.$datos[0]['address2'];
            $data['poblacion_destinatario']    = $datos[0]['city'];
            $data['cp_destinatario']           = $datos[0]['postcode'];
            $data['telefono_destinatario']     = trim($datos[0]['phone']);
            $data['movil_destinatario']        = trim($datos[0]['phone_mobile']);
            $data['email_destinatario']        = $datos[0]['email'];
            $envialia_pais                     = $datos[0]['iso_code'];

            // Obtenemos el comentario sobre la direccion de envio si existe
            $data['observaciones'] = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
                    SELECT `message` FROM `'._DB_PREFIX_.'message`
                    WHERE `id_order` = '.(int)$id_pedido.' ORDER BY `id_message`');


            //HAY QUE CONTROLAR SI EL COMPRADOR A ELEGIDO CONTRAREEMBOLSO Y PONERLO EN EL PARAMETRO
            $envialia_importe_servicio = $datos[0]['total_paid_real'];//importe total del pedido
            $metodo_pago = $datos[0]['module'];
            if(self::esContraReembolso($metodo_pago)){
                $envialia_reembolso=floatval($envialia_importe_servicio);
            }
            else{
                $envialia_reembolso = 0;
            }
            $data['reembolso'] = $envialia_reembolso;

            //controlar de que pais es el comprador
            if($envialia_pais == 'ES' || $envialia_pais == 'PT' || $envialia_pais == 'AD'){
                $envialia_pais = '';
            }
            $data['pais_destinatario'] = $envialia_pais;

            // Datos del vendedor o la tienda
            $vendedor = Configuration::getMultiple(array('PS_SHOP_NAME','PS_SHOP_EMAIL','PS_SHOP_ADDR1','PS_SHOP_ADDR2','PS_SHOP_CODE','PS_SHOP_CITY','PS_SHOP_COUNTRY_ID','PS_SHOP_STATE_ID','PS_SHOP_PHONE','PS_SHOP_FAX'));
            $data['nombre_remitente']          = $vendedor['PS_SHOP_NAME'];
            $data['nombre_via_remitente']      = $vendedor['PS_SHOP_ADDR1'];
            $data['poblacion_remitente']       = $vendedor['PS_SHOP_CITY'];
            $data['telefono_remitente']        = $vendedor['PS_SHOP_PHONE'];

            // comprobar si tenemos que sobreescribir el CP
            if(Configuration::get('ENVIALIA_SOBRE_CP')){
                $envialia_CP_remitente = Configuration::get('ENVIALIA_SOBRE_CP');
            }
            else{
                $envialia_CP_remitente = $vendedor['PS_SHOP_CODE'];
            }

            $data['cp_remitente'] = $envialia_CP_remitente;

            return $data;
        }

        return false;
    }

    public static function esContraReembolso($metodo_pago = null)
    {        
        $metodos = array('codfee','cashondelivery','cashondeliveryplus');
        foreach($metodos as $m) {
            if($metodo_pago == $m)
                return true;      
        }        
        return false;
    }
    
    public static function limpiarNumTrack($codigo)
    {
        $codigo = substr($codigo,1,36);
        return $codigo;
    }

    public static function grabaEnvio($id_pedido = false)
    {
        $resultado = false;

        if($id_pedido){
            $sesion = self::login();
            $dataConnection = self::getDataConnection();
            $dataGraba = self::getDataGrabaEnvio($id_pedido);
            
            $pais_destinatario = ($dataGraba['pais_destinatario'] != "") ? '<strCodPais>'.$dataGraba['pais_destinatario'].'</strCodPais>' : "";

            //Realizamos el pedido
            $peticion = '<?xml version="1.0" encoding="UTF-8"?>
                <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
                    <soap:Header>
                        <ROClientIDHeader xmlns="http://tempuri.org/">
                            <ID>'.$sesion['id_sesion'].'</ID>
                        </ROClientIDHeader>
                    </soap:Header>
                    <soap:Body>
                        <WebServService___GrabaEnvio7 xmlns="http://tempuri.org/">
                            <strCodAgeCargo>'.$dataConnection['agencia'].'</strCodAgeCargo>
                            <strCodAgeOri>'.$dataConnection['agencia'].'</strCodAgeOri>
                            <dtFecha>'.date("Y/m/d").'</dtFecha>
                            <strCodTipoServ>'.$dataGraba['tipo_servicio'].'</strCodTipoServ>
                            <strCodCli>'.$dataConnection['cliente'].'</strCodCli>

                            <strNomOri>'.$dataGraba['nombre_remitente'].'</strNomOri>
                            <strDirOri>'.$dataGraba['nombre_via_remitente'].'</strDirOri>
                            <strPobOri>'.$dataGraba['poblacion_remitente'].'</strPobOri>
                            <strCPOri>'.$dataGraba['cp_remitente'].'</strCPOri>
                            <strTlfOri>'.$dataGraba['telefono_remitente'].'</strTlfOri>

                            <strNomDes>'.$dataGraba['nombre_destinatario'].'</strNomDes>
                            <strDirDes>'.$dataGraba['nombre_via_destinatario'].'</strDirDes>
                            <strPobDes>'.$dataGraba['poblacion_destinatario'].'</strPobDes>
                            <strCPDes>'.$dataGraba['cp_destinatario'].'</strCPDes>
                            <strTlfDes>'.$dataGraba['telefono_destinatario'].'</strTlfDes>

                            <intPaq>'.$dataGraba['numero_paquetes'].'</intPaq>
                            <dPesoOri>'.floatval($dataGraba['peso']).'</dPesoOri>
                            <dReembolso>'.$dataGraba['reembolso'].'</dReembolso>
                            <strRef>'.$dataGraba['referencia'].'</strRef>
                            <strCampo4>'.$dataGraba['ref_productos'].'</strCampo4>                            
                            <strObs><![CDATA['.$dataGraba['observaciones'].']]></strObs>
                            '.$pais_destinatario.'
                            <strDesDirEmails>'.$dataGraba['email_destinatario'].'</strDesDirEmails>
                            <strDesMoviles>'.$dataGraba['movil_destinatario'].'</strDesMoviles>
                            <boInsert>'.true.'</boInsert>
                        </WebServService___GrabaEnvio7>
                    </soap:Body>
                </soap:Envelope>';

            $respuesta = self::peticionCurl($dataConnection['url'],$peticion);

            if ($respuesta){
                $xml = simplexml_load_string($respuesta);
                // hay excepciones desde el WS
                if($xml->xpath('//faultstring')){
                    foreach($xml->xpath('//faultstring') as $error){
                        EnvialiaLog::error('ENVIALIA ERROR en WS metodo grabaEnvio : '.$error);
                    }
                }
                else{
                    foreach ($xml->xpath('//v1:strAlbaranOut') as $item)
                        $envialia_num_albaran = (string)$item;
    
                    foreach ($xml->xpath('//v1:strGuidOut') as $item)
                        $envialia_num_seguimiento = (string)$item;
    
                    // primero tenemos que transformar el codigo de seguimiento
                    $cod_tracking = self::limpiarNumTrack($envialia_num_seguimiento);
                    if(!$cod_tracking){
                        EnvialiaLog::error('ENVIALIA ERROR en WS metodo grabaEnvio: limpiarNumTrack');
                    }
                    $resultado = array(
                        'num_albaran' => $envialia_num_albaran,
                        'num_seguimiento' => $envialia_num_seguimiento,
                        'url_track' => $sesion['url_track'],
                        );
                }
            }        
        }
        else{
            EnvialiaLog::error('WS metodo grabaEnvio no recibio el id_pedido'.chr(10));
        }
        
        return $resultado;
    }

    public static function etiquetaEnvio($num_albaran = false)
    {
        $resultado = false;

        if($num_albaran){
            $sesion = self::login();
            $dataConnection = self::getDataConnection();

            $peticion = '<?xml version="1.0" encoding="utf-8"?>
                    <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
                        <soap:Header>
                            <ROClientIDHeader xmlns="http://tempuri.org/">
                                <ID>'.$sesion['id_sesion'].'</ID>
                            </ROClientIDHeader>
                            </soap:Header>
                        <soap:Body>
                            <WebServService___ConsEtiquetaEnvio>
                                <strAlbaran>'.$num_albaran.'</strAlbaran>
                            </WebServService___ConsEtiquetaEnvio>
                        </soap:Body>
                    </soap:Envelope>';
            
            $respuesta = self::peticionCurl($dataConnection['url'],$peticion);

            if($respuesta){
                $xml = simplexml_load_string($respuesta);
                // hay excepciones desde el WS
                if($xml->xpath('//faultstring')){
                    foreach($xml->xpath('//faultstring') as $error){
                        EnvialiaLog::error('ENVIALIA ERROR en WS metodo etiquetaEnvio : '.$error);
                    }
                }
                else{
                    foreach ($xml->xpath('//v1:strEtiqueta') as $item){
                        $envialia_etiqueta = (string)$item;
                    }
                    $resultado = array('etiqueta' => $envialia_etiqueta);
                }
            }

        }

        return $resultado;
    }

    public static function etiquetaEnvioMasiva($envios = false)
    {
        $resultado = false;

        if($envios){
            $sesion = self::login();
            $dataConnection = self::getDataConnection();

            //preparamos el xml de los envios
            $xml_envios = '<Envios>';
            foreach ($envios as $envio) {
                $xml_envios .= '<Envio strCodAgeOri="'.$dataConnection['agencia'].'" strAlbaran="'.$envio['num_albaran'].'"/>';
            }
            $xml_envios .= '</Envios>';
        
            // Sustituye caracteres especiales para el xml
            $patron = array('<', '>', '\'', '"');
            $reemplazar = array('&lt;', '&gt;', '&apos;', '&quot;');
            $xml_envios = str_replace($patron, $reemplazar, $xml_envios);

            $peticion  = '<?xml version="1.0" encoding="utf-8"?>'.chr(10);
            $peticion .= '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">'.chr(10);
            $peticion .= '  <soap:Header>'.chr(10);
            $peticion .= '      <ROClientIDHeader xmlns="http://tempuri.org/">'.chr(10);
            $peticion .= '          <ID>'.$sesion['id_sesion'].'</ID>'.chr(10);
            $peticion .= '      </ROClientIDHeader>'.chr(10);
            $peticion .= '  </soap:Header>'.chr(10);
            $peticion .= '  <soap:Body>'.chr(10);
            $peticion .= '      <WebServService___ConsEtiquetaEnvioMasiva>'.chr(10);
            $peticion .= '          <strEnvios>'.chr(10);
            $peticion .= utf8_encode($xml_envios);
            $peticion .= '          </strEnvios>'.chr(10);
            $peticion .= '          <boPaginaA4>0</boPaginaA4>'.chr(10);
            $peticion .= '          <intNumEtiqImpresasA4>0</intNumEtiqImpresasA4>'.chr(10);
            $peticion .= '      </WebServService___ConsEtiquetaEnvioMasiva>'.chr(10);
            $peticion .= '  </soap:Body>'.chr(10);
            $peticion .= '</soap:Envelope>'.chr(10);
            
            $respuesta = self::peticionCurl($dataConnection['url'],$peticion);

            if($respuesta){
                $xml = simplexml_load_string($respuesta);
                // hay excepciones desde el WS
                if($xml->xpath('//faultstring')){
                    foreach($xml->xpath('//faultstring') as $error){
                        EnvialiaLog::error('ENVIALIA ERROR en WS metodo etiquetaEnvioMasiva : '.$error);
                    }
                }
                else{
                    foreach ($xml->xpath('//v1:strEtiqueta') as $item){
                        $envialia_etiqueta = (string)$item;
                    }
                    $resultado = array('etiqueta' => $envialia_etiqueta);
                }
            }
        }
        return $resultado;
    }

    public static function borraEnvio($num_albaran = false)
    {
        $resultado = false;

        if($num_albaran){
            $sesion = self::login();
            $dataConnection = self::getDataConnection();

            $peticion  = '<?xml version="1.0" encoding="utf-8"?>';
            $peticion .= '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
            $peticion .= '<soap:Header>';
            $peticion .= '    <ROClientIDHeader xmlns="http://tempuri.org/">';
            $peticion .= '        <ID>'.$sesion['id_sesion'].'</ID>';
            $peticion .= '    </ROClientIDHeader>';
            $peticion .= '</soap:Header>';
            $peticion .= '<soap:Body>';
            $peticion .= '    <WebServService___BorraEnvio xmlns="http://tempuri.org/">';
            $peticion .= '        <strCodAgeCargo>'.$dataConnection['agencia'].'</strCodAgeCargo>';
            $peticion .= '        <strCodAgeOri>'.$dataConnection['agencia'].'</strCodAgeOri>';
            $peticion .= '        <strAlbaran>'.$num_albaran.'</strAlbaran>';
            $peticion .= '    </WebServService___BorraEnvio>';
            $peticion .= '</soap:Body>';
            $peticion .= '</soap:Envelope>';

            $respuesta = self::peticionCurl($dataConnection['url'],$peticion);

            if($respuesta){
                $xml = simplexml_load_string($respuesta);
                // hay excepciones desde el WS
                if($xml->xpath('//v1:faultstring')){
                    foreach($xml->xpath('//v1:faultstring') as $error){
                        EnvialiaLog::error('ENVIALIA ERROR WS metodo borraEnvio: '.$error);
                    }               
                }
                else{
                    
                    foreach($xml->xpath('//v1:intCodError') as $error){

                        switch ($error){
                            case '1':
                                $error = "ERROR 1 - El envío no existe.";
                            break; 
                            case '2':
                                $error = "ERROR 2 - El usuario no tiene permiso para borrar este envío.";
                            break; 
                            case '3':
                                $error = "ERROR 3 - La fecha está fuera del rango permitido.";
                            break;

                            case '0': // No hay error
                                $error = false;
                            break;
                        }
                    
                        if($error){
                            EnvialiaLog::error('ENVIALIA ERROR WS metodo borraEnvio: '.$error);                        
                        }
                        else{
                            // todo salio bien y se cancelo el envio
                            $resultado = true;
                        }                    
                    }
                }
            }
        }
        return $resultado;
    }

    public static function login()
    {
        $data = self::getDataConnection();

        $peticion = '<?xml version="1.0" encoding="utf-8"?>
              <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
                  <soap:Body>
                     <LoginWSService___LoginCli>
                         <strCodAge>'.$data['agencia'].'</strCodAge>
                         <strCod>'.$data['cliente'].'</strCod>
                         <strPass>'.$data['pass'].'</strPass>
                     </LoginWSService___LoginCli>
                  </soap:Body>
              </soap:Envelope>';
        
        $respuesta = self::peticionCurl($data['url'],$peticion);
        $resultado = false;

        if ($respuesta){
            $xml = simplexml_load_string($respuesta);

            // hay excepciones desde el WS
            if($xml->xpath('//faultstring')){
                foreach($xml->xpath('//faultstring') as $error){
                    EnvialiaLog::error('ENVIALIA ERROR : '.$error);                    
                }                    
            }
            else{
                // obtenemos el hash de sesion
                foreach ($xml->xpath('//v1:strSesion') as $item){
                    $id_sesion_cliente = (string)$item;
                }
                // obtenemos url seguimiento necesario para el tracking
                foreach ($xml->xpath('//v1:strURLDetSegEnv') as $item){
                    $url_seguimiento = (string)$item;
                }
                $resultado = array(
                    'id_sesion' => $id_sesion_cliente,
                    'url_track' => $url_seguimiento,
                );
            }
        }
    
        return $resultado;
    }

    public static function peticionCurl($url,$request)
    {
        $url = $url."/soap";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, TRUE);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request );
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));

        $resultado = curl_exec($ch);
        
        if(ENVIALIA_DEBUG){
            EnvialiaLog::info("PETICION WS :\n" . $request);
            EnvialiaLog::info("RESPUESTA WS :\n" . $resultado);
        }

        if (curl_errno($ch)) {
            EnvialiaLog::error(curl_error($ch));
            $resultado = false;
        }

        return $resultado;
    }
}
