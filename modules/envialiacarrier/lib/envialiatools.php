<?php

class EnvialiaTools
{	

	/*
		$toOrderArray -> Array a ordenar
		$field -> Nombre del campo del array por el que queremos ordenarlo string o variable.
		$inverse -> Su valor será true o false. El valor true lo ordenará de manera descendente 
		y el false (valor por defecto) lo ordenará de manera ascendente.
	*/
    public static function orderMultiDimensionalArray ($toOrderArray, $field, $inverse = false) 
    {  
    	$position = array();  
        $newRow = array();  
        foreach ($toOrderArray as $key => $row) {  
            $position[$key]  = $row[$field];  
            $newRow[$key] = $row;  
        }  
        if ($inverse) {  
            arsort($position);  
        }  
        else {  
            asort($position);  
        }  
        $returnArray = array();  
        foreach ($position as $key => $pos) {       
            $returnArray[] = $newRow[$key];  
        }  
        return $returnArray;  
    }  

    /*
		Este metodo devuelve el importe correspondiente al servicio, si no se puede leer el 
		archivo CSV retorna falso
    */
	public static function getImporte($csvFile,$data)
	{
    	$archivo = _PS_MODULE_DIR_.'envialiacarrier/'.$csvFile;
    	$por_peso = ($csvFile == 'envialia.tarifas.csv') ? true : false;    
    	$campo = ($por_peso) ? 'peso' : 'precio_carrito';
    	$resultado = false;

		if(file_exists($archivo)){
	    	if($fp = fopen ($archivo, 'r')){
	    		$tarifas = array();
	    		while (($fila = fgetcsv ($fp ,500 ,";")) !== false) { // Mientras hay lineas que leer...
					if(($data['pais'] == $fila[1]) && ($data['servicio'] == $fila[0])){
						$tarifas[] = array(
							'servicio'      => $fila[0],
							'pais'          => $fila[1],
							'cp_origen'     => $fila[2],
							'cp_destino'    => $fila[3],
							$campo			=> $fila[4],
							'importe'       => $fila[5],
						);
					}
		    	}
	    		fclose ( $fp );
	    		// si son metodos para ES, AD, PT funcionan con el codigo postal
	    		if($data['servicio'] == 'E24' || $data['servicio'] == 'E72'){
					$rango = array();
					foreach($tarifas as $t){
						if((intval($data['cp']) >= intval($t['cp_origen'])) && (intval($data['cp']) <= intval($t['cp_destino'])) ){
							$rango[] = $t;
						}
					}

					$rangos = self::orderMultidimensionalArray($rango,$campo);
			        if(ENVIALIA_DEBUG){
			            EnvialiaLog::info("Rangos de tarifas :\n".print_r($rangos,true));
			        }

					foreach($rangos as $r){
						if($por_peso){
							if(floatval($data[$campo]) <= floatval($r[$campo])){
								$importe = $r['importe'];
								break;
							}
						}
						else{
							if(floatval($data[$campo]) <= floatval($r[$campo])){
								$importe = $r['importe'];
								break;
							}
						}
					}
					$resultado = $importe;
				}
				// europa y resto del mundo sin CP
				else{
					$rango = array();
					foreach($tarifas as $t){
						if($data['pais'] == $t['pais']){
							$rango[] = $t;
						}
					}
			        if(ENVIALIA_DEBUG){
			            EnvialiaLog::info("Rangos de tarifas EEU para el pais ".$data['pais'].":\n".print_r($tarifas,true));
			        }

					$rangos = self::orderMultidimensionalArray($rango,$campo);
			        if(ENVIALIA_DEBUG){
			            EnvialiaLog::info("Rangos de tarifas ordenadas EEU:\n".print_r($rangos,true));
			        }

					foreach($rangos as $r){
						if($por_peso){
							if(floatval($data[$campo]) <= floatval($r[$campo])){
								$importe = $r['importe'];
								break;
							}
						}
						else{
							if(floatval($data[$campo]) <= floatval($r[$campo])){
								$importe = $r['importe'];
								break;
							}
						}
					}
					$resultado = $importe;
				}
	    	}
	    	else{
			    if(ENVIALIA_DEBUG){
		            EnvialiaLog::error("Imposible abrir el archivo :\n" . $archivo);
		        }
        	}
    	}
    	else{
		    if(ENVIALIA_DEBUG){
	            EnvialiaLog::error("el archivo no existe:\n" . $archivo);
	        }
    	}

    	return $resultado;
	}

    public static function esUsuarioE24($pais){

        $paises = array('ES','PT','AD');
        foreach ($paises as $p) {
        	if($pais == $p)
        		return true;
        }
    	return false;
    }

    public static function esUsuarioE72($pais){
   		return self::esUsuarioE24($pais);
    }

    public static function esUsuarioEEU($pais){
        $paises = Array("DE","AT","BE","BG","CC","DK","SK","SI","EE","FI","FR","GR","GG",
                        "NL","HU","IE","IT","LV","LI","LT","LU","MC","NO","PL","GB","CZ",
                        "RO","SM","SE","CH","VA");
        foreach($paises as $p){
        	if($pais == $p)
        		return true;
        }
        return false;
    }

    public static function esUsuarioEWW($pais){

        $paises = array('ES','PT','AD');
        foreach ($paises as $p) {
        	if($pais == $p)
        		return false;
        }
    	return true;
    }

    public static function agregarImpuesto($pais){

        $paises = Array("AT","BE","BG","CC","CY","CZ","DK","EE","FI",
                        "FR","DE","GR","HU","IE","IT","LV","LT","LU",
                        "MT","NL","PL","PT","RO","SK","SI","ES","SE","GB");
        foreach ($paises as $p) {
            if($pais == $p){
                return true;
            }
        }
        return false;
    }


}