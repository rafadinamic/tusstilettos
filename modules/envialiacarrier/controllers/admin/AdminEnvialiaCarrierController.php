<?php
require_once(_PS_MODULE_DIR_.'envialiacarrier/lib/envialialog.php');
require_once(_PS_MODULE_DIR_.'envialiacarrier/lib/envialiawebservice.php');
require_once(_PS_MODULE_DIR_.'envialiacarrier/lib/envialiatools.php');

class AdminEnvialiaCarrierController extends ModuleAdminController
{
	public function __construct()
	{
		//revisamos si hay pedidos nuevos para envialia
		$this->inicializarEnvialiaEnvios();

		//set variables		
		$this->table = 'envialia_envios';
		$this->class = 'EnvialiaCarrierEnvios';
		$this->fields_list = array(
			'id_envialia_envios' => array('title' => $this->l('ID'), 'align' => 'center', 'width' => 25),
			'id_envio_order' => array('title' => $this->l('ID pedido'), 'align' => 'center', 'width' => 25),
			'usuario' => array('title' => $this->l('Usuario'), 'align' => 'center'),
			'precio' => array('title' => $this->l('Precio'), 'align' => 'right'),
			'fecha_pedido' => array('title' => $this->l('Fecha pedido'), 'align' => 'center'),
			'fecha' => array('title' => $this->l('Fecha envio'), 'align' => 'center'),
			'codigo_envio' => array('title' => $this->l('Codigo envio')),
			);

		//enable bootstrap
		$this->bootstrap = true;
		
		parent::__construct();

		$this->_select = 'o.id_order,o.module,o.total_paid_real AS precio,o.valid,o.date_add as fecha_pedido,c.name, CONCAT(u.firstname," ",u.lastname) AS usuario';
		$this->_join = 'JOIN '._DB_PREFIX_.'orders o ON o.id_order = a.id_envio_order JOIN '._DB_PREFIX_.'carrier c ON c.id_carrier = o.id_carrier JOIN '._DB_PREFIX_.'customer u ON u.id_customer = o.id_customer'; 
        $this->_where = 'AND c.external_module_name = "envialiacarrier" AND o.valid = 1';

		// Add actions
		$this->addRowAction('Imprimir');
		$this->addRowAction('Seguimiento');
		$this->addRowAction('Cancelar');

		// Add bulk actions - acciones agrupadas
		$this->bulk_actions = array(
			'imprimirseleccionados' => array(
				'text' => $this->l('Imprimir seleccionados'), 
				'confirm' => $this->l('¿Confirmar que quiere generar los envios seleccionados?'),
			),
			'imprimirseleccionados1pdf' => array(
				'text' => $this->l('Imprimir seleccionados en unico archivo pdf'), 
				'confirm' => $this->l('¿Confirmar que quiere imprimir los seleccionados en un único archivo pdf?'),
			),
			'cancelarseleccionados' => array(
				'text' => $this->l('Cancelar seleccionados'), 
				'confirm' => $this->l('¿Confirmar que quiere cancelar los envios seleccionados?'),
			)
		);
	}

	// Metodos para las acciones agrupadas

	protected function processBulkCancelarSeleccionados()
	{
		// obtenemos los datos del envio
        $envios = Db::getInstance()->ExecuteS('
                SELECT * FROM '._DB_PREFIX_.'envialia_envios 
                WHERE id_envialia_envios IN ('.implode(',', $this->boxes).')');

        foreach ($envios as $envio) {
			if(EnvialiaWebservice::borraEnvio($envio['num_albaran'])){
	        	// Ya cancelamos el pedido y podemos borrar su codigo de barras            	
	        	Db::getInstance()->Execute('
		        	UPDATE '._DB_PREFIX_.'envialia_envios SET  
		        	codigo_envio = "",
		        	url_track = "",
		        	num_albaran = "",
		        	codigo_barras = "",
		        	fecha = null 
		        	WHERE id_envialia_envios = "'.(int)$envio['id_envialia_envios'].'"');

	        	// eliminamos los codigos de seguimiento en la tabla de orders y order_carrier
				Db::getInstance()->Execute('
					UPDATE '._DB_PREFIX_.'orders SET shipping_number="" 
					WHERE id_order = '.(int)$envio['id_envio_order']);
				Db::getInstance()->Execute('
					UPDATE '._DB_PREFIX_.'order_carrier SET 
					tracking_number = "" 
					WHERE id_order = '.(int)$envio['id_envio_order']);
			}
        }
	}

	protected function processBulkImprimirSeleccionados()
	{
		// obtenemos los datos de todos los envios que no esten impresos
		// $this->boxes son los ids seleccionados
        $envios = Db::getInstance()->ExecuteS('
                SELECT * FROM '._DB_PREFIX_.'envialia_envios 
                WHERE id_envialia_envios IN ('.implode(',', $this->boxes).') 
                AND num_albaran = ""');
        //Tools::dieObject($envios);
        if($envios){
        	foreach ($envios as $envio) {
        		$pedido = EnvialiaWebservice::grabaEnvio($envio['id_envio_order']);
        		if($pedido){
        			$this->guardarPedido($envio['id_envio_order'],$pedido);
        			$pedido_etiqueta = EnvialiaWebservice::etiquetaEnvio($pedido['num_albaran']);
        			// si hay etiqueta guardamos el pdf
        			if($pedido_etiqueta){
        				$this->guardarEtiqueta($envio['id_envio_order'],$pedido_etiqueta);
        			}
    			}
        	}
        }
	}
	protected function processBulkImprimirSeleccionados1Pdf()
	{
		$resultado = false;

		// obtenemos los datos de todos los envios que fueron seleccionados
		// $this->boxes son los ids seleccionados
        $envios = Db::getInstance()->ExecuteS('
                SELECT * FROM '._DB_PREFIX_.'envialia_envios 
                WHERE id_envialia_envios IN ('.implode(',', $this->boxes).')');

        // antes de imprimir revisamos que todos los pedidos tengan el numero de albaran
        // si no lo tienen lo solicitamos al WS
        $albaran_nuevo = false;
        foreach ($envios as $envio) {
	    	if($envio['num_albaran'] == ""){
	    		$pedido = EnvialiaWebservice::grabaEnvio($envio['id_envio_order']);
	    		if($pedido){
	    			$this->guardarPedido($envio['id_envio_order'],$pedido);
	    			$albaran_nuevo = true;
        		}
    		}
        }

		// obtenemos los datos de todos los envios seleccionados si se crearon albaranes nuevos
		if($albaran_nuevo)
        	$envios = Db::getInstance()->ExecuteS('
                SELECT * FROM '._DB_PREFIX_.'envialia_envios 
                WHERE id_envialia_envios IN ('.implode(',', $this->boxes).')');

        // ya tenemos todos los envios con albaran, imprimimos los seleccionados en 
        // un unico archivo pdf
        if($envios){
   			$pedido_etiqueta = EnvialiaWebservice::etiquetaEnvioMasiva($envios);
			// si hay etiqueta guardamos el pdf
			if($pedido_etiqueta){				
		    	// preparamos para guardar el archivo pdf		    	
			    $nombre = "etiqueta_multiple_".time().".pdf";  // nombre unico para el archivo
		    	$ruta = _PS_MODULE_DIR_.'envialiacarrier/PDF/'.$nombre;
		    	$href = '../modules/envialiacarrier/PDF/'.$nombre;
		    	$descodificar = base64_decode($pedido_etiqueta['etiqueta']);

		    	// abrimos archivo
				if($fp = fopen($ruta,"wb+")){
					// escribimos
					if(!fwrite($fp, trim($descodificar))){	
						EnvialiaLog::error("IMPOSIBLE escribir EL ARCHIVO $ruta \n\r");
					}
					else{
				        $resultado = true;
					}
				}
				else{
					EnvialiaLog::error("IMPOSIBLE ABRIR EL ARCHIVO $ruta \n\r");
				}
				fclose($fp);
			}
        }
        if($resultado)
			Tools::redirectAdmin('index.php?controller=AdminEnvialiaCarrier&token='.$this->token.'&view=Imprimir1pdf&archivo='.$nombre);
		
		Tools::redirectAdmin('index.php?controller=AdminEnvialiaCarrier&token='.$this->token);		
	}

    
    /**
     * Assign smarty variables for all default views, list and form, then call other init functions
     * usamos este metodo como controlador de las otras vistas que no estan contempladas en el framework
     */
    public function initContent()
    {

		$vista = (isset($_GET['view'])) ? $_GET['view'] : false;
		$id_envialia_envios = (isset($_GET['id_envialia_envios'])) ? $_GET['id_envialia_envios'] : false;
		$archivo = (isset($_GET['archivo'])) ? $_GET['archivo'] : false;

        if (!$this->viewAccess()) {
            $this->errors[] = Tools::displayError('You do not have permission to view this.');
            return;
        }

        $this->getLanguages();
        $this->initToolbar();
        $this->initTabModuleList();
        $this->initPageHeaderToolbar();

		if($vista !== false) {			
			switch ($vista) {
				case 'Imprimir':
					$this->content .= $this->displayImprimir($id_envialia_envios);
					break;
				
				case 'Cancelar':
					$this->content .= $this->displayCancelar($id_envialia_envios);
					break;
				
				case 'Seguimiento':
					$this->content .= $this->displaySeguimiento($id_envialia_envios);
					break;

				case 'Imprimir1pdf':
					$this->content .= $this->displayImprimir1pdf($archivo);
					break;
			}

			$this->context->smarty->assign(array(
	            'maintenance_mode' => !(bool)Configuration::get('PS_SHOP_ENABLE'),
	            'content' => $this->content,
	            'lite_display' => $this->lite_display,
	            'url_post' => self::$currentIndex.'&token='.$this->token,
	            'show_page_header_toolbar' => $this->show_page_header_toolbar,
	            'page_header_toolbar_title' => $this->page_header_toolbar_title,
	            'title' => $this->page_header_toolbar_title,
	            'toolbar_btn' => $this->page_header_toolbar_btn,
	            'page_header_toolbar_btn' => $this->page_header_toolbar_btn
	        ));	        

        }
        else{        	
        	parent::initContent();
        	$this->context->controller->addJS(dirname(__FILE__).'/../../views/js/limpiar.js');
        }
    }

    /*
	 * Metodo para agregar el action de imprimir etiqueta
     */
    public function displayImprimirLink($token, $id)
	{
	    $tpl = $this->context->smarty->createTemplate(dirname(__FILE__).'/../../views/templates/admin/action_imprimir.tpl');
	    $tpl->assign(array(
	        'href' => 'index.php?controller=AdminEnvialiaCarrier&token='.$this->token.'&'.$this->identifier.'='.$id.'&view=Imprimir',
            'action' => $this->l('Imprimir'),
            'id' => $id
	    ));
	    return $tpl->fetch();
	}

    /*
	 * Metodo para mostrar la vista del action de imprimir etiqueta
     */
    public function displayImprimir($id_envio)
	{
		$tpl = $this->context->smarty->createTemplate(dirname(__FILE__).'/../../views/templates/admin/imprimir.tpl');

		// obtenemos los datos del envio
        $envio = Db::getInstance()->ExecuteS('
                SELECT * FROM '._DB_PREFIX_.'envialia_envios 
                WHERE id_envialia_envios = '.(int)$id_envio);
        if($envio){
        	// si no hay numero de albaran hacemos el pedido al WS y lo guardamos
        	if($envio[0]['num_albaran'] == ""){
        		$pedido = EnvialiaWebservice::grabaEnvio($envio[0]['id_envio_order']);
        		if($pedido){
        			$this->guardarPedido($envio[0]['id_envio_order'],$pedido);
        			$pedido_etiqueta = EnvialiaWebservice::etiquetaEnvio($pedido['num_albaran']);
        			if($pedido_etiqueta){
        				$download_pdf = $this->guardarEtiqueta($envio[0]['id_envio_order'],$pedido_etiqueta);
        				if($download_pdf)
        					$tpl->assign('href_download',$download_pdf);
        			}
    			}

    		}
    		else{
    			$download_pdf = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
                    SELECT `codigo_barras` FROM `'._DB_PREFIX_.'envialia_envios`
                    WHERE `id_envio_order` = '.(int)$envio[0]['id_envio_order']);
				$tpl->assign('href_download',$download_pdf);
    		}
        }
		$tpl->assign('titulo', 'Etiqueta PDF del pedido '.$envio[0]['id_envio_order']);
        $tpl->assign('href_back','index.php?controller=AdminEnvialiaCarrier&token='.$this->token);
	    return $tpl->fetch();
	}

    /*
	 * Metodo para mostrar la vista del action de imprimir multiples etiquetas en un solo pdf
     */
    public function displayImprimir1pdf($archivo)
	{
		$tpl = $this->context->smarty->createTemplate(dirname(__FILE__).'/../../views/templates/admin/imprimir1pdf.tpl');

		if($archivo){
			$download_pdf = '../modules/envialiacarrier/PDF/'.$archivo;
			$tpl->assign('href_download',$download_pdf);
		}
		$tpl->assign('titulo', 'Etiquetas PDF de los pedidos seleccionados');
        $tpl->assign('href_back','index.php?controller=AdminEnvialiaCarrier&token='.$this->token);
	    return $tpl->fetch();
	}

	protected function guardarEtiqueta($id_order,$pedido_etiqueta)
	{
    	// preparamos para guardar el archivo pdf
    	$resultado = false;
	    $nombre = "etiqueta_".$id_order.".pdf"; 
    	$ruta = _PS_MODULE_DIR_.'envialiacarrier/PDF/'.$nombre;
    	$href = '../modules/envialiacarrier/PDF/'.$nombre;
    	$descodificar = base64_decode($pedido_etiqueta['etiqueta']);

    	// abrimos archivo
		if($fp = fopen($ruta,"wb+")){
			// escribimos
			if(!fwrite($fp, trim($descodificar))){	
				EnvialiaLog::error("IMPOSIBLE escribir EL ARCHIVO $ruta \n\r");
			}
			else{
		        Db::getInstance()->Execute('
		        	UPDATE '._DB_PREFIX_.'envialia_envios SET  
		        	codigo_barras = "'.$href.'" 
		        	WHERE id_envio_order = "'.$id_order.'"');

		        $resultado = $href;
			}
		}
		else{
			EnvialiaLog::error("IMPOSIBLE ABRIR EL ARCHIVO $ruta \n\r");
		}
		fclose($fp);
        return $resultado;
	}

	protected function guardarPedido($id_order,$pedido)
	{
    	//preparamos la URL para el track
    	$fecha = date('d/m/y');
    	$cortar =  preg_split("/\?/",$pedido['url_track']);
        $url_seguimiento = $cortar[0];

        // limpiar servicio
        $patron = array('{','}');
        $reemplazar = array('','');
        $servico = str_replace($patron, $reemplazar, $pedido['num_seguimiento']);

        $enlace = $url_seguimiento."?servicio=".$servico."&fecha=".$fecha;
        
        Db::getInstance()->Execute('
        	UPDATE '._DB_PREFIX_.'envialia_envios SET  
        	codigo_envio = "'.$pedido['num_seguimiento'].'",
        	url_track = "'.$enlace.'",
        	num_albaran = "'.$pedido['num_albaran'].'",
        	fecha = "'.date('Y-m-d H:i:s').'" 
        	WHERE id_envio_order = "'.$id_order.'"');
        // guardamos el codigo de seguimiento en la tabla de orders y order_carrier
		Db::getInstance()->Execute('
			UPDATE '._DB_PREFIX_.'orders SET 
			shipping_number = "'.$this->comprimir_num_track($pedido['num_seguimiento']).'" 
			WHERE id_order = "'.$id_order.'"');
		Db::getInstance()->Execute('
			UPDATE '._DB_PREFIX_.'order_carrier SET 
			tracking_number = "'.$this->comprimir_num_track($pedido['num_seguimiento']).'" 
			WHERE id_order = "'.$id_order.'"');

	}

	protected function comprimir_num_track($codigo)
    {
    	$patron = array('{','}','-');
    	$cambiar = array('','','');
    	return str_replace($patron, $cambiar, $codigo);
    }    


    /*
	 * Metodo para agregar el action de cancelar pedido
     */
    public function displayCancelarLink($token, $id)
	{
	    $tpl = $this->context->smarty->createTemplate(dirname(__FILE__).'/../../views/templates/admin/action_cancelar.tpl');

	    // Nos aseguramos que exista numero albaran, si no hay no mostramos la opcion
		$albaran = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
                    SELECT `num_albaran` FROM `'._DB_PREFIX_.'envialia_envios`
                    WHERE `id_envialia_envios` = '.(int)$id);

	    $tpl->assign(array(
	        'href' => 'index.php?controller=AdminEnvialiaCarrier&token='.$this->token.'&'.$this->identifier.'='.$id.'&view=Cancelar',
            'action' => $this->l('Cancelar'),
            'id' => $id,
            'albaran' => $albaran,
	    ));
	    return $tpl->fetch();
	}

    /*
	 * Metodo para mostrar la vista del action cancelar pedido
     */
    public function displayCancelar($id_envio)
	{
		$tpl = $this->context->smarty->createTemplate(dirname(__FILE__).'/../../views/templates/admin/cancelar.tpl');

		// obtenemos los datos del envio
        $envio = Db::getInstance()->ExecuteS('
                SELECT * FROM '._DB_PREFIX_.'envialia_envios 
                WHERE id_envialia_envios = '.(int)$id_envio);

		if(EnvialiaWebservice::borraEnvio($envio[0]['num_albaran'])){
			$mensaje = "Envio cancelado correctamente";
        	// Ya cancelamos el pedido y podemos borrar su codigo de barras            	
        	Db::getInstance()->Execute('
	        	UPDATE '._DB_PREFIX_.'envialia_envios SET  
	        	codigo_envio = "",
	        	url_track = "",
	        	num_albaran = "",
	        	codigo_barras = "",
	        	fecha = null 
	        	WHERE id_envialia_envios = "'.(int)$id_envio.'"');

        	// eliminamos los codigos de seguimiento en la tabla de orders y order_carrier
			Db::getInstance()->Execute('
				UPDATE '._DB_PREFIX_.'orders SET shipping_number="" 
				WHERE id_order = '.(int)$envio[0]['id_envio_order']);
			Db::getInstance()->Execute('
				UPDATE '._DB_PREFIX_.'order_carrier SET 
				tracking_number = "" 
				WHERE id_order = '.(int)$envio[0]['id_envio_order']);

		}
		else{
			$mensaje = "Imposible cancelar el envio. Pongase en contacto con el area de soporte tecnico de ENVIALIA.";
		}
		$tpl->assign('titulo', 'Cancelar el envio del pedido '.$envio[0]['id_envio_order']);
		$tpl->assign('mensaje', $mensaje);
		$tpl->assign('href_back','index.php?controller=AdminEnvialiaCarrier&token='.$this->token);		
	    return $tpl->fetch();
	}

    /*
	 * Metodo para agregar el action seguimiento del envio
     */
    public function displaySeguimientoLink($token, $id)
	{
	    $tpl = $this->context->smarty->createTemplate(dirname(__FILE__).'/../../views/templates/admin/action_seguimiento.tpl');
	    
	    // Nos aseguramos que exista la url de seguimiento, si no hay no mostramos la opcion
		$albaran = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
                    SELECT `num_albaran` FROM `'._DB_PREFIX_.'envialia_envios`
                    WHERE `id_envialia_envios` = '.(int)$id);

	    $tpl->assign(array(
	        'href' => 'index.php?controller=AdminEnvialiaCarrier&token='.$this->token.'&'.$this->identifier.'='.$id.'&view=Seguimiento',
            'action' => $this->l('Seguimiento'),
            'id' => $id,
            'albaran' => $albaran,
	    ));
	    return $tpl->fetch();
	}

    /*
	 * Metodo para mostrar la vista del action de seguimiento del envio
     */
    public function displaySeguimiento($id_envio)
	{
		$tpl = $this->context->smarty->createTemplate(dirname(__FILE__).'/../../views/templates/admin/seguimiento.tpl');
		// obtenemos los datos del envio
        $envio = Db::getInstance()->ExecuteS('
                SELECT * FROM '._DB_PREFIX_.'envialia_envios 
                WHERE id_envialia_envios = '.(int)$id_envio);

		$tpl->assign('href_seguimiento',$envio[0]['url_track']);
		$tpl->assign('titulo', 'Seguimiento del envio para el pedido '.$envio[0]['id_envio_order']);
		$tpl->assign('href_back','index.php?controller=AdminEnvialiaCarrier&token='.$this->token);		

	    return $tpl->fetch();
	}	
	protected function inicializarEnvialiaEnvios()
    {
    	// verificamos si hay pedidos sin registro de envio nuevo
        $envios = Db::getInstance()->ExecuteS('SELECT o.id_order FROM '._DB_PREFIX_.'orders o JOIN '._DB_PREFIX_.'carrier c ON c.id_carrier = o.id_carrier WHERE c.external_module_name = "envialiacarrier"');
        if(!$envios){
            return false;
        }
        foreach ($envios as $envio){
        	if(!Db::getInstance()->ExecuteS('SELECT id_envio_order FROM '._DB_PREFIX_.'envialia_envios where id_envio_order = "'.$envio['id_order'].'"')){
        	   $resultado = Db::getInstance()->Execute('INSERT INTO '._DB_PREFIX_.'envialia_envios (id_envio_order,codigo_envio,url_track,num_albaran,fecha) VALUES ("'.$envio['id_order'].'","","","","0000-00-00 00:00:00")');
				if(!$resultado){
					EnvialiaLog::error("admin inicializarEnvialiaEnvios() resultado insert = ".Db::getInstance()->getMsgError());
				}
        	}            
        }
        return true;        
    }
}

