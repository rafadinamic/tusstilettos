<?php

class EnvialiaCarrierActionCarrierUpdateController
{
	public function __construct($module, $file, $path)
	{
		$this->file = $file;
		$this->module = $module;
		$this->context = Context::getContext();
		$this->_path = $path;
	}

	public function run($params)
	{
		$old_id_carrier = (int)$params['id_carrier'];
		$new_id_carrier = (int)$params['carrier']->id;
		if (Configuration::get('ENVIALIA_24H') == $old_id_carrier)
			Configuration::updateValue('ENVIALIA_24H', $new_id_carrier);
		if (Configuration::get('ENVIALIA_72H') == $old_id_carrier)
			Configuration::updateValue('ENVIALIA_72H', $new_id_carrier);
		if (Configuration::get('ENVIALIA_EUROPE') == $old_id_carrier)
			Configuration::updateValue('ENVIALIA_EUROPE', $new_id_carrier);
		if (Configuration::get('ENVIALIA_WORLDWIDE') == $old_id_carrier)
			Configuration::updateValue('ENVIALIA_WORLDWIDE', $new_id_carrier);
	}
}