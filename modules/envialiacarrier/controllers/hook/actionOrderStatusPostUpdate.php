<?php

class EnvialiaCarrierActionOrderStatusPostUpdateController
{	
	public function __construct($module, $file, $path)
	{
		$this->file = $file;
		$this->module = $module;
		$this->context = Context::getContext();
		$this->_path = $path;
	}

	protected function guardarPedido($id_order)
	{
		// obtenemos los datos del envio
        $envio = Db::getInstance()->ExecuteS('
                SELECT * FROM '._DB_PREFIX_.'envialia_envios 
                WHERE id_envio_order = '.(int)$id_order.' AND num_albaran = ""');
        if($envio){
        	// si no hay numero de albaran hacemos el pedido al WS y lo guardamos
       		$pedido = EnvialiaWebservice::grabaEnvio($envio[0]['id_envio_order']);
       		if($pedido){
		    	//preparamos la URL para el track
		    	$fecha = date('d/m/y');
		    	$cortar=  preg_split("/\?/",$pedido['url_track']);
		        $url_seguimiento=$cortar[0];
		        $enlace=$url_seguimiento."?servicio=".$pedido['num_seguimiento']."&fecha=".$fecha;
		        
		        Db::getInstance()->Execute('
		        	UPDATE '._DB_PREFIX_.'envialia_envios SET  
		        	codigo_envio = "'.$pedido['num_seguimiento'].'",
		        	url_track = "'.$enlace.'",
		        	num_albaran = "'.$pedido['num_albaran'].'",
		        	fecha = "'.date('Y-m-d H:i:s').'" 
		        	WHERE id_envio_order = "'.$id_order.'"');
		        // guardamos el codigo de seguimiento en la tabla de orders y order_carrier
				Db::getInstance()->Execute('
					UPDATE '._DB_PREFIX_.'orders SET 
					shipping_number = "'.$this->comprimir_num_track($pedido['num_seguimiento']).'" 
					WHERE id_order = "'.$id_order.'"');
				Db::getInstance()->Execute('
					UPDATE '._DB_PREFIX_.'order_carrier SET 
					tracking_number = "'.$this->comprimir_num_track($pedido['num_seguimiento']).'" 
					WHERE id_order = "'.$id_order.'"');
			}
		}
	}

	protected function comprimir_num_track($codigo)
    {
    	$patron = array('{','}','-');
    	$cambiar = array('','','');
    	return str_replace($patron, $cambiar, $codigo);
    }    


	public function run($params)
	{
		$newOrderStatus = $params['newOrderStatus'];
		$id_status = (int)$newOrderStatus->id;
		$id_pedido = $params['id_order'];

		// realizamos el pedido automaticamente
		if((int)Configuration::get('ENVIALIA_ESTADO_PEDIDO') == $id_status){
			$this->guardarPedido($id_pedido);
		}
	}
}