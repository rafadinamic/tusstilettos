<?php

class EnvialiaCarrierGetContentController
{
	public function __construct($module, $file, $path)
	{
		$this->file = $file;
		$this->module = $module;
		$this->context = Context::getContext();
		$this->_path = $path;
	}

	public function testAPIConnection()
	{
		$data = EnvialiaWebService::getDataConnection();
		$dataOk = true;
		if(empty($data['agencia']) || empty($data['cliente']) || empty($data['pass']) || empty($data['url'])){
			$dataOk = false;
		}

		if($dataOk){
			$resultado = EnvialiaWebService::login();
			if (is_array($resultado)){
				if(array_key_exists('id_sesion', $resultado)){
			        if(ENVIALIA_DEBUG){
			            EnvialiaLog::info("LA PETICION ANTERIOR VIENE DE testAPIConnection DESDE LA CONFIGURACION DEL MODULO\n");
						EnvialiaLog::info("resultado = ".print_r($resultado,true));
			        }
					return true;
				}
			}
		}
		
		return false;
	}

	public function processConfiguration()
	{
		// guardamos o actualizamos los valores de configuracion
		if (Tools::isSubmit('envialiacarrier_form'))
		{
			Configuration::updateValue('ENVIALIA_CODIGO_AGENCIA', Tools::getValue('ENVIALIA_CODIGO_AGENCIA'));
			Configuration::updateValue('ENVIALIA_CODIGO_CLIENTE', Tools::getValue('ENVIALIA_CODIGO_CLIENTE'));
			Configuration::updateValue('ENVIALIA_PASSWORD_CLIENTE', Tools::getValue('ENVIALIA_PASSWORD_CLIENTE'));
			Configuration::updateValue('ENVIALIA_URL', Tools::getValue('ENVIALIA_URL'));

			Configuration::updateValue('ENVIALIA_ENVIO_GRAT', Tools::getValue('ENVIALIA_ENVIO_GRAT'));
			Configuration::updateValue('ENVIALIA_SERVICIO_GRAT', Tools::getValue('ENVIALIA_SERVICIO_GRAT'));
			Configuration::updateValue('ENVIALIA_IMP_MIN_ENVIO_GRAT', Tools::getValue('ENVIALIA_IMP_MIN_ENVIO_GRAT'));
			Configuration::updateValue('ENVIALIA_VER_NO_GRAT', Tools::getValue('ENVIALIA_VER_NO_GRAT'));
			Configuration::updateValue('ENVIALIA_ENVIO_GRAT_INT', Tools::getValue('ENVIALIA_ENVIO_GRAT_INT'));
			Configuration::updateValue('ENVIALIA_SERVICIO_GRAT_INT', Tools::getValue('ENVIALIA_SERVICIO_GRAT_INT'));
			Configuration::updateValue('ENVIALIA_IMP_MIN_ENVIO_GRAT_INT', Tools::getValue('ENVIALIA_IMP_MIN_ENVIO_GRAT_INT'));
			Configuration::updateValue('ENVIALIA_VER_NO_GRAT_INT', Tools::getValue('ENVIALIA_VER_NO_GRAT_INT'));


			Configuration::updateValue('ENVIALIA_ECOMM_24H', Tools::getValue('ENVIALIA_ECOMM_24H'));
			Configuration::updateValue('ENVIALIA_ECOMM_72H', Tools::getValue('ENVIALIA_ECOMM_72H'));
			Configuration::updateValue('ENVIALIA_ECOMM_EUROPE_EXPRESS', Tools::getValue('ENVIALIA_ECOMM_EUROPE_EXPRESS'));
			Configuration::updateValue('ENVIALIA_ECOMM_WORLDWIDE', Tools::getValue('ENVIALIA_ECOMM_WORLDWIDE'));

			Configuration::updateValue('ENVIALIA_ESTADO_PEDIDO', Tools::getValue('ENVIALIA_ESTADO_PEDIDO'));

			Configuration::updateValue('ENVIALIA_BULTOS', Tools::getValue('ENVIALIA_BULTOS'));
			Configuration::updateValue('ENVIALIA_FIJO_BULTOS', Tools::getValue('ENVIALIA_FIJO_BULTOS'));
			Configuration::updateValue('ENVIALIA_NUM_BULTOS', Tools::getValue('ENVIALIA_NUM_BULTOS'));
			Configuration::updateValue('ENVIALIA_CALCULAR_PRECIO', Tools::getValue('ENVIALIA_CALCULAR_PRECIO'));
			Configuration::updateValue('ENVIALIA_IMPUESTO', Tools::getValue('ENVIALIA_IMPUESTO'));
			Configuration::updateValue('ENVIALIA_COSTE_FIJO_ENVIO', Tools::getValue('ENVIALIA_COSTE_FIJO_ENVIO'));
			Configuration::updateValue('ENVIALIA_MANIPULACION', Tools::getValue('ENVIALIA_MANIPULACION'));
			Configuration::updateValue('ENVIALIA_COSTE_MANIPULACION', Tools::getValue('ENVIALIA_COSTE_MANIPULACION'));
			Configuration::updateValue('ENVIALIA_MARGEN_COSTE_ENVIO', Tools::getValue('ENVIALIA_MARGEN_COSTE_ENVIO'));
			Configuration::updateValue('ENVIALIA_SOBRE_CP', Tools::getValue('ENVIALIA_SOBRE_CP'));

			// testeamos que se pueda conectar a la API de ENVIALIA
			if ($this->testAPIConnection())
				$this->context->smarty->assign('confirmation', 'ok');
			else
				$this->context->smarty->assign('confirmation', 'ko');
		}
	}

	public function renderForm()
	{
		$inputs = $this->getInputsForm();

		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->module->l('ENVIALIA E-COMM'),
					'icon' => 'icon-wrench'
				),
				'input' => $inputs,
				'submit' => array('title' => $this->module->l('Save'))
			)
		);

		$helper = new HelperForm();
		$helper->table = 'envialiacarrier';
		$helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
		$helper->allow_employee_form_lang = (int)Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG');
		$helper->submit_action = 'envialiacarrier_form';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->module->name.'&tab_module='.$this->module->tab.'&module_name='.$this->module->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => array(
				'ENVIALIA_CODIGO_AGENCIA' => Tools::getValue('ENVIALIA_CODIGO_AGENCIA', Configuration::get('ENVIALIA_CODIGO_AGENCIA')),
				'ENVIALIA_CODIGO_CLIENTE' => Tools::getValue('ENVIALIA_CODIGO_CLIENTE', Configuration::get('ENVIALIA_CODIGO_CLIENTE')),
				'ENVIALIA_PASSWORD_CLIENTE' => Tools::getValue('ENVIALIA_PASSWORD_CLIENTE', Configuration::get('ENVIALIA_PASSWORD_CLIENTE')),
				'ENVIALIA_URL' => Tools::getValue('ENVIALIA_URL', Configuration::get('ENVIALIA_URL')),

				'ENVIALIA_ENVIO_GRAT' => Tools::getValue('ENVIALIA_ENVIO_GRAT', Configuration::get('ENVIALIA_ENVIO_GRAT')),
				'ENVIALIA_SERVICIO_GRAT' => Tools::getValue('ENVIALIA_SERVICIO_GRAT', Configuration::get('ENVIALIA_SERVICIO_GRAT')),
				'ENVIALIA_IMP_MIN_ENVIO_GRAT' => Tools::getValue('ENVIALIA_IMP_MIN_ENVIO_GRAT', Configuration::get('ENVIALIA_IMP_MIN_ENVIO_GRAT')),
				'ENVIALIA_VER_NO_GRAT' => Tools::getValue('ENVIALIA_VER_NO_GRAT', Configuration::get('ENVIALIA_VER_NO_GRAT')),
				'ENVIALIA_ENVIO_GRAT_INT' => Tools::getValue('ENVIALIA_ENVIO_GRAT_INT', Configuration::get('ENVIALIA_ENVIO_GRAT_INT')),
				'ENVIALIA_SERVICIO_GRAT_INT' => Tools::getValue('ENVIALIA_SERVICIO_GRAT_INT', Configuration::get('ENVIALIA_SERVICIO_GRAT_INT')),
				'ENVIALIA_IMP_MIN_ENVIO_GRAT_INT' => Tools::getValue('ENVIALIA_IMP_MIN_ENVIO_GRAT_INT', Configuration::get('ENVIALIA_IMP_MIN_ENVIO_GRAT_INT')),
				'ENVIALIA_VER_NO_GRAT_INT' => Tools::getValue('ENVIALIA_VER_NO_GRAT_INT', Configuration::get('ENVIALIA_VER_NO_GRAT_INT')),

				'ENVIALIA_ECOMM_24H' => Tools::getValue('ENVIALIA_ECOMM_24H', Configuration::get('ENVIALIA_ECOMM_24H')),
				'ENVIALIA_ECOMM_72H' => Tools::getValue('ENVIALIA_ECOMM_72H', Configuration::get('ENVIALIA_ECOMM_72H')),
				'ENVIALIA_ECOMM_EUROPE_EXPRESS' => Tools::getValue('ENVIALIA_ECOMM_EUROPE_EXPRESS', Configuration::get('ENVIALIA_ECOMM_EUROPE_EXPRESS')),
				'ENVIALIA_ECOMM_WORLDWIDE' => Tools::getValue('ENVIALIA_ECOMM_WORLDWIDE', Configuration::get('ENVIALIA_ECOMM_WORLDWIDE')),

				'ENVIALIA_ESTADO_PEDIDO' => Tools::getValue('ENVIALIA_ESTADO_PEDIDO', Configuration::get('ENVIALIA_ESTADO_PEDIDO')),

				'ENVIALIA_BULTOS' => Tools::getValue('ENVIALIA_BULTOS', Configuration::get('ENVIALIA_BULTOS')),
				'ENVIALIA_FIJO_BULTOS' => Tools::getValue('ENVIALIA_FIJO_BULTOS', Configuration::get('ENVIALIA_FIJO_BULTOS')),
				'ENVIALIA_NUM_BULTOS' => Tools::getValue('ENVIALIA_NUM_BULTOS', Configuration::get('ENVIALIA_NUM_BULTOS')),
				'ENVIALIA_CALCULAR_PRECIO' => Tools::getValue('ENVIALIA_CALCULAR_PRECIO', Configuration::get('ENVIALIA_CALCULAR_PRECIO')),
				'ENVIALIA_IMPUESTO' => Tools::getValue('ENVIALIA_IMPUESTO', Configuration::get('ENVIALIA_IMPUESTO')),
				'ENVIALIA_COSTE_FIJO_ENVIO' => Tools::getValue('ENVIALIA_COSTE_FIJO_ENVIO', Configuration::get('ENVIALIA_COSTE_FIJO_ENVIO')),
				'ENVIALIA_MANIPULACION' => Tools::getValue('ENVIALIA_MANIPULACION', Configuration::get('ENVIALIA_MANIPULACION')),
				'ENVIALIA_COSTE_MANIPULACION' => Tools::getValue('ENVIALIA_COSTE_MANIPULACION', Configuration::get('ENVIALIA_COSTE_MANIPULACION')),
				'ENVIALIA_MARGEN_COSTE_ENVIO' => Tools::getValue('ENVIALIA_MARGEN_COSTE_ENVIO', Configuration::get('ENVIALIA_MARGEN_COSTE_ENVIO')),
				'ENVIALIA_SOBRE_CP' => Tools::getValue('ENVIALIA_SOBRE_CP', Configuration::get('ENVIALIA_SOBRE_CP')),
			),
			'languages' => $this->context->controller->getLanguages()
		);

		return $helper->generateForm(array($fields_form));
	}

	public function infoPathTarifas()
	{
		// paths de los CSV necesarios para pasarlos a la tpl
		$ruta_csv = _PS_MODULE_DIR_.'envialiacarrier/envialia.tarifas.csv';
        $ruta_csv2 = _PS_MODULE_DIR_.'envialiacarrier/envialia.tarifas.importe.carrito.csv';

		$html =  '<div class="alert alert-info">';
		$html .= '<p>Ruta de las tarifas -> '.$ruta_csv.'</p>';
		$html .= '<p>Ruta de las tarifas importe carrito -> '.$ruta_csv.'</p>';
		$html .= '</div>';		

		return $html;
	}

	public function run()
	{
		$this->processConfiguration();
		$html_confirmation_message = $this->module->display($this->file, 'getContent.tpl');
		$html_form = $this->renderForm();
		$html_info = $this->infoPathTarifas();
		return $html_confirmation_message.$html_form.$html_info;
	}

	public function getEstadosPedido()
	{
		// Get order states available on store
	    $orderStates = OrderState::getOrderStates((int)$this->context->language->id);
	    
	    $statuses_array[] = array('id_option' => 0, 'name' => 'Ninguno');

	    foreach ($orderStates as $status)
	        $statuses_array[] = array('id_option' => $status['id_order_state'], 'name' => $status['name']);

	    return $statuses_array;
	}

	public function getInputsForm()
	{
		$options_envialia_servicio_gratuito = array(
			array('id_option' => 0, 'name' => 'Ninguno'),
			array('id_option' => 'E24', 'name' => 'E-COMM 24H'),
			array('id_option' => 'E72', 'name' => 'E-COMM 72H'),
		);
		$options_envialia_servicio_gratuito_internacional = array(
			array('id_option' => 0, 'name' => 'Ninguno'),
			array('id_option' => 'EEU', 'name' => 'E-COMM EUROPE EXPRESS'),
			array('id_option' => 'EWW', 'name' => 'E-COMM WORLDWIDE'),
		);
		$options_envialia_manipulacion = array(
			array('id_option' => 0, 'name' => 'Ninguno'),
			array('id_option' => 'F', 'name' => 'Fijo'),
			array('id_option' => 'V', 'name' => 'Variable'),
		);
		$options_envialia_estado_pedido = $this->getEstadosPedido();

		$inputs = array(
			// configuracion conexion WS
			array('name' => 'ENVIALIA_CODIGO_AGENCIA', 'label' => $this->module->l('Codigo Agencia'), 'type' => 'text', 'required' => true, 'desc' => $this->module->l('Código del centro de servicio ENVIALIA')),
			array('name' => 'ENVIALIA_CODIGO_CLIENTE', 'label' => $this->module->l('Codigo Cliente'), 'type' => 'text', 'required' => true, 'desc' => $this->module->l('Código de la cuenta de ENVIALIA')),
			array('name' => 'ENVIALIA_PASSWORD_CLIENTE', 'label' => $this->module->l('Password Cliente'), 'type' => 'text', 'required' => true, 'desc' => $this->module->l('Password de la cuenta de ENVIALIA')),
			array('name' => 'ENVIALIA_URL', 'label' => $this->module->l('URL WebService'), 'type' => 'text', 'required' => true, 'desc' => $this->module->l('Servidor API ENVIALIA, solicítalo en tu centro de servicio ENVIALIA (telf. 902-400909)')),
			// configuracion envios gratuitos para ES,PT,AD
			array(
				'type' => 'radio',
				'name' => 'ENVIALIA_ENVIO_GRAT',
				'label' => $this->module->l('Habilitar envio Gratuito (ES,PT,AD)'),
				'desc' => $this->module->l('Habilita el envío gratuito a los siguientes paises: ES-PT-AD.'),				
				'is_bool' => true,
				'values'  => array(
					array(
						'id' => 'envio_gratuito_no',
						'value' => 0,
						'label' => $this->module->l('No'),
					),
					array(
						'id' => 'envio_gratuito_si',
						'value' => 1,
						'label' => $this->module->l('Si'),
					),
				)					
			),
			array(
				'type' => 'select',
				'label' => $this->module->l('Servicio para envio Gratuito (ES,PT,AD):'),
			  	'desc' => $this->module->l('Servicio para envío gratuito a ES-PT-AD'),
			  	'name' => 'ENVIALIA_SERVICIO_GRAT',
			  	'options' => array(
			    	'query' => $options_envialia_servicio_gratuito,
			    	'id' => 'id_option',
			    	'name' => 'name'
			  	)
			),
			array(
				'type' => 'text',
				'name' => 'ENVIALIA_IMP_MIN_ENVIO_GRAT',
				'label' => $this->module->l('Importe mínimo para envíos gratuitos (ES,PT,AD)'),				
				'desc' => $this->module->l('Importe mínimo de pedido para envío gratuito a ES-PT-AD')
			),			
			array(
				'type' => 'radio',
				'name' => 'ENVIALIA_VER_NO_GRAT',
				'label' => $this->module->l('Visualizar envios NO gratuitos (ES,PT,AD)'),
				'desc' => $this->module->l('Visualizar también los envios no gratuitos para los siguientes paises: ES-PT-AD.'),
				'is_bool' => true,
				'values'  => array(
					array(
						'id' => 'ver_gratuito_no',
						'value' => 0,
						'label' => $this->module->l('No'),
					),
					array(
						'id' => 'ver_gratuito_si',
						'value' => 1,
						'label' => $this->module->l('Si'),
					),
				)					
			),
			// configuracion envios gratuitos Internacionales
			array(
				'type' => 'radio',
				'name' => 'ENVIALIA_ENVIO_GRAT_INT',
				'label' => $this->module->l('Habilitar envío gratuito INTERNACIONAL'),
				'desc' => $this->module->l('Habilita el envío gratuito INTERNACIONAL.'),
				'is_bool' => true,
				'values'  => array(
					array(
						'id' => 'envio_gratuito_internacional_no',
						'value' => 0,
						'label' => $this->module->l('No'),
					),
					array(
						'id' => 'envio_gratuito_internacional_si',
						'value' => 1,
						'label' => $this->module->l('Si'),
					),
				)					
			),
			array(
				'type' => 'select',
				'label' => $this->module->l('Servicio para envio gratuito INTERNACIONAL:'),
			  	'desc' => $this->module->l('Servicio para envio gratuito INTERNACIONAL.'),
			  	'name' => 'ENVIALIA_SERVICIO_GRAT_INT',
			  	'options' => array(
			    	'query' => $options_envialia_servicio_gratuito_internacional,
			    	'id' => 'id_option',
			    	'name' => 'name'
			  	)
			),
			array(
				'type' => 'text',
				'name' => 'ENVIALIA_IMP_MIN_ENVIO_GRAT_INT',
				'label' => $this->module->l('Importe mínimo para envíos gratuitos INTERNACIONALES:'),
				'desc' => $this->module->l('Importe mínimo de pedido para envío gratuito INTERNACIONAL.')
			),
			array(
				'type' => 'radio',
				'name' => 'ENVIALIA_VER_NO_GRAT_INT',
				'label' => $this->module->l('Visualizar envios NO gratuitos INTERNACIONAL:'),
				'desc' => $this->module->l('Visualizar también los envios no gratuitos internacionales.'),
				'is_bool' => true,
				'values'  => array(
					array(
						'id' => 'ver_gratuito_no_int',
						'value' => 0,
						'label' => $this->module->l('No'),
					),
					array(
						'id' => 'ver_gratuito_si_int',
						'value' => 1,
						'label' => $this->module->l('Si'),
					),
				)
			),
			// configuracion para los servicios NO GRATUITOS
			array(
				'type' => 'radio',
				'name' => 'ENVIALIA_ECOMM_24H',
				'label' => $this->module->l('Habilitar servicio E-COMM 24H:'),
				'desc' => $this->module->l('Habilita método de envío 24 horas. Este método solo está disponible para destinos ESPAÑA-PORTUGAL-ANDORRA.'),
				'is_bool' => true,
				'values'  => array(
					array(
						'id' => 'envialia_servicio_e24',
						'value' => 0,
						'label' => $this->module->l('No'),
					),
					array(
						'id' => 'envialia_servicio_e24',
						'value' => 'E24',
						'label' => $this->module->l('Si'),
					),
				)
			),
			array(
				'type' => 'radio',
				'name' => 'ENVIALIA_ECOMM_72H',
				'label' => $this->module->l('Habilitar servicio E-COMM 72H:'),
				'desc' => $this->module->l('Habilita método de envío 72 horas. Este método solo está disponible para destinos ESPAÑA-PORTUGAL-ANDORRA.'),
				'is_bool' => true,
				'values'  => array(
					array(
						'id' => 'envialia_servicio_e72',
						'value' => 0,
						'label' => $this->module->l('No'),
					),
					array(
						'id' => 'envialia_servicio_e72',
						'value' => 'E72',
						'label' => $this->module->l('Si'),
					),
				)
			),
			array(
				'type' => 'radio',
				'name' => 'ENVIALIA_ECOMM_EUROPE_EXPRESS',
				'label' => $this->module->l('Habilitar servicio E-COMM EUROPE EXPRESS:'),
				'desc' => $this->module->l('Habilita método de envío a Europa. Este servicio está disponible SOLO PARA LOS PAISES de destino DE-AT-BE-BG-CC-DK-SK-SI-EE-FI-FR-GR-GG-NL-HU-IE-IT-LV-LI-LT-LU-MC-NO-PL-GB-CZ-RO-SM-SE-CH-VA.'),
				'is_bool' => true,
				'values'  => array(
					array(
						'id' => 'envialia_servicio_eeu',
						'value' => 0,
						'label' => $this->module->l('No'),
					),
					array(
						'id' => 'envialia_servicio_eeu',
						'value' => 'EEU',
						'label' => $this->module->l('Si'),
					),
				)
			),
			array(
				'type' => 'radio',
				'name' => 'ENVIALIA_ECOMM_WORLDWIDE',
				'label' => $this->module->l('Habilitar servicio E-COMM WORLDWIDE:'),
				'desc' => $this->module->l('Habilita método de envío a todo el mundo. Este método está disponible para TODOS LOS PAISES de destino salvo ESPAÑA-PORTUGAL-ANDORRA.'),
				'is_bool' => true,
				'values'  => array(
					array(
						'id' => 'envialia_servicio_eww',
						'value' => 0,
						'label' => $this->module->l('No'),
					),
					array(
						'id' => 'envialia_servicio_eww',
						'value' => 'EWW',
						'label' => $this->module->l('Si'),
					),
				)
			),
			// configuracion para imprimir etiquetas automaticamente segun el estado del pedido
			array(
				'type' => 'select',
				'label' => $this->module->l('Imprimir etiqueta si el estado de pedido es:'),
			  	'desc' => $this->module->l('Realiza el pedido del envio a ENVIALIA automaticamente segun el estado del pedido configurado.'),
			  	'name' => 'ENVIALIA_ESTADO_PEDIDO',
			  	'options' => array(
			    	'query' => $options_envialia_estado_pedido,
			    	'id' => 'id_option',
			    	'name' => 'name'
			  	)
			),
			// configuracion para el manejo de los bultos			
			array(
				'type' => 'radio',
				'name' => 'ENVIALIA_BULTOS',
				'label' => $this->module->l('Bultos por envío:'),
				'desc' => $this->module->l('Configuración de bultos por envío fijo o variable según numero de artículos.'),
				'is_bool' => true,
				'values'  => array(
					array(
						'id' => 'envialia_bultos',
						'value' => 0,
						'label' => $this->module->l('Fijo'),
					),
					array(
						'id' => 'envialia_bultos',
						'value' => 'EWW',
						'label' => $this->module->l('Variable'),
					),
				)
			),
			array(
				'type' => 'text',
				'name' => 'ENVIALIA_FIJO_BULTOS',
				'label' => $this->module->l('Número de artículos por bultos fijo:'),
				'desc' => $this->module->l('Indique el número de artículos por bulto.')
			),
			array(
				'type' => 'text',
				'name' => 'ENVIALIA_NUM_BULTOS',
				'label' => $this->module->l('Número de artículos por bultos variable:'),
				'desc' => $this->module->l('Indique el número de artículos por bulto.')
			),
			// configuracion de impuestos, manipulacion y otros...
			array(
				'type' => 'radio',
				'name' => 'ENVIALIA_CALCULAR_PRECIO',
				'label' => $this->module->l('Calcular precio de envio por:'),
				'desc' => $this->module->l('Calcula el precio del envio por el peso total o por el precio del carrito de compras del usuario.'),
				'is_bool' => true,
				'values'  => array(
					array(
						'id' => 'envialia_precio_por',
						'value' => 0,
						'label' => $this->module->l('Peso'),
					),
					array(
						'id' => 'envialia_precio_por',
						'value' => 1,
						'label' => $this->module->l('Importe carrito'),
					),
				)
			),
			array(
				'type' => 'text',
				'name' => 'ENVIALIA_IMPUESTO',
				'label' => $this->module->l('Impuesto agregado:'),
				'desc' => $this->module->l('Impuesto agregado. Modo de uso, si quiere poner un 18% deberá escribir 0.18')
			),
			array(
				'type' => 'text',
				'name' => 'ENVIALIA_COSTE_FIJO_ENVIO',
				'label' => $this->module->l('Coste fijo de envío:'),
				'desc' => $this->module->l('Precio fijo por envío.')
			),
			array(
				'type' => 'select',
				'label' => $this->module->l('Manipulación:'),
			  	'desc' => $this->module->l('Sistema de cálculo para el cargo de manipulación.'),
			  	'name' => 'ENVIALIA_MANIPULACION',
			  	'options' => array(
			    	'query' => $options_envialia_manipulacion,
			    	'id' => 'id_option',
			    	'name' => 'name'
			  	)
			),
			array(
				'type' => 'text',
				'name' => 'ENVIALIA_COSTE_MANIPULACION',
				'label' => $this->module->l('Coste de manipulación:'),
				'desc' => $this->module->l('Coste manipulación. (0 = sin coste).')
			),
			array(
				'type' => 'text',
				'name' => 'ENVIALIA_MARGEN_COSTE_ENVIO',
				'label' => $this->module->l('Margen sobre coste de envío:'),
				'desc' => $this->module->l('Margen de incremento sobre tarifa de coste de envío.')
			),
			array(
				'type' => 'text',
				'name' => 'ENVIALIA_SOBRE_CP',
				'label' => $this->module->l('Sobrescribir Código Postal Origen:'),
				'desc' => $this->module->l('Escriba el Código Postal para sobreescribir el existente.')
			),
		);

		return $inputs;
	}
}
