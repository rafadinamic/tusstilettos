<?php

class EnvialiaCarrierGetOrderShippingCostController
{
	public function __construct($module, $file, $path)
	{
		$this->file = $file;
		$this->module = $module;
		$this->context = Context::getContext();
		$this->_path = $path;
	}

	public function _getImporteTotal($data)
	{
		$importe = 0;
		$subimporte = 0;

    	//obtenemos el tipo de tarifas 0=por peso 1=por carrito
    	$csvFile = (Configuration::get('ENVIALIA_CALCULAR_PRECIO')) ? 'envialia.tarifas.importe.carrito.csv' : 'envialia.tarifas.csv';

		//Hay que agregar el IVA???
		$iva = 0;
		if(EnvialiaTools::agregarImpuesto($data['pais'])){
			$iva = Configuration::get('ENVIALIA_IMPUESTO');
		}
		
		//Preparamos los parametros adicionales
	    if(Configuration::get('ENVIALIA_COSTE_FIJO_ENVIO')){
	    	$coste_fijo_envio = floatval(Configuration::get('ENVIALIA_COSTE_FIJO_ENVIO'));
	    }

	    $coste_manipulacion = 0;
	    if(Configuration::get('ENVIALIA_MANIPULACION') == 'F'){
	        $coste_manipulacion = floatval(Configuration::get('ENVIALIA_COSTE_MANIPULACION'));
	    }
	    if(Configuration::get('ENVIALIA_MANIPULACION') == 'V'){
	        $coste_manipulacion = floatval($data['precio_carrito']*(Configuration::get('ENVIALIA_COSTE_MANIPULACION')/100));
	    }

	    if(Configuration::get('ENVIALIA_MARGEN_COSTE_ENVIO')){
	        $coste_margen = floatval(Configuration::get('ENVIALIA_MARGEN_COSTE_ENVIO'));
	    }

	    // Sumamos los costes adicionales, margenes e iva , si estan configurados en el módulo

		if(Configuration::get('ENVIALIA_COSTE_FIJO_ENVIO')){
			$subimporte = $coste_fijo_envio;
			$coste_envio = $coste_fijo_envio + ($coste_fijo_envio * $iva);
			$importe = floatval($coste_envio + $coste_manipulacion);
		}
		else{
			//Si no hay coste_fijo buscamos en el csv el precio
			//necesitamos el tipo_servicio, cp_cliente, pais y peso
			$coste_envio = EnvialiaTools::getImporte($csvFile,$data);
			$subimporte = $coste_envio;

			//Sumamos el MARGEN SOBRE COSTE DE ENVIO
			if(Configuration::get('ENVIALIA_MARGEN_COSTE_ENVIO')){
				$coste_envio = $coste_envio+$coste_margen;
			}
			$coste_envio = $coste_envio +($coste_envio*$iva);
			$importe = floatval($coste_envio+$coste_manipulacion);
		}
		//formateamos el importe
		$importe = number_format($importe,2,".","");
		
		return (float)$importe;
	}

	// metodo de envio 24 horas
	public function getShippingCostEnvialia24h($data)
	{
		$data['servicio'] = 'E24';
		$precio_envio = false;

		if(EnvialiaTools::esUsuarioE24($data['pais'])){
			// si es gratuito
			if(Configuration::get('ENVIALIA_ENVIO_GRAT') && 
				$data['precio_carrito'] > Configuration::get('ENVIALIA_IMP_MIN_ENVIO_GRAT')){
		
				// E24 es gratuito
				if(Configuration::get('ENVIALIA_SERVICIO_GRAT') == 'E24'){
					$precio_envio = 0;					
				}
			
				// Esta habilitado ver resto de servicios y E24 no es el seleccinado
				if(Configuration::get('ENVIALIA_VER_NO_GRAT') && 
					Configuration::get('ENVIALIA_SERVICIO_GRAT') == 'E72'){
					$precio_envio = $this->_getImporteTotal($data);					
				}
			}
			else{
				if(Configuration::get('ENVIALIA_ECOMM_24H')){
					$precio_envio = $this->_getImporteTotal($data);
				}
			}
		}

		return $precio_envio;
	}

	// metodo envio 72 horas
	public function getShippingCostEnvialia72h($data)
	{
		$data['servicio'] = 'E72';
		$precio_envio = false;   	

		if(EnvialiaTools::esUsuarioE72($data['pais'])){
			// si es gratuito
			if(Configuration::get('ENVIALIA_ENVIO_GRAT') && 
				$data['precio_carrito'] > Configuration::get('ENVIALIA_IMP_MIN_ENVIO_GRAT')){
		
				// E24 es gratuito
				if(Configuration::get('ENVIALIA_SERVICIO_GRAT') == 'E72'){
					$precio_envio = 0;					
				}
			
				// Esta habilitado ver resto de servicios y E24 no es el seleccinado
				if(Configuration::get('ENVIALIA_VER_NO_GRAT') && 
					Configuration::get('ENVIALIA_SERVICIO_GRAT') == 'E24'){
					$precio_envio = $this->_getImporteTotal($data);					
				}
			}
			else{
				if(Configuration::get('ENVIALIA_ECOMM_72H')){
					$precio_envio = $this->_getImporteTotal($data);
				}
			}
		}
		return $precio_envio;
	}

	// metodo envio para europa
	public function getShippingCostEnvialiaEurope($data)
	{
		$data['servicio'] = 'EEU';
		$precio_envio = false;   	

		if(EnvialiaTools::esUsuarioEEU($data['pais'])){
			// si es gratuito
			if(Configuration::get('ENVIALIA_ENVIO_GRAT_INT') && 
				$data['precio_carrito'] > Configuration::get('ENVIALIA_IMP_MIN_ENVIO_GRAT_INT')){
		
				// EEU es gratuito
				if(Configuration::get('ENVIALIA_SERVICIO_GRAT_INT') == 'EEU'){
					$precio_envio = 0;					
				}
			
				// Esta habilitado ver resto de servicios y EEU no es el seleccinado
				if(Configuration::get('ENVIALIA_VER_NO_GRAT_INT') && 
					Configuration::get('ENVIALIA_SERVICIO_GRAT_INT') == 'EWW'){
					$precio_envio = $this->_getImporteTotal($data);					
				}
			}
			else{
				if(Configuration::get('ENVIALIA_ECOMM_EUROPE_EXPRESS')){
					$precio_envio = $this->_getImporteTotal($data);
				}
			}
		}
		return $precio_envio;

	}

	// metodo envio para resto del mundo
	public function getShippingCostEnvialiaWorldwide($data)
	{
		$data['servicio'] = 'EWW';
		$precio_envio = false;   	

		if(EnvialiaTools::esUsuarioEWW($data['pais'])){
			// si es gratuito
			if(Configuration::get('ENVIALIA_ENVIO_GRAT_INT') && 
				$data['precio_carrito'] > Configuration::get('ENVIALIA_IMP_MIN_ENVIO_GRAT_INT')){
		
				// EEU es gratuito
				if(Configuration::get('ENVIALIA_SERVICIO_GRAT_INT') == 'EWW'){
					$precio_envio = 0;					
				}
			
				// Esta habilitado ver resto de servicios y EEU no es el seleccinado
				if(Configuration::get('ENVIALIA_VER_NO_GRAT_INT') && 
					Configuration::get('ENVIALIA_SERVICIO_GRAT_INT') == 'EEU'){
					$precio_envio = $this->_getImporteTotal($data);					
				}
			}
			else{
				if(Configuration::get('ENVIALIA_ECOMM_WORLDWIDE')){
					$precio_envio = $this->_getImporteTotal($data);
				}
			}
		}
		return $precio_envio;
	}

	public function getShippingCost($id_carrier, $data)
	{
		$shipping_cost = false;

		if ($id_carrier == Configuration::get('ENVIALIA_24H'))
			$shipping_cost = $this->getShippingCostEnvialia24h($data);
		if ($id_carrier == Configuration::get('ENVIALIA_72H'))
			$shipping_cost = $this->getShippingCostEnvialia72h($data);
		if ($id_carrier == Configuration::get('ENVIALIA_EUROPE'))
			$shipping_cost = $this->getShippingCostEnvialiaEurope($data);
		if ($id_carrier == Configuration::get('ENVIALIA_WORLDWIDE'))
			$shipping_cost = $this->getShippingCostEnvialiaWorldwide($data);
		
		return $shipping_cost;
	}

	public function loadData($cart)
	{
		$address = new Address($cart->id_address_delivery);
		//$customer = new Customer($cart->id_customer);

		$query = 'SELECT iso_code FROM '._DB_PREFIX_.'country where id_country = "'.$address->id_country.'"';
		$resultado = Db::getInstance()->ExecuteS($query);
		$usuario_pais = ($resultado) ? $resultado[0]['iso_code'] : '';

		$data = array(
			'cp' => $address->postcode, // codigo postal comprador
			'pais' => $usuario_pais,	// pais comprador
			'precio_carrito' => $cart->getOrderTotal(true, Cart::BOTH_WITHOUT_SHIPPING), // importe carrito
			'peso' => ($cart->getTotalWeight() < 1) ? 1 : $cart->getTotalWeight(), // peso carrito
		);
		
		if(ENVIALIA_DEBUG){
			//EnvialiaLog::info("Datos usuario : \n".print_r($data,true));
		}
		return $data;
	}

	public function run($cart, $shipping_fees)
	{
		$shipping_cost = false;

		$data = $this->loadData($cart);		
		if(!empty($data['cp']) && !empty($data['pais']))
			$shipping_cost = $this->getShippingCost($this->module->id_carrier, $data);

		if ($shipping_cost === false)
			return false;

		return $shipping_cost + $shipping_fees;
	}
}
