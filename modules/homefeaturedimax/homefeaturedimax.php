<?php

/*
 * 2007-2012 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author PrestaShop SA <contact@prestashop.com>
 *  @copyright  2007-2012 PrestaShop SA
 *  @version  Release: $Revision: 7048 $
 *  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_'))
    exit;

class HomeFeaturedImax extends Module {

    private $_html = '';
    private $_postErrors = array();

    function __construct() {
        $this->name = 'homefeaturedimax';
        $this->tab = 'front_office_features';
        $this->version = '0.9';
        $this->author = 'Informax';
        $this->need_instance = 0;

        parent::__construct();

        $this->displayName = $this->l('Featured Products on the homepage by Imax');
        $this->description = $this->l('Displays Featured Products in the middle of your homepage.');
    }

   function install()
	{
		if (!Configuration::updateValue('HOME_FEATURED_NBR', 8) || !parent::install() || !$this->registerHook('displayHome') || !$this->registerHook('header'))
			return false;
		return true;
	}

	public function getContent()
	{
		$output = '<h2>'.$this->displayName.'</h2>';
		if (Tools::isSubmit('submitHomeFeatured'))
		{
			$nbr = (int)(Tools::getValue('nbr'));
			if (!$nbr OR $nbr <= 0 OR !Validate::isInt($nbr))
				$errors[] = $this->l('Invalid number of products');
			else
				Configuration::updateValue('HOME_FEATURED_NBR', (int)($nbr));
			if (isset($errors) AND sizeof($errors))
				$output .= $this->displayError(implode('<br />', $errors));
			else
				$output .= $this->displayConfirmation($this->l('Numero de Productos a Mostrar'));                       
                                               
                        $cat = Tools::getValue('catToShow');
                        $serializeArray = serialize($cat);                         
                        if (!$serializeArray)
				$errors[] = $this->l('Selecciona alguna categoria');
			else
				Configuration::updateValue('HOME_FEATURED_IMAX_CAT',$serializeArray );
			if (isset($errors) AND sizeof($errors))
				$output .= $this->displayError(implode('<br />', $errors));
			else
				$output .= $this->displayConfirmation($this->l('Categorias Actualizadas'));                       
                      
                                             
		}
		return $output.$this->displayForm();
	}

	public function displayForm()
	{
                $category = new Category(); 
                $selectCategorias = "";
                $cat = $category->getCategories((int)Context::getContext()->language->id);
                $arrayCatActivadasSerializado = Configuration::get('HOME_FEATURED_IMAX_CAT');                 
                $arrayCatActivadas = unserialize($arrayCatActivadasSerializado);               
                foreach ($cat as $cat1){
                    foreach ($cat1 as $c){
                    if ($c['infos']['id_parent'] == 2){                    
                        if (in_array($c['infos']['id_category'], $arrayCatActivadas)){
                            $checked = " checked='checked' ";
                        }
                           else     {
                               $checked = "";
                           }
                    $selectCategorias .= '<input type="checkbox" id="catToShow" name="catToShow[]" value="'.$c['infos']['id_category'].'" '. $checked .'> '.$c['infos']['name'].'<br>';            
                        }
                    }
                }
                $this->context->controller->addCss($this->_path.'panel.css', 'all');
		$output = ' 
                    <ul id="menuTab">
				<li id="menuTab1" class="menuTabButton selected">1. ' . $this->l('Configuracion categorias') . '</li>
				<li id="menuTab2" class="menuTabButton">2. ' . $this->l('Ayuda') . '</li>
			</ul>
			<div id="tabList">
				<div id="menuTab1Sheet" class="tabItem selected">
                                <form action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" method="post">
                                 <fieldset><legend><img src="'.$this->_path.'logo.gif" alt="" title="" />'.$this->l('Settings').'</legend>
                                         <p>'.$this->l('Para mostrar tus productos, en la pagina principal, selecciona la categoria pertinente.').'</p><br />
                                         <label>'.$this->l('Numero de productos a mostrar').'</label>
                                         <div class="margin-form">
                                                 <input type="text" size="5" name="nbr" value="'.Tools::safeOutput(Tools::getValue('nbr', (int)(Configuration::get('HOME_FEATURED_NBR')))).'" />
                                                 <p class="clear">'.$this->l('The number of products displayed on homepage (default: 10).').'</p>

                                         </div>
                                         <label>'.$this->l('Seleccione categorias a Mostrar').'</label>
                                         <div class="margin-form">'.                                
                                         $selectCategorias
                                         .'</div>
                                         <center><input type="submit" name="submitHomeFeatured" value="'.$this->l('Save').'" class="button" /></center>
                                 </fieldset>
                         </form>
                         </div>
                         <div id="menuTab2Sheet" class="tabItem">
                        <ul>
                        <li>
                        Visite nuestra pagina <a href="http://www.informax.es"> Informax.es </a>
                        </li>
                        <li>
                        Mas info de nuestros modulos en el 986 484 538
                        </li>
                        <li>
                        Mas info via mail: <a href="mailto:alberto.alvarez@informax.es">Mail</a>
                        </li>
                        <li>
                        <iframe width="560" height="315" src="http://www.youtube.com/embed/nr3O1BXVWKw" frameborder="0" allowfullscreen></iframe>
                        </li>
                        </ul>
                         </div>				
			</div>
			<br clear="left" />			
			<style>
				#menuTab { float: left; padding: 0; margin: 0; text-align: left; }
				#menuTab li { text-align: left; float: left; display: inline; padding: 5px; padding-right: 10px; background: #EFEFEF; font-weight: bold; cursor: pointer; border-left: 1px solid #EFEFEF; border-right: 1px solid #EFEFEF; border-top: 1px solid #EFEFEF; }
				#menuTab li.menuTabButton.selected { background: #FFF6D3; border-left: 1px solid #CCCCCC; border-right: 1px solid #CCCCCC; border-top: 1px solid #CCCCCC; }
				#tabList { clear: left; }
				.tabItem { display: none; }
				.tabItem.selected { display: block; background: #FFFFF0; border: 1px solid #CCCCCC; padding: 10px; padding-top: 20px; }
			</style>
			<script>                       
                        $("ul#tabList li:first").addClass("selected");  //Activar primera pestaña                        
				$(".menuTabButton").click(function () {
				  $(".menuTabButton.selected").removeClass("selected");
				  $(this).addClass("selected");
				  $(".tabItem.selected").removeClass("selected");
				  $("#" + this.id + "Sheet").addClass("selected");                                 
				});
			</script>';
             
                      
                        
                
                 
                
		return $output;
	}

	public function hookDisplayHeader($params)
	{
		$this->hookHeader($params);
	}

	public function hookHeader($params)
	{
		$this->context->controller->addCss($this->_path.'homefeaturedImax.css', 'all');
	}










	public function hookDisplayHome($params)
	{
                $products = array();
                
                $this->context->controller->addCss($this->_path.'homefeaturedImax.css', 'all');
                $arrayCatActivadasSerializado = Configuration::get('HOME_FEATURED_IMAX_CAT');                 
                $arrayCatActivadas = unserialize($arrayCatActivadasSerializado);  
                foreach ($arrayCatActivadas as $i){
                   $category = new Category(); 
                $cat = $category->getCategories((int)Context::getContext()->language->id);
                    $category = new Category($i, (int)Context::getContext()->language->id);
                    $nb = (int)(Configuration::get('HOME_FEATURED_NBR'));
                    $temp = $category->getProducts((int)Context::getContext()->language->id, 1, ($nb ? $nb : 10));
                    $products[$temp[0]['category']] = $temp;
                }               
                $this->smarty->assign(array(
		'products' => $products,
			'add_prod_display' => Configuration::get('PS_ATTRIBUTE_CATEGORY_DISPLAY'),
			'homeSize' => Image::getSize('home_default'),
		));
                return $this->display(__FILE__, 'homefeaturedimax.tpl');
        }
}

