<?php

/*
 * 2007-2012 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author PrestaShop SA <contact@prestashop.com>
 *  @copyright  2007-2012 PrestaShop SA
 *  @version  Release: $Revision: 7048 $
 *  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_'))
    exit;

class HomeFeaturedImax extends Module {

    private $_html = '';
    private $_postErrors = array();

    function __construct() {
        $this->name = 'homefeaturedimax';
        $this->tab = 'front_office_features';
        $this->version = '0.9';
        $this->author = 'Informax';
        $this->need_instance = 0;

        parent::__construct();

        $this->displayName = $this->l('Featured Products on the homepage by Imax');
        $this->description = $this->l('Displays Featured Products in the middle of your homepage.');
    }

    function install() {
        if (!Configuration::updateValue('HOME_FEATURED_NBR', 8) || !parent::install() || !$this->registerHook('displayHome') || !$this->registerHook('displayHeader'))
            return false;
        return true;
    }

    public function getContent() {
        $output = '<h2>' . $this->displayName . '</h2>';
        if (Tools::isSubmit('submitHomeFeaturedByTag')) {           
            $opciones = $_POST['opcionesTag'];
            $nbr = (int) (Tools::getValue('nbr'));
            $datos = array();
            $datos['opciones'] = $opciones;
            $datos['nbr'] = $nbr;
            $datos = serialize($datos);            
            if (!$nbr OR $nbr <= 0 OR !Validate::isInt($nbr)) {
                $errors[] = $this->l('Invalid number of products');
            } else {
                if (!isset($opciones) || empty($opciones)) {
                    $errors[] = $this->l('Seleccion de tags no valida');
                } else {
                    echo $datos;
                    Configuration::updateValue('HOME_FEATURED_TAGS', (string)($datos));
                }
            }          
            if (isset($errors) AND sizeof($errors))
                $output .= $this->displayError(implode('<br />', $errors));
            else
                $output .= $this->displayConfirmation($this->l('Settings updated'));
        }
        return $output . $this->displayForm();
    }

    public function displayForm() {
        $output = '
		<form action="' . Tools::safeOutput($_SERVER['REQUEST_URI']) . '" method="post">
			<fieldset><legend><img src="' . $this->_path . 'logo.gif" alt="" title="" />' . $this->l('Settings') . '</legend>
				<p>' . $this->l('Cuantos productos quieres mostrar por cada Categoria.') . '</p><br />
				<label>' . $this->l('Number of products displayed') . '</label>
				<div class="margin-form">
					<input type="text" size="5" name="nbr" value="' . Tools::safeOutput(Tools::getValue('nbr', (int) (Configuration::get('HOME_FEATURED_NBR')))) . '" />
					<p class="clear">' . $this->l('The number of products displayed on homepage (default: 10).') . '</p>

				</div>
                               <label>' . $this->l('Listados de Tags a Mostrar') . '</label>
				<div class="margin-form">';
        $tags = Tag::getMainTags((int) Context::getContext()->language->id);

        $output .= '<ul class="listForm">';
        foreach ($tags AS $tag) {
            $output .= "<li><input type='checkbox' value='" . $tag['id_tag'] . "' name='opcionesTag[]'><label class='t'>" . $tag['name'] . "</label></li>";
        }
        $output .= '</ul>';
        $output .= '</div>
			<center><input type="submit" name="submitHomeFeaturedByTag" value="' . $this->l('Save') . '" class="button" /></center>
			</fieldset>
		</form>';
        //print_r(Tag::getMainTags((int) Context::getContext()->language->id));
        return $output;
    }

    public function hookDisplayHeader($params) {
        $this->context->controller->addCss($this->_path . 'homefeaturedimax.css');
    }

    public function hookDisplayHome($params) {
        $category = new Category(Context::getContext()->shop->getCategory(), (int) Context::getContext()->language->id);
        $nb = (int) (Configuration::get('HOME_FEATURED_NBR'));
        $products = $category->getProducts((int) Context::getContext()->language->id, 1, ($nb ? $nb : 10));

        //print_r(Tag::getMainTags((int) Context::getContext()->language->id));
        echo Tag::getProductsbyTag('kk');

        $this->smarty->assign(array(
            'products' => $products,
            'add_prod_display' => Configuration::get('PS_ATTRIBUTE_CATEGORY_DISPLAY'),
            'homeSize' => Image::getSize('home'),
        ));

        return $this->display(__FILE__, 'homefeaturedimax.tpl');
    }

}
