
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<div class="container first">
    <img class="libro_bebi" src="/modules/testmodule/img/Libro_Bebi.png" alt="libros bebi">
    <img class="zapatos_perfil" src="/modules/testmodule/img/Zapatos_Perfil.png" alt="zapatos perfil">

    <div class="img-center">
        <img class="zpt_suela" src="/modules/testmodule/img/Zapatos_Suela.png" alt="zapatos suela">
    </div>
    <div class="texto-venta">
        <p class="claim">"Quería unos tacones rosas, altos, elegantes y femeninos. Unos zapatos que escondieran dentro aquello que siempre se ha dicho que no podía ser la mujer que los llevara. Una mujer que hace lo que quiere, que se pone lo que quiere, que es femenina porque quiere y sobre todo, una mujer que pisa fuerte."</p>
        <p class="autor">@SrtaBebi</p>

        <p class="product">Stiletto fabricado en ante rosa,</p>
        <p class="product">cuenta con forro de piel y tacón de 10cm.</p>

        {if isset($product)}


            {if !isset($priceDisplayPrecision)}
                {assign var='priceDisplayPrecision' value=2}
            {/if}
            {if !$priceDisplay || $priceDisplay == 2}
                {assign var='productPrice' value=$product->getPrice(true, $smarty.const.NULL, 6)}
                {assign var='productPriceWithoutReduction' value=$product->getPriceWithoutReduct(false, $smarty.const.NULL)}
            {elseif $priceDisplay == 1}
                {assign var='productPrice' value=$product->getPrice(false, $smarty.const.NULL, 6)}
                {assign var='productPriceWithoutReduction' value=$product->getPriceWithoutReduct(true, $smarty.const.NULL)}
            {/if}

            <p class="price">
                {if $product->show_price && !isset($restricted_country_mode) && !$PS_CATALOG_MODE}
                        <!-- prices -->
                    <div>
                        <p class="our_price_display" itemprop="offers" itemscope itemtype="https://schema.org/Offer">{strip}
                                {if $product->quantity > 0}<link itemprop="availability" href="https://schema.org/InStock"/>{/if}
                                {if $priceDisplay >= 0 && $priceDisplay <= 2}
                                    <span id="our_price_display" class="price" itemprop="price" content="{$productPrice}">{convertPrice price=$productPrice|floatval}</span> iva incl.
                                    {*Escondemos tax incl*}
                                    <span style="display: none;">
                                        {if $tax_enabled  && ((isset($display_tax_label) && $display_tax_label == 1) || !isset($display_tax_label))}
                                            {if $priceDisplay == 1} {l s='tax excl.'}{else} {l s='tax incl.'}{/if}
                                        {/if}
                                    </span>
                                    <meta itemprop="priceCurrency" content="{$currency->iso_code}" />
                                    {hook h="displayProductPriceBlock" product=$product type="price"}
                                {/if}
                            {/strip}</p>
                        <p id="reduction_percent" {if $productPriceWithoutReduction <= 0 || !$product->specificPrice || $product->specificPrice.reduction_type != 'percentage'} style="display:none;"{/if}>{strip}
                                <span id="reduction_percent_display">
                                                        {if $product->specificPrice && $product->specificPrice.reduction_type == 'percentage'}-{$product->specificPrice.reduction*100}%{/if}
                                                    </span>
                            {/strip}</p>
                        <p id="reduction_amount" {if $productPriceWithoutReduction <= 0 || !$product->specificPrice || $product->specificPrice.reduction_type != 'amount' || $product->specificPrice.reduction|floatval ==0} style="display:none"{/if}>{strip}
                                <span id="reduction_amount_display">
                                                    {if $product->specificPrice && $product->specificPrice.reduction_type == 'amount' && $product->specificPrice.reduction|floatval !=0}
                                                        -{convertPrice price=$productPriceWithoutReduction|floatval-$productPrice|floatval}
                                                    {/if}
                                                    </span>
                            {/strip}</p>
                        <p id="old_price"{if (!$product->specificPrice || !$product->specificPrice.reduction)} class="hidden"{/if}>{strip}
                                {if $priceDisplay >= 0 && $priceDisplay <= 2}
                                    {hook h="displayProductPriceBlock" product=$product type="old_price"}
                                    <span id="old_price_display"><span class="price">{if $productPriceWithoutReduction > $productPrice}{convertPrice price=$productPriceWithoutReduction|floatval}{/if}</span>{if $productPriceWithoutReduction > $productPrice && $tax_enabled && $display_tax_label == 1} {if $priceDisplay == 1}{l s='tax excl.'}{else}{l s='tax incl.'}{/if}{/if}</span>
                                {/if}
                            {/strip}</p>
                        {if $priceDisplay == 2}
                            <br />
                            <span id="pretaxe_price">{strip}
                                                        <span id="pretaxe_price_display">{convertPrice price=$product->getPrice(false, $smarty.const.NULL)}</span> {l s='tax excl.'}
                                                    {/strip}</span>
                        {/if}
                    </div> <!-- end prices -->
                    {if $product->ecotax != 0}
                        <p class="price-ecotax">{l s='Including'} <span id="ecotax_price_display">{if $priceDisplay == 2}{$ecotax_tax_exc|convertAndFormatPrice}{else}{$ecotax_tax_inc|convertAndFormatPrice}{/if}</span> {l s='for ecotax'}
                            {if $product->specificPrice && $product->specificPrice.reduction}
                                <br />{l s='(not impacted by the discount)'}
                            {/if}
                        </p>
                    {/if}
                    {if !empty($product->unity) && $product->unit_price_ratio > 0.000000}
                        {math equation="pprice / punit_price" pprice=$productPrice  punit_price=$product->unit_price_ratio assign=unit_price}
                        <p class="unit-price"><span id="unit_price_display">{convertPrice price=$unit_price}</span> {l s='per'} {$product->unity|escape:'html':'UTF-8'}</p>
                        {hook h="displayProductPriceBlock" product=$product type="unit_price"}
                    {/if}
            {/if}
            </p>
            {if ($product->show_price && !isset($restricted_country_mode)) || isset($groups) || $product->reference || (isset($HOOK_PRODUCT_ACTIONS) && $HOOK_PRODUCT_ACTIONS)}
                <!-- add to cart form-->
                <form class="form-talla" id="buy_block"{if $PS_CATALOG_MODE && !isset($groups) && $product->quantity > 0} class="hidden"{/if} action="{$link->getPageLink('cart')|escape:'html':'UTF-8'}" method="post">
                    <!-- hidden datas -->
                    <p class="hidden">
                        <input type="hidden" name="token" value="{$static_token}" />
                        <input type="hidden" name="id_product" value="{$product->id|intval}" id="product_page_product_id" />
                        <input type="hidden" name="add" value="1" />
                        <input type="hidden" name="id_product_attribute" id="idCombination" value="" />
                    </p>
                    <div class="box-info-product">

                        <!-- end product_attributes -->
                        <div class="box-cart-bottom">
                            <!-- quantity wanted -->
                            {if !$PS_CATALOG_MODE}
                                {* <p id="quantity_wanted_p"{if (!$allow_oosp && $product->quantity <= 0) || !$product->available_for_order || $PS_CATALOG_MODE} style="display: none;"{/if}>
                                    <label for="quantity_wanted">{l s='Quantity'}</label>
                                    <a href="#" data-field-qty="qty" class="btn btn-default button-minus product_quantity_down">
                                        <span><i class="icon-minus"></i></span>
                                    </a>
                                    <input type="text" min="1" name="qty" id="quantity_wanted" class="text" value="{if isset($quantityBackup)}{$quantityBackup|intval}{else}{if $product->minimal_quantity > 1}{$product->minimal_quantity}{else}1{/if}{/if}" />
                                    <a href="#" data-field-qty="qty" class="btn btn-default button-plus product_quantity_up">
                                        <span><i class="icon-plus"></i></span>
                                    </a>
                                    <span class="clearfix"></span>
                                </p> *}
                            {/if}
                            <!-- minimal quantity wanted -->
                            <p id="minimal_quantity_wanted_p"{if $product->minimal_quantity <= 1 || !$product->available_for_order || $PS_CATALOG_MODE} style="display: none;"{/if}>
                                {l s='The minimum purchase order quantity for the product is'} <b id="minimal_quantity_label">{$product->minimal_quantity}</b>
                            </p>


                            <fieldset>{l s='Selecciona tu talla'}</fieldset>

                            <div id="attributes">
                                {if isset($groups)}
                                    <!-- attributes -->
                                    {foreach from=$groups key=id_attribute_group item=group}
                                        {if $group.attributes|@count}
                                            {*<label class="attribute_label" {if $group.group_type != 'color' && $group.group_type != 'radio'}for="group_{$id_attribute_group|intval}"{/if}>{$group.name|escape:'html':'UTF-8'}&nbsp;</label>*}
                                            {assign var="groupName" value="group_$id_attribute_group"}



                                            {if ($group.group_type == 'select')}
                                                <select name="{$groupName}" id="group_{$id_attribute_group|intval}" class="form-control attribute_select no-print">
                                                    {foreach from=$group.attributes key=id_attribute item=group_attribute}
                                                        <option value="{$id_attribute|intval}"{if (isset($smarty.get.$groupName) && $smarty.get.$groupName|intval == $id_attribute) || $group.default == $id_attribute} selected="selected"{/if} title="{$group_attribute|escape:'html':'UTF-8'}">{$group_attribute|escape:'html':'UTF-8'}</option>
                                                    {/foreach}
                                                </select>
                                            {elseif ($group.group_type == 'color')}
                                                <ul id="color_to_pick_list" class="clearfix">
                                                    {assign var="default_colorpicker" value=""}
                                                    {foreach from=$group.attributes key=id_attribute item=group_attribute}
                                                        {assign var='img_color_exists' value=file_exists($col_img_dir|cat:$id_attribute|cat:'.jpg')}
                                                        <li{if $group.default == $id_attribute} class="selected"{/if}>
                                                            <a href="{$link->getProductLink($product)|escape:'html':'UTF-8'}" id="color_{$id_attribute|intval}" name="{$colors.$id_attribute.name|escape:'html':'UTF-8'}" class="color_pick{if ($group.default == $id_attribute)} selected{/if}"{if !$img_color_exists && isset($colors.$id_attribute.value) && $colors.$id_attribute.value} style="background:{$colors.$id_attribute.value|escape:'html':'UTF-8'};"{/if} title="{$colors.$id_attribute.name|escape:'html':'UTF-8'}">
                                                                {if $img_color_exists}
                                                                    <img src="{$img_col_dir}{$id_attribute|intval}.jpg" alt="{$colors.$id_attribute.name|escape:'html':'UTF-8'}" title="{$colors.$id_attribute.name|escape:'html':'UTF-8'}" width="20" height="20" />
                                                                {/if}
                                                            </a>
                                                        </li>
                                                        {if ($group.default == $id_attribute)}
                                                            {$default_colorpicker = $id_attribute}
                                                        {/if}
                                                    {/foreach}
                                                </ul>
                                                <input type="hidden" class="color_pick_hidden" name="{$groupName|escape:'html':'UTF-8'}" value="{$default_colorpicker|intval}" />
                                            {elseif ($group.group_type == 'radio')}
                                                <ul>
                                                    {foreach from=$group.attributes key=id_attribute item=group_attribute}
                                                            <input  class="input-radio attribute_radio" id="{$group_attribute|escape:'html':'UTF-8'}" type="radio" name="{$groupName|escape:'html':'UTF-8'}" value="{$id_attribute|intval}" {if ($group.default == $id_attribute)} checked="checked"{/if}><label for="{$group_attribute|escape:'html':'UTF-8'}">{$group_attribute|escape:'html':'UTF-8'}</label>
                                                    {/foreach}
                                                </ul>
                                            {/if}
                                        {/if}
                                    {/foreach}
                                {/if}
                            </div>

                            <div{if (!$allow_oosp && $product->quantity <= 0) || !$product->available_for_order || (isset($restricted_country_mode) && $restricted_country_mode) || $PS_CATALOG_MODE} class="unvisible"{/if}>
                                <p id="add_to_cart" class="buttons_bottom_block no-print">
                                    <input class="button" type="submit" value="{l s='Los quiero'}">
                                    <p class="plazo">*plazo de entrega 15 días laborables.</p>
                                </p>
                            </div>
                            {if isset($HOOK_PRODUCT_ACTIONS) && $HOOK_PRODUCT_ACTIONS}{$HOOK_PRODUCT_ACTIONS}{/if}
                        </div> <!-- end box-cart-bottom -->
                    </div> <!-- end box-info-product -->
                </form>
            {/if}
            {if isset($HOOK_PRODUCT_ACTIONS) && $HOOK_PRODUCT_ACTIONS}{$HOOK_PRODUCT_ACTIONS}{/if}
        {/if}
    </div>
</div>

<div class="container second">
    <img class="textos" src="/modules/testmodule/img/Texto_Bebi-top.png" alt="info landing">
    <a href="https://tusstilettos.com/" target="_blank">
        <img class="textos" src="/modules/testmodule/img/Texto_Bebi-bottom.png" alt="info landing">
    </a>
</div>
</div> <!-- /container -->        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>


<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->

{literal}
<script>
  (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
          function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
          e=o.createElement(i);r=o.getElementsByTagName(i)[0];
          e.src='//www.google-analytics.com/analytics.js';
          r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
  ga('create','UA-89714249-1','auto');ga('send','pageview');
</script>
{/literal}

{strip}
    {if isset($smarty.get.ad) && $smarty.get.ad}
        {addJsDefL name=ad}{$base_dir|cat:$smarty.get.ad|escape:'html':'UTF-8'}{/addJsDefL}
    {/if}
    {if isset($smarty.get.adtoken) && $smarty.get.adtoken}
        {addJsDefL name=adtoken}{$smarty.get.adtoken|escape:'html':'UTF-8'}{/addJsDefL}
    {/if}
    {addJsDef allowBuyWhenOutOfStock=$allow_oosp|boolval}
    {addJsDef availableNowValue=$product->available_now|escape:'quotes':'UTF-8'}
    {addJsDef availableLaterValue=$product->available_later|escape:'quotes':'UTF-8'}
    {addJsDef attribute_anchor_separator=$attribute_anchor_separator|escape:'quotes':'UTF-8'}
    {addJsDef attributesCombinations=$attributesCombinations}
    {addJsDef currentDate=$smarty.now|date_format:'%Y-%m-%d %H:%M:%S'}
    {if isset($combinations) && $combinations}
        {addJsDef combinations=$combinations}
        {addJsDef combinationsFromController=$combinations}
        {addJsDef displayDiscountPrice=$display_discount_price}
        {addJsDefL name='upToTxt'}{l s='Up to' js=1}{/addJsDefL}
    {/if}
    {if isset($combinationImages) && $combinationImages}
        {addJsDef combinationImages=$combinationImages}
    {/if}
    {addJsDef customizationId=$id_customization}
    {addJsDef customizationFields=$customizationFields}
    {addJsDef default_eco_tax=$product->ecotax|floatval}
    {addJsDef displayPrice=$priceDisplay|intval}
    {addJsDef ecotaxTax_rate=$ecotaxTax_rate|floatval}
    {if isset($cover.id_image_only)}
        {addJsDef idDefaultImage=$cover.id_image_only|intval}
    {else}
        {addJsDef idDefaultImage=0}
    {/if}
    {addJsDef img_ps_dir=$img_ps_dir}
    {addJsDef img_prod_dir=$img_prod_dir}
    {addJsDef id_product=$product->id|intval}
    {addJsDef jqZoomEnabled=$jqZoomEnabled|boolval}
    {addJsDef maxQuantityToAllowDisplayOfLastQuantityMessage=$last_qties|intval}
    {addJsDef minimalQuantity=$product->minimal_quantity|intval}
    {addJsDef noTaxForThisProduct=$no_tax|boolval}
    {if isset($customer_group_without_tax)}
        {addJsDef customerGroupWithoutTax=$customer_group_without_tax|boolval}
    {else}
        {addJsDef customerGroupWithoutTax=false}
    {/if}
    {if isset($group_reduction)}
        {addJsDef groupReduction=$group_reduction|floatval}
    {else}
        {addJsDef groupReduction=false}
    {/if}
    {addJsDef oosHookJsCodeFunctions=Array()}
    {addJsDef productHasAttributes=isset($groups)|boolval}
    {addJsDef productPriceTaxExcluded=($product->getPriceWithoutReduct(true)|default:'null' - $product->ecotax)|floatval}
    {addJsDef productPriceTaxIncluded=($product->getPriceWithoutReduct(false)|default:'null' - $product->ecotax * (1 + $ecotaxTax_rate / 100))|floatval}
    {addJsDef productBasePriceTaxExcluded=($product->getPrice(false, null, 6, null, false, false) - $product->ecotax)|floatval}
    {addJsDef productBasePriceTaxExcl=($product->getPrice(false, null, 6, null, false, false)|floatval)}
    {addJsDef productBasePriceTaxIncl=($product->getPrice(true, null, 6, null, false, false)|floatval)}
    {addJsDef productReference=$product->reference|escape:'html':'UTF-8'}
    {addJsDef productAvailableForOrder=$product->available_for_order|boolval}
    {addJsDef productPriceWithoutReduction=$productPriceWithoutReduction|floatval}
    {addJsDef productPrice=$productPrice|floatval}
    {addJsDef productUnitPriceRatio=$product->unit_price_ratio|floatval}
    {addJsDef productShowPrice=(!$PS_CATALOG_MODE && $product->show_price)|boolval}
    {addJsDef PS_CATALOG_MODE=$PS_CATALOG_MODE}
    {if $product->specificPrice && $product->specificPrice|@count}
        {addJsDef product_specific_price=$product->specificPrice}
    {else}
        {addJsDef product_specific_price=array()}
    {/if}
    {if $display_qties == 1 && $product->quantity}
        {addJsDef quantityAvailable=$product->quantity}
    {else}
        {addJsDef quantityAvailable=0}
    {/if}
    {addJsDef quantitiesDisplayAllowed=$display_qties|boolval}
    {if $product->specificPrice && $product->specificPrice.reduction && $product->specificPrice.reduction_type == 'percentage'}
        {addJsDef reduction_percent=$product->specificPrice.reduction*100|floatval}
    {else}
        {addJsDef reduction_percent=0}
    {/if}
    {if $product->specificPrice && $product->specificPrice.reduction && $product->specificPrice.reduction_type == 'amount'}
        {addJsDef reduction_price=$product->specificPrice.reduction|floatval}
    {else}
        {addJsDef reduction_price=0}
    {/if}
    {if $product->specificPrice && $product->specificPrice.price}
        {addJsDef specific_price=$product->specificPrice.price|floatval}
    {else}
        {addJsDef specific_price=0}
    {/if}
    {addJsDef specific_currency=($product->specificPrice && $product->specificPrice.id_currency)|boolval} {* TODO: remove if always false *}
    {addJsDef stock_management=$PS_STOCK_MANAGEMENT|intval}
    {addJsDef taxRate=$tax_rate|floatval}
    {addJsDefL name=doesntExist}{l s='This combination does not exist for this product. Please select another combination.' js=1}{/addJsDefL}
    {addJsDefL name=doesntExistNoMore}{l s='This product is no longer in stock' js=1}{/addJsDefL}
    {addJsDefL name=doesntExistNoMoreBut}{l s='with those attributes but is available with others.' js=1}{/addJsDefL}
    {addJsDefL name=fieldRequired}{l s='Please fill in all the required fields before saving your customization.' js=1}{/addJsDefL}
    {addJsDefL name=uploading_in_progress}{l s='Uploading in progress, please be patient.' js=1}{/addJsDefL}
    {addJsDefL name='product_fileDefaultHtml'}{l s='No file selected' js=1}{/addJsDefL}
    {addJsDefL name='product_fileButtonHtml'}{l s='Choose File' js=1}{/addJsDefL}
{/strip}