Cash On Delivery With Fee - Module for Prestashop 1.4, 1.5 and 1.6
------------------------------------------------------------------

Developed by: idnovate
/*
 * idnovate
 * Cash On Delivery With Fee Module
 *	
 * This product is licensed for one customer to use on one domain. Site developer has the 
 * right to modify this module to suit their needs, but can not redistribute the module in 
 * whole or in part. Any other use of this module constitues a violation of the user agreement.
 *
 *
 *	Versions
 * 	1.50 - Oct 10, 2012 - idnovate - For Prestashop 1.4 & 1.5
 * 	1.60 - Dec 15, 2012 - idnovate - For Prestashop 1.4 & 1.5
 * 	1.70 - Jan 27, 2013 - idnovate - For Prestashop 1.4 & 1.5
 * 	1.80 - Apr 04, 2014 - idnovate - For Prestashop 1.4 & 1.5 & 1.6
 * 	1.90 - Dec 21, 2014 - idnovate - For Prestashop 1.4 & 1.5 & 1.6
 * 	2.00 - Jan 21, 2016 - idnovate - For Prestashop 1.4 & 1.5 & 1.6
 * 	2.0.1 - Feb 12, 2016 - idnovate - For Prestashop 1.4 & 1.5 & 1.6
 * 	2.1.4 - Oct 05, 2016 - idnovate - For Prestashop 1.4 & 1.5 & 1.6
 * 	3.0.0 - Feb 21, 2017 - idnovate - For Prestashop 1.4 & 1.5 & 1.6 & 1.7
 *
 *  Copyright 2017 idnovate
*/

[ES]Descripción: Este módulo permite a las tiendas aceptar el pago de compras mediante la opción contra reembolso
aplicando una comisión extra.

[EN]Description: ]This module allows stores to accept payment of purchases by cash on delivery option
applying an extra fee.

[ES]Instrucciones de instalación:

1. Copia la carpeta codfee a la carpeta Modules
2. Desde el BackOffice accede a Módulos - Pago
3. Instala el módulo

[EN]Installation

1. Copy codfee folder to the Modules folder
2. From the Backoffice, enter Modules - Payments and Gateways
3. Install the module

[ES]Opciones de configuración

- Permite 3 opciones de configuración:
	- Aplicar una comisión fija.
	- Aplicar una comisión basada en un porcentaje sobre el total del pedido.
	- Aplicar una comisión basada en una cantidad fija más un porcentaje.
	- Permite establecer un importe de compra a partir de la cual la comisión no se aplicará.

[EN]Configuration

- Allows three settings:
	- Apply a fixed commission.
	- Apply a commission based on a percentage of the total order.
	- Apply a commission based on a fixed amount plus a percentage.
	- Set purchase amount from which the commission will not apply.